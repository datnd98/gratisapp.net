<?php 
function timeago($time)
  {
    if(!is_numeric($time)){
      $time = strtotime($time);
    }
    $interval = ceil((time()-$time)/31536000);
    if ($interval > 1) {
    return $interval ." years ago";
    }
    $interval = ceil((time()-$time) / 2592000);
    if ($interval > 1) {
      return $interval ." months ago";
    }
    $interval = ceil((time()-$time) / 86400);
    if ($interval > 1) {
      return $interval ." days ago";
    }
    $interval = ceil((time()-$time)/3600);
    if ($interval > 1) {
      return $interval ." hours ago";
    }
    $interval = ceil((time()-$time) / 60);
    if ($interval > 1) {
      return $interval ." minutes ago";
    }
    return time()-$time ." seconds ago";
  }
function formatSizeUnits($bytes)
  {
      if ($bytes >= 1073741824)
      {
          $bytes = number_format($bytes / 1073741824, 1) . ' GB';
      }
      elseif ($bytes >= 1048576)
      {
          $bytes = number_format($bytes / 1048576, 1) . ' MB';
      }
      elseif ($bytes >= 1024)
      {
          $bytes = number_format($bytes / 1024, 1) . ' KB';
      }
      elseif ($bytes > 1)
      {
          $bytes = $bytes . ' bytes';
      }
      elseif ($bytes == 1)
      {
          $bytes = $bytes . ' byte';
      }
      else
      {
          $bytes = '0 bytes';
      }

      return $bytes;
  }
function formatSizeUnitsMB($bytes)
  {
    if ($bytes >= 1073741824)
    {
        $bytes = number_format(($bytes / 1073741824),1) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format(($bytes / 1048576),1) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format(($bytes / 1024),1) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
  }
 function str_slug($text){
  $tmp = $text;
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'utf-8//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return $tmp;
  }

  return $text;
}
function slug_appid($appid){
  if(substr($appid, 0, 4) == 'com.'){
    $appid = substr($appid, 4);
    $appid = 'app-'.$appid;
  }
  if(substr($appid, 0, 4) == 'app.'){
    $appid = substr($appid, 4);
    $appid = 'ap-'.$appid;
  }
  //$text = str_replace('com.', 'app-', $appid);
  $text = str_replace('.', '-', $appid);
  return $text.'.html';
}
function slugCategory($category){
  $tmp = strtolower($category);
  $tmp = str_replace(' & ', '_and_', $tmp);
  $tmp = str_replace(' ', '_', $tmp);
  return $tmp;
}
function asset_url(){
  return 'https://cdn.apktrending.com/';
}
function closetags($html) {

  #put all opened tags into an array

  preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);

  $openedtags = $result[1];   #put all closed tags into an array

  preg_match_all('#</([a-z]+)>#iU', $html, $result);

  $closedtags = $result[1];

  $len_opened = count($openedtags);

  # all tags are closed

  if (count($closedtags) == $len_opened) {

    return $html;

  }

  $openedtags = array_reverse($openedtags);

  # close tags

  for ($i=0; $i < $len_opened; $i++) {

    if (!in_array($openedtags[$i], $closedtags) && $openedtags[$i] != 'br'){

      $html .= '</'.$openedtags[$i].'>';

    } else {

      unset($closedtags[array_search($openedtags[$i], $closedtags)]);    }

  }  return $html;
}  
?>