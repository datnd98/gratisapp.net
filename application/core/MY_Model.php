<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	public function getRawSql($sql){
		$list = array();
		$this->load->database();

		$query = $this->db->query($sql);
		foreach ($query->result() as $row)
		{
			$tmp = array();
			foreach ($row as $key => $value) {
				
				$tmp[$key] = $value;

			}
		    $list[] = $tmp;
		}
		return $list;
	}
	public function queryRawSql($sql){
		$this->load->database();
		$check = $this->db->query($sql);
		return $check;
	}

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */