<main>
	<section id="apk-box-01" class="apk-box apk-box--detail py-3">
		<div class="container">
			<nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white p-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fat fat-home"></i> Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url('category/'.urlencode($apk['category'])) ?>"><?php echo $apk['category'] ?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url($apk['urlTitle'].'/'.$apk['appid']) ?>"><?php echo $apk['title'] ?></a></li>
                <li class="breadcrumb-item">Download <?php echo $apk['title'] ?> APK <?php echo $version ?></li>
            </ol>
            </nav>
            
    		<div class="row">
    			<div class="col-lg-8 col-xl-9">
                    <div class="ads">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- 336x280 -->
                        <ins class="adsbygoogle"
                        style="display:inline-block;width:336px;height:280px"
                        data-ad-client="ca-pub-1323220695835105"
                        data-ad-slot="7671789628"></ins>
                        <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
    				<div class="apkdownload">
    					<p id="text-download">Downloading</p>
    					<h1>gratisapp.<?php echo $apk['appid'] ?>.<?php echo $versionCode ?>.<?php if($apk['OBB'] == 0){ echo 'apk';} else {echo 'zip';} ?>   (<?php echo formatSizeUnitsMB($apk['size']) ?>)</h1>
    					<p>If the download doesn't start, <a href="<?php echo $download_link  ?>" rel="nofollow">click here</a></p>
    				</div>
    				<div class="row tutorial">
    					<div class="col-lg-8 video-tutorial">
                            <?php
                            switch ($apk['OBB']) {
                                case 0:
                                    $yt_id = 'cVOXsqRFkyk';
                                    break;
                                case 1:
                                    $yt_id = 'g9agZY8NZJo';
                                    break;
                                case 2:
                                    $yt_id = 'M2oF4vi51Qo';
                                    break;
                                default:
                                    $yt_id = 'cVOXsqRFkyk';
                                    break;
                            }
                            ?>
    						<iframe
							  src="https://www.youtube.com/embed/<?php echo $yt_id ?>"
							  srcdoc="<style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style><a href=https://www.youtube.com/embed/<?php echo $yt_id ?>?autoplay=1><img src=https://img.youtube.com/vi/<?php echo $yt_id ?>/hqdefault.jpg alt='Video The Dark Knight Rises: What Went Wrong? – Wisecrack Edition'><span>▶</span></a>"
							  frameborder="0"
							  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
							  allowfullscreen
							  title="The Dark Knight Rises: What Went Wrong? – Wisecrack Edition">
							</iframe>
                            <iframe style="display: none" src="<?php echo $download_link  ?>"></iframe>
    					</div>
    					<div class="col-lg-4 info-tutorial">
    						<h3>How to install your download?</h3>
    						<ul>
    							<li>Find your file:</li>
								<li>Check your downloads folder.</li>
								<li>- With Windows use Control + J
								- With Mac use Shift + Command + J
								Once you have located your download:
								- Click in your Download file!
								- Follow the installer instructions.</li>
    						</ul>
    						<a href="#" class="btn-help" title="">Install Help</a>
    					</div>
    				</div>
    				<div class="row tutorial report">
    					<h3>I want to report this software because:</h3>
    					<ul>
    						<li><a href="#">download doesn't work</a></li>
    						<li><a href="#">Installation failed</a></li>
    						<li><a href="#">Copyright infringement</a></li>
    						<li><a href="#">Another error</a></li>
    					</ul>
    				</div>
    			</div>
    			<div class="col-lg-4 col-xl-3">
    				<!-- BEGIN: .apk-sidebar -->
                    <aside class="apk-sidebar clearfix">
                        <div class="apk-socials float-md-left float-lg-none py-3 pt-lg-0 mb-3 mb-md-0 mb-lg-3 border-bottom">
                            <label class=" d-block mb-0">Follow APKTrending on</label>
                            <div class="follow_icon">
                            <a class="text-secondary" target="_blank" href="https://www.facebook.com/APKTrending.website"><img src="<?php echo base_url() ?>assets/images/facebook.svg" alt="" class="lazyload"></a>
                            <a class="text-secondary" target="_blank" href="https://twitter.com/ApkTrending"><img src="<?php echo base_url() ?>assets/images/twitter.svg" alt="" class="lazyload"></a>
                            <a class="text-secondary" target="_blank" href="https://www.youtube.com/channel/UC830f7syAimid3f8aCkKN-A"><img src="<?php echo base_url() ?>assets/images/youtube.svg" alt="" class="lazyload"></a>
                            <a class="text-secondary" target="_blank" href="https://www.pinterest.com/APKTrendingwebsite"><img src="<?php echo base_url() ?>assets/images/pinterest.svg" alt="" class="lazyload"></a>
                            </div>
                        </div>
                        <div class="apk-downloader float-md-right float-lg-none pt-md-3 pt-lg-0">
                            <div class="top-banner">
                                <img src="https://cdn.apktrending.com/assets/images/icon.png">
                                <div class="apk-downloader__title font-weight-bold">Install APKTrending App<br><span>For your faster download experience</span></div>
                            </div>
                            
                            <a href="https://cdn.apktrending.com/assets/images/apktrending.apk" class="download-app" title="">Download</a>
                        </div>
                    </aside>
                    <!-- END: /.apk-sidebar -->

                    <aside class="apk-sidebar clearfix">
                        <div class="apk-sidebar__title h6 font-weight-bold text-uppercase">Similar</div>
                        <?php $relate = json_decode($apk['similar'],true); ?>
                        <ul class="apk-sidebar__content widget-list-app">
                            <?php foreach ($relate as $item) {?>
                            <li class="clearfix">
                                <div class="apk-app clearfix">
                                    <div class="apk-app__img float-left">
                                        <?php $slug = str_slug($item['title']); ?>
                                        <a href="<?php echo base_url($item['urlTitle'].'/'.$item['appid']) ?>">
                                            <img data-src="<?php echo $item['cover'].'=s48' ?>" alt="<?php echo $item['title'] ?>" class="lazyload">
                                        </a>
                                    </div>
                                    <div class="apk-app__content">
                                        <h2 class="apk-app__title h6 mb-1"><a href="<?php echo base_url($item['urlTitle'].'/'.$item['appid']) ?>"><?php echo $item['title'] ?>
                                            </a></h2>
                                        <div class="apk-app__author font-weight-light mb-2">by <a href="<?php echo base_url('dev/'.urlencode($item['dev_id'])) ?>"><?php echo $item['offerby'] ?>
                                            </a></div>
                                        <div class="apk-app__rating d-inline-block mr-3">
                                            <span class="star-rating">
                                                <span class="star-rating__size" style="width: <?php echo round($item['score']/5*100,2) ?>%;"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </aside>

    			</div>

    		</div>
		</div>
	</section>
</main>