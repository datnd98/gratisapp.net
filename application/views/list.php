<main class="list_app">
    <section class="section_banner text-center mt-4 mb-4">
        <div class="container">
            <a href="#" title="" style="    display: revert;">
                <!--                img_desktop-->
                <img src="<?php echo base_url(); ?>assets/images/banner.png" alt="" class="hidden-sm hidden-xs">
                <!--                img_mobile-->
                <img src="<?php echo base_url(); ?>assets/images/ads_mobile.png" alt="" class="hidden-lg hidden-md">
            </a>
        </div>
    </section>
    <section class="section_content">
        <div class="container">
            <div class="row">
                <div class="box_related_app mt-4 mb-4 col-md-12">
                    <div class="title-head mb-3 " style="flex-wrap: wrap">
                        <h2 class="mg-0 title"><?php
                                                echo $title_page;
                                                ?></h2>
                        <!-- <div class="aselect select_category " style="min-width: 270px;">
                            <select name="" id="normal-select-2" aria-placeholder="Your Drive" class="form-control select_category" style="display: inline-block;max-width:270px;border-radius: 4px;">
                                <option value="">Category</option>
                            </select>
                        </div> -->
                    </div>
                    <div class="row list_app">
                        <?php for ($j = 0; $j < sizeof($list); $j++) : ?>
                            <div class="col-md-4 col-sm-6">
                                <div class="item_box_app">
                                    <div class="images_app">
                                        <a href="<?php echo base_url($list[$j]['urlTitle'] . '/' . $list[$j]['appid']); ?>">
                                            <img src="<?php echo $list[$j]['cover']; ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="content_right">
                                        <h3 class="title mg-0"><a href="<?php echo base_url($list[$j]['urlTitle'] . '/' . $list[$j]['appid']); ?>"><?php echo $list[$j]['title']; ?></a></h3>
                                        <div class="rankting">
                                            <div class="item_rate flex">
                                                <span class="" style="width: <?php echo round($list[$j]['score'] / 5 * 100, 2) ?>%;"></span>
                                            </div>
                                        </div>
                                        <span class="date"><?php
                                                            $today = strtotime(date('Y-m-d'));
                                                            $time = floor(($today - $list[$j]['uploaddate']) / 86400);
                                                            if ($time > 365) {
                                                                if (floor($time / 365) > 1) {
                                                                    echo floor($time / 365) . " years ago";
                                                                } else {
                                                                    echo floor($time / 365) . " year ago";
                                                                }
                                                            } elseif ($time > 31) {
                                                                if (floor($time / 31) > 1) {
                                                                    echo floor($time / 31) . " months ago";
                                                                } else {
                                                                    echo floor($time / 31) . " month ago";
                                                                }
                                                            } elseif ($time > 1) {
                                                                echo $time . " days ago";
                                                            } elseif ($time == 1) {
                                                                echo $time . " day ago";
                                                            } else {
                                                                echo floor(($today - $list[$j]['uploaddate']) / 3600) . " hours ago";
                                                            }
                                                            ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                        <div class="col-md-12 text-center mt-2">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <?php
                                    $page_total = $totalPage;
                                    $char = '?';
                                    $params = $_GET;
                                    $url = '';
                                    foreach ($params as $key => $param) {
                                        if ($key != 'page' && $key != '_url') {
                                            //$char = '&';
                                            $url .= '&' . $key . '=' . $param;
                                        }
                                    }
                                    if ($totalPage > ($page + 3)) {
                                        $i = $page + 3;
                                    } else {
                                        $i = $totalPage;
                                    }
                                    if ($page == 1) {
                                        $start = 1;
                                    } else {
                                        $start = $page - 1;
                                    }
                                    ?>
                                    <?php if ($totalPage > 1) {
                                        if ($page == 2) { ?>
                                            <li class="page-item">
                                                <a class="page-link pre" href="<?php echo current_url(); ?><?php if ($url != '') {
                                                                                                                $tmp = substr($url, 1);
                                                                                                                $tmp = '?' . $tmp;
                                                                                                                echo $tmp;
                                                                                                            } ?>"><svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M0.25 7.99997C0.25 8.17434 0.311093 8.34889 0.433124 8.48201L6.68309 15.3002C6.92731 15.5666 7.32278 15.5666 7.56684 15.3002C7.8109 15.0338 7.81105 14.6023 7.56684 14.3361L1.75874 7.99997L7.56684 1.66384C7.81105 1.39742 7.81105 0.966001 7.56684 0.699751C7.32262 0.433501 6.92715 0.433331 6.68309 0.699751L0.433124 7.51792C0.311093 7.65105 0.25 7.82559 0.25 7.99997Z" fill="#8F9AA6" />
                                                    </svg></a></li>
                                        <?php } elseif ($page > 2) { ?>
                                            <li class="page-item">
                                                <a class="page-link pre" href="<?php echo current_url() . $char ?>page=1<?php echo $url ?>"><img src="<?php echo base_url(); ?>assets/images/leftArrow.png"></a>
                                            </li>
                                            <li class="page-item">
                                                <a class="page-link pre" href="<?php echo current_url() . $char ?>page=<?php echo $page - 1; ?><?php echo $url ?>"><svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M0.25 7.99997C0.25 8.17434 0.311093 8.34889 0.433124 8.48201L6.68309 15.3002C6.92731 15.5666 7.32278 15.5666 7.56684 15.3002C7.8109 15.0338 7.81105 14.6023 7.56684 14.3361L1.75874 7.99997L7.56684 1.66384C7.81105 1.39742 7.81105 0.966001 7.56684 0.699751C7.32262 0.433501 6.92715 0.433331 6.68309 0.699751L0.433124 7.51792C0.311093 7.65105 0.25 7.82559 0.25 7.99997Z" fill="#8F9AA6" />
                                                    </svg>
                                                </a></li>
                                        <?php } ?>
                                        <?php for ($j = $start; $j <= $i; $j++) {
                                        ?>
                                            <?php if ($j == $page) { ?>
                                                <li class="page-item"><a class="page-link active" href="#"><?php
                                                                                                            /*if ($page >= 1000000000) {
                                                                                                                $mol = $page % 1000000000;
                                                                                                                $rate = ($page - $mol) / 1000000000;
                                                                                                                echo $rate . "B+";
                                                                                                            } elseif ($page >= 1000000) {
                                                                                                                $mol = $page % 1000000;
                                                                                                                $rate = ($page - $mol) / 1000000;
                                                                                                                echo $rate . "M+";
                                                                                                            } elseif ($page >= 1000) {
                                                                                                                $mol = $page % 1000;
                                                                                                                $rate = ($page - $mol) / 1000;
                                                                                                                echo $rate . "K+";
                                                                                                            } else {
                                                                                                                echo $page;
                                                                                                            }*/
                                                                                                            echo $page;
                                                                                                            ?></a></li>
                                            <?php } else { ?>
                                                <?php if ($j == 1) { ?>
                                                    <li class="page-item"><a class="page-link" href="<?php echo current_url(); ?><?php if ($url != '') {
                                                                                                                                        $tmp = substr($url, 1);
                                                                                                                                        $tmp = '?' . $tmp;
                                                                                                                                        echo $tmp;
                                                                                                                                    } ?>"><?php
                                                                                                                                            /*if ($j >= 1000000000) {
                                                                                                                                                $mol = $j % 1000000000;
                                                                                                                                                $rate = ($j - $mol) / 1000000000;
                                                                                                                                                echo $rate . "B+";
                                                                                                                                            } elseif ($j >= 1000000) {
                                                                                                                                                $mol = $j % 1000000;
                                                                                                                                                $rate = ($j - $mol) / 1000000;
                                                                                                                                                echo $rate . "M+";
                                                                                                                                            } elseif ($j >= 1000) {
                                                                                                                                                $mol = $j % 1000;
                                                                                                                                                $rate = ($j - $mol) / 1000;
                                                                                                                                                echo $rate . "K+";
                                                                                                                                            } else {
                                                                                                                                                echo $j;
                                                                                                                                            }*/
                                                                                                                                            echo $j;
                                                                                                                                            ?></a></li>
                                                <?php } else { ?>
                                                    <li class="page-item"><a class="page-link" href="<?php echo current_url() . $char ?>page=<?php echo $j . $url ?>"><?php
                                                                                                                                                                        /*if ($j >= 1000000000) {
                                                                                                                                                                            $mol = $j % 1000000000;
                                                                                                                                                                            $rate = ($j - $mol) / 1000000000;
                                                                                                                                                                            echo $rate . "B+";
                                                                                                                                                                        } elseif ($j >= 1000000) {
                                                                                                                                                                            $mol = $j % 1000000;
                                                                                                                                                                            $rate = ($j - $mol) / 1000000;
                                                                                                                                                                            echo $rate . "M+";
                                                                                                                                                                        } elseif ($j >= 1000) {
                                                                                                                                                                            $mol = $j % 1000;
                                                                                                                                                                            $rate = ($j - $mol) / 1000;
                                                                                                                                                                            echo $rate . "K+";
                                                                                                                                                                        } else {
                                                                                                                                                                            echo $j;
                                                                                                                                                                        }*/
                                                                                                                                                                        echo $j;
                                                                                                                                                                        ?></a></li>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if ($totalPage > 3 && $page < $totalPage - 4) { ?>
                                            <li class="page-item">
                                                <span>...</span>
                                            </li>
                                        <?php } ?>
                                        <?php if ($page < $totalPage) { ?>
                                            <?php if ($i < $totalPage) { ?>

                                                <li class="page-item"><a class="page-link" href="<?php echo current_url() . $char ?>page=<?php echo $totalPage . $url ?>"><?php
                                                                                                                                                                            /*if ($totalPage >= 1000000000) {
                                                                                                                                                                                $mol = $totalPage % 1000000000;
                                                                                                                                                                                $rate = ($totalPage - $mol) / 1000000000;
                                                                                                                                                                                echo $rate . "B+";
                                                                                                                                                                            } elseif ($totalPage >= 1000000) {
                                                                                                                                                                                $mol = $totalPage % 1000000;
                                                                                                                                                                                $rate = ($totalPage - $mol) / 1000000;
                                                                                                                                                                                echo $rate . "M+";
                                                                                                                                                                            } elseif ($totalPage >= 1000) {
                                                                                                                                                                                $mol = $totalPage % 1000;
                                                                                                                                                                                $rate = ($totalPage - $mol) / 1000;
                                                                                                                                                                                echo $rate . "K+";
                                                                                                                                                                            } else {
                                                                                                                                                                                echo $totalPage;
                                                                                                                                                                            }*/
                                                                                                                                                                            echo $totalPage;
                                                                                                                                                                            ?></a></li>
                                            <?php } ?>
                                            <li class="page-item">
                                                <a class="page-link" href="<?php echo current_url() . $char ?>page=<?php echo $page + 1; ?><?php echo $url ?>"><svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M7.75 8.00003C7.75 7.82566 7.68891 7.65111 7.56688 7.51799L1.31691 0.699815C1.07269 0.433395 0.677224 0.433395 0.433163 0.699815C0.189102 0.966235 0.188946 1.39765 0.433163 1.6639L6.24126 8.00003L0.433163 14.3362C0.188945 14.6026 0.188945 15.034 0.433162 15.3002C0.67738 15.5665 1.07285 15.5667 1.31691 15.3002L7.56688 8.48208C7.68891 8.34895 7.75 8.17441 7.75 8.00003Z" fill="#111111" />
                                                    </svg></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- <a href="" class="btn_show_more hidden-md hidden-lg">Show more</a> -->
                </div>
                <!--                end_box_related_app-->
            </div>
            <div class="row hidden-lg hidden-md">
                <div class="col-md-12">
                    <div class="box_tag mb-4">
                        <h3 class="title_default mt-0">Hot Tags</h3>
                        <div class="list_tag">
                            <a href="" title="">Among Us</a>
                            <a href="" title="">Tiktok</a>
                            <a href="" title="">Facebook</a>
                            <a href="" title="">GAM</a>
                            <a href="" title="">LOL</a>
                            <a href="" title="">PUBG</a>
                            <a href="" title="">Pinterest</a>
                            <a href="" title="">PUBG</a>
                            <a href="" title="">Pinterest</a>
                            <a href="" title="">Insta</a>
                            <a href="" title="">LOL</a>

                        </div>
                        <!--                        end_list_tag-->
                    </div>
                    <div class="box_social">
                        <p>Follow gratisapp.net on</p>
                        <ul class="list_social text-center mt-2">
                            <li><a href="" title="Facebook"><img src="<?php echo base_url(); ?>assets/images/Path.svg" alt=""></a></li>
                            <li><a href="" title="Pinterest"><img src="<?php echo base_url(); ?>assets/images/Pinterest.svg" alt=""></a></li>
                            <li><a href="" title="Youtube"><img src="<?php echo base_url(); ?>assets/images/Youtube.svg" alt=""></a></li>
                            <li><a href="" title="Twitter"><img src="<?php echo base_url(); ?>assets/images/Twitter.svg" alt=""></a></li>
                        </ul>
                    </div>
                    <!--banner_mobile-->
                    <div class="mt-4">
                        <a href="" class="hidden-lg hidden-md "><img src="<?php echo base_url(); ?>assets/images/banner_mobile_bt.png" alt="" style="width: 100%"></a>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>