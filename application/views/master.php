<!DOCTYPE html>
<html>

<head>
	<?php $this->load->view($meta); ?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext" rel="stylesheet">
	<link rel="icon" type="image/png" href="<?php echo asset_url() ?>assets/images/favicon.ico">
	<meta name="google-site-verification" content="NW8ZBXhhkmoSrg-YP6LgJZ623l0iKHbi4WxaINsLb5o" />
	<?php $this->load->view($style); ?>
</head>
<script async>
	function setCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + (value || "") + expires + "; path=/";
	}

	function getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return 'undefined';
	}

	function delete_cookie(name) {
		document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}
</script>

<body>
	<?php $this->load->view('header') ?>
	<?php $this->load->view($main); ?>

	<?php $this->load->view('footer'); ?>
	<script src="<?php echo base_url() ?>assets/js/lazysizes.min.js" type="text/javascript" charset="utf-8" async defer></script>
</body>
<script async>
	(function() {
		;
		if (getCookie("total_cookie") !== 'undefined') {
			var total_cookie = getCookie("total_cookie");
			var html_history = '';
			for (var i = 1; i <= total_cookie; i++) {
				if (getCookie("cookie" + i) !== 'undefined') {
					var key_search_history = getCookie("cookie" + i);
					html_history += '<li>';
					html_history += '<a href="/search?q=' + key_search_history + '" class="link">';
					html_history += '<p>' + key_search_history + ' </p>';
					html_history += '</a>';
					html_history += '<span onclick="eraseCookie(' + i + ');" style="min-width: 9px;"><img data-src="<?php echo asset_url() ?>assets/images/X.svg" alt="X Trending" class="lazyload"></span>';
					html_history += '</li>';
				}
			}
			document.getElementById("list_history").innerHTML = html_history;
			document.getElementById("search_history").style.display = "block";
		} else {
			document.getElementById("search_history").style.display = "none";
		}
	})();

	function clear_all_cookie() {
		if (getCookie("total_cookie") !== 'undefined') {
			var total_cookie = getCookie("total_cookie");
			var html_history = '';
			for (var i = 1; i <= total_cookie; i++) {
				var key_search_history = "cookie" + i;
				delete_cookie(key_search_history);
			}
			delete_cookie("total_cookie");
			document.getElementById("list_history").innerHTML = html_history;
			document.getElementById("search_history").style.display = "none";
		}
	}

	function eraseCookie(name) {
		if (getCookie("total_cookie") !== 'undefined') {
			var total_cookie = getCookie("total_cookie");
			var html_history = '';
			for (var i = 1; i <= total_cookie; i++) {
				if (i == name) {
					delete_cookie("cookie" + i);
				} else {
					if (getCookie("cookie" + i) !== 'undefined') {
						var key_search_history = getCookie("cookie" + i);
						html_history += '<li>';
						html_history += '<a href="/search?q=' + key_search_history + '" class="link">';
						html_history += '<p>' + key_search_history + ' </p>';
						html_history += '</a>';
						html_history += '<span onclick="eraseCookie(' + i + ');" style="min-width: 9px;"><img data-src="<?php echo base_url() ?>assets/images/X.svg" alt="X Trending" class="lazyload"></span>';
						html_history += '</li>';
					}
				}
			}
			if (html_history == '') {
				document.getElementById("search_history").style.display = "none";
				delete_cookie('total_cookie');
			} else {
				document.getElementById("list_history").innerHTML = html_history;
				document.getElementById("search_history").style.display = "block";
			}
		}
	}

	function active_cancel() {
		var cancel = document.getElementById("search_cancel");
		var search_btn = document.getElementById("search-btn_mobile");

		if (!cancel.classList.contains('active')) {
			console.log(1);
			cancel.classList.add("active");
			search_btn.style.display = "none";
			// document.getElementById("search-top-mobile").focus();

		} else {
			console.log(2);
			cancel.classList.remove("active");
			search_btn.style.display = "block";
		}
		// console.log(1);
	}

	function active_search_screen() {
		var search_screen = document.getElementById("search_screen");


		if (!search_screen.classList.contains('active')) {
			// console.log(1);
			search_screen.classList.add("active");
		} else {
			// console.log(2);
			search_screen.classList.remove("active");
		}

	}

	function opensearch() {
		var clicked = document.getElementsByClassName('active');
		var element = document.getElementById("search-mb");


		//element.classList.add("clicked");

		if (!element.classList.contains('active')) {
			console.log(1);
			element.classList.add("active");
			document.getElementById("search-top-mobile").focus();
			document.body.style.overflow = "hidden";
			document.getElementById("apk-logo").style.display = "none";

		} else {
			console.log(2);
			element.classList.remove("active");
			document.body.style.overflow = "auto";
			document.getElementById("apk-logo").style.display = "block";



		}
		active_cancel();
		active_search_screen();

	}



	function openmenu() {
		var clicked = document.getElementsByClassName('active-mb');
		var element = document.getElementById("menu-mb");
		var icon = document.getElementById("menu-icon");
		if (!element.classList.contains('active-mb')) {

			element.classList.add("active-mb");
			icon.classList.add("clicked");
		} else {

			element.classList.remove("active-mb");
			icon.classList.remove("clicked");
		}


	}

	function hideSearch(ss) {
		const element = document.getElementById('search-pc');
		const isHover = e => e.parentElement.querySelector(':hover') === e;
		const hovered = isHover(element);
		if (hovered !== ss.hovered) {
			if (hovered) {
				element.classList.add("show-livesearch");
			} else {
				element.classList.remove("show-livesearch");
			}
		}
	}

	function livesearch(string) {
		var div = document.getElementById('search_results')
		if (document.getElementById("input_search").value.length > 0) {
			var string = document.getElementById("input_search").value;
			if (window.XMLHttpRequest) {
				// code for modern browsers
				xhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					document.getElementById("demo").innerHTML = this.responseText;
				}
			};
			var params = 'q=' + string;
			xhttp.open("POST", "/gratisapp.net/livesearch", true);
			xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

			xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					//console.log(xhttp.responseText);
					var data = xhttp.responseText;
					data = JSON.parse(data);
					var html = '';
					var html = '<ul class="menu_results" id="menu_results">';
					for (var i = 0; i < data.length; i++) {
						html += '<li>';
						html += '<div class="item"><div class="images">';
						html += '<a href="<?php echo base_url() ?>' + data[i].urltitle + '/' + data[i].appid + '">';
						html += '<img src="' + data[i].cover + '=s32" alt="' + data[i].title + '">';
						html += '</a></div><div class="txt"><div class="title">';
						html += '<a href="<?php echo base_url() ?>' + data[i].urltitle + '/' + data[i].appid + '">';
						html += data[i].title;
						html += '</a></div><p>';
						html += data[i].offerby;
						html += '</p>';
						html += '</div></div>';
						html += '</li>';
					}
					html += '</ul>';
					document.getElementById("search_results").innerHTML = html;
					div.classList.add("opened");
				}
			}
			xhttp.send(params);
		} else {
			div.classList.remove("opened");
		}
	}

	function leavesearch() {
		var element = document.getElementById("search-pc");
		element.classList.remove("show-livesearch");
		var ss = document.getElementById("livesearch-mb");
		ss.classList.remove("show-livesearch");
	}

	function showSearch(string) {
		//if(string.length > 0){
		var element = document.getElementById("search-pc");
		element.classList.add("show-livesearch");
		var ss = document.getElementById("livesearch-mb");
		ss.classList.add("show-livesearch");
		//}
	}

	////////////////////////// slide show
	var interval = setInterval(change_mob, 1000);

	function change_mob() {
		var slideIndex = 1;
		showSlides(slideIndex);
		clearInterval(interval);
	}
	var slideIndex = 1;
	showSlides(slideIndex);

	function plusSlides(n) {
		showSlides(slideIndex += n);
	}

	function currentSlide(n) {
		showSlides(slideIndex = n);
	}

	function showSlides(n) {
		var i;
		var slides = document.getElementsByClassName("mySlides");
		var dots = document.getElementsByClassName("dot");
		if (n > slides.length) {
			slideIndex = 1
		}
		if (n < 1) {
			slideIndex = slides.length
		}
		for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";
		}
		for (i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace(" active", "");
		}
		slides[slideIndex - 1].style.display = "flex";
		dots[slideIndex - 1].className += " active";
	}
	//////////////////////////////////

	function opengame(i) {
		document.getElementById('app_' + i).style.display = 'none';
		if (i == 1) {
			document.getElementById('app_button' + (i - 1)).classList.remove("white_text");
			document.getElementById('app_button' + (i - 1)).classList.add("black_text");
			document.getElementById('app_button' + (i - 1)).classList.remove("blue_bgr");
			document.getElementById('app_button' + (i - 1)).classList.add("gray_bgr");
			document.getElementById('game_button' + (i - 1)).classList.remove("gray_bgr");
			document.getElementById('game_button' + (i - 1)).classList.add("blue_bgr");
			document.getElementById('game_button' + (i - 1)).classList.remove("black_text");
			document.getElementById('game_button' + (i - 1)).classList.add("white_text");
		}
		if (i == 2) {
			document.getElementById('app_button' + (i - 1)).classList.remove("white_text");
			document.getElementById('app_button' + (i - 1)).classList.add("black_text");
			document.getElementById('app_button' + (i - 1)).classList.remove("red_bgr");
			document.getElementById('app_button' + (i - 1)).classList.add("gray_bgr");
			document.getElementById('game_button' + (i - 1)).classList.remove("gray_bgr");
			document.getElementById('game_button' + (i - 1)).classList.add("red_bgr");
			document.getElementById('game_button' + (i - 1)).classList.remove("black_text");
			document.getElementById('game_button' + (i - 1)).classList.add("white_text");
		}
		if (i == 3) {
			document.getElementById('app_button' + (i - 1)).classList.remove("white_text");
			document.getElementById('app_button' + (i - 1)).classList.add("black_text");
			document.getElementById('app_button' + (i - 1)).classList.remove("green_bgr");
			document.getElementById('app_button' + (i - 1)).classList.add("gray_bgr");
			document.getElementById('game_button' + (i - 1)).classList.remove("gray_bgr");
			document.getElementById('game_button' + (i - 1)).classList.add("green_bgr");
			document.getElementById('game_button' + (i - 1)).classList.remove("black_text");
			document.getElementById('game_button' + (i - 1)).classList.add("white_text");
		}
		// console.log(color);
		document.getElementById('game_' + i).style.display = 'flex';
	}


	function openapp(i) {
		document.getElementById('app_' + i).style.display = 'flex';
		if (i == 1) {
			document.getElementById('game_button' + (i - 1)).classList.remove("white_text");
			document.getElementById('game_button' + (i - 1)).classList.add("black_text");
			document.getElementById('game_button' + (i - 1)).classList.remove("blue_bgr");
			document.getElementById('game_button' + (i - 1)).classList.add("gray_bgr");
			document.getElementById('app_button' + (i - 1)).classList.remove("gray_bgr");
			document.getElementById('app_button' + (i - 1)).classList.add("blue_bgr");
			document.getElementById('app_button' + (i - 1)).classList.remove("black_text");
			document.getElementById('app_button' + (i - 1)).classList.add("white_text");
		}
		if (i == 2) {
			document.getElementById('game_button' + (i - 1)).classList.remove("white_text");
			document.getElementById('game_button' + (i - 1)).classList.add("black_text");
			document.getElementById('game_button' + (i - 1)).classList.remove("red_bgr");
			document.getElementById('game_button' + (i - 1)).classList.add("gray_bgr");
			document.getElementById('app_button' + (i - 1)).classList.remove("gray_bgr");
			document.getElementById('app_button' + (i - 1)).classList.add("red_bgr");
			document.getElementById('app_button' + (i - 1)).classList.remove("black_text");
			document.getElementById('app_button' + (i - 1)).classList.add("white_text");
		}
		if (i == 3) {
			document.getElementById('game_button' + (i - 1)).classList.remove("white_text");
			document.getElementById('game_button' + (i - 1)).classList.add("black_text");
			document.getElementById('game_button' + (i - 1)).classList.remove("green_bgr");
			document.getElementById('game_button' + (i - 1)).classList.add("gray_bgr");
			document.getElementById('app_button' + (i - 1)).classList.remove("gray_bgr");
			document.getElementById('app_button' + (i - 1)).classList.add("green_bgr");
			document.getElementById('app_button' + (i - 1)).classList.remove("black_text");
			document.getElementById('app_button' + (i - 1)).classList.add("white_text");
		}
		document.getElementById('game_' + i).style.display = 'none';
	}
</script>
<script async>
	function lazyloadAds(e, p, w) {
		var a = document.createElement("script");
		if (a.type = "text/javascript", a.async = !0, a.src = e, p)
			for (var c in p) a.dataset[c] = p[c];
		var n = w ? document.querySelector(w) : document.getElementsByTagName("script")[0];
		n.parentNode.insertBefore(a, n)
	}(function() {
		var lazyLoad = false;

		function onLazyLoad() {
			if (lazyLoad === false) {
				lazyLoad = true;
				document.removeEventListener('scroll', onLazyLoad);
				document.removeEventListener('mousemove', onLazyLoad);
				document.removeEventListener('mousedown', onLazyLoad);
				document.removeEventListener('touchstart', onLazyLoad);
				lazyloadAds('https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');
				(adsbygoogle = window.adsbygoogle || []).push({
					google_ad_client: "ca-pub-1323220695835105",
					enable_page_level_ads: true
				});
				lazyloadBackground();
			}
		}
		document.addEventListener("scroll", onLazyLoad), document.addEventListener("mousemove", onLazyLoad), document.addEventListener("mousedown", onLazyLoad), document.addEventListener("touchstart", onLazyLoad), document.addEventListener("load", function() {
			document.body.clientHeight != document.documentElement.clientHeight && 0 == document.documentElement.scrollTop && 0 == document.body.scrollTop || onLazyLoad()
		});
	})();
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-96491293-1"></script>
<script async>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'UA-96491293-1');
</script>

</html>