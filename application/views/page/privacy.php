<h1 dir="ltr"><strong>Privacy Policy</strong></h1>

<p dir="ltr">The ultimate purpose of this page is to inform you of our policies regarding the collection, use, and disclosure of Personal Information we receive from users of the Site.</p>

<p dir="ltr">APKTrending is committed to using your Personal Information only for providing and improving the Site as We believe that greater protection of personal privacy on the Web will not only protect consumers but also build consumer trust and ultimately boost their participation in online activities. By using the Site, you agree to the collection and use of information in accordance with this policy.</p>


<p dir="ltr"><strong>Information Collection And Use</strong></p>



<p dir="ltr">While using our Site, we may ask you to voluntarily provide us with certain personally identifiable information that can be used to contact or identify you. Personally, identifiable information may include but is not limited to your name (&quot;Personal Information&quot;). The types of information we collect will be diverse depending on the services you use, how you use them and what you choose to provide. It can be your name, address, telephone number, or demographic information, etc. Once you visit our website, leave a comment, register your name or email address, your information will be automatically collected, even when you access our website anonymously.</p>



<p dir="ltr"><strong>Log Data</strong></p>



<p dir="ltr">Like many site operators, we collect log certain anonymous information that your browser sends whenever you visit our Site (&quot;Log Data&quot;).</p>


<p dir="ltr">This Log Data may include information such as your computer&#39;s Internet Protocol (&quot;IP&quot;) address, browser type, browser version, the pages of our Site that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>


<p dir="ltr"><strong>Cookies</strong></p>



<p dir="ltr">Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer&#39;s hard drive.</p>



<p dir="ltr">Like many sites, we do use &quot;cookies&quot; to collect information to improve the overall functionality of our website, as well as provide a better web experience for users. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Site.</p>



<p dir="ltr"><strong>Security</strong></p>


<p dir="ltr">The security of your Personal Information is important to us, and we are committed to protecting the Personal Information we collect, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to keep your Personal Information secure, we cannot guarantee its absolute security.</p>



<p dir="ltr"><strong>Disclosure to Third Parties</strong></p>



<p dir="ltr">We do not sell, share or trade personal information with any third party unless you consent to such sharing at the time you provide your Personal Information. However, this does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you because those parties have already reached an agreement to keep this information confidential. In other words, non-personally identifiable information such as aggregated statistics or demographics of users will be disclosed to other third parties only for the purpose of delivering targeted ads.</p>



<p dir="ltr"><strong>Third-party links</strong></p>



<p dir="ltr">Our website may contain links to other websites or services for which we are not entirely responsible. If you click inserted links to access other third-party websites, your personal information, therefore, may be collected in accordance with their own privacy policy but ours. Thus, we strongly encourage you to thoroughly review the privacy policy before making the decision to submit your personal information.</p>



<p dir="ltr"><strong>Children&#39;s Online Privacy Protection Act Compliance</strong></p>



<p dir="ltr">In compliance with COPPA, APKTrending guarantees that we do not knowingly retain any personal information from children under the age of 13. In case you think your child accidentally submitted this kind of information, we highly recommend you to contact us promptly so that we can take immediate action to remove such information from our storage.&nbsp;</p>


<p dir="ltr"><strong>Privacy Policy&rsquo;s Changes</strong></p>



<p dir="ltr">This Privacy Policy is effective as of July 9, 2017, and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.</p>



<p dir="ltr">We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>



<p dir="ltr"><strong>If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us or by placing a prominent notice on our website.</strong></p>



<p dir="ltr"><strong>Contact Us</strong></p>



<p dir="ltr">We actively invite your feedback and contribution to help us further enhance the quality of our website. If you have any questions about this Privacy Policy, please do not hesitate to contact us via email at apktrending.contact@gmail.com.</p>
