<h1 dir="ltr"><strong>DMCA Disclaimer</strong></h1>

<p dir="ltr">APKTrending is a third-party website which is dedicated to only providing pure and original files from Google Play without any modifications. All trademarks, service marks, registered trademarks, product names or company logos displaying on our website belong to their respective owners.</p>

<p dir="ltr">In compliance with the federal Digital Millennium Copyright Act (DMCA), we are prepared to promptly respond to any notices of alleged copyright infringement reported to our website-APKTrending.com. If you are a copyright owner or a person authorized to act on its behalf claiming that your products have been violated, please contact and submit a copyright infringement notice to our website by the means of providing the APKTrending copyright representative with the following information:&nbsp;</p>

<ul style="list-style-type: disc;padding-left: 40px;">
	<li dir="ltr">
	<p dir="ltr">A physical or electronic signature of a person authorized to act on behalf of the owner of the copyright that has been allegedly infringed upon.</p>
	</li>
	<li dir="ltr">
	<p dir="ltr">Identification of the copyrighted work claimed to have been infringed, or if multiple copyrighted works are covered by a single notification, a representative list of such works at that site.</p>
	</li>
	<li dir="ltr">
	<p dir="ltr">Identification of the material is claimed to be infringing upon the developer&rsquo;s property, including information related to the location of the infringing material with sufficient detail so that our website APKTrending has the potential to verify its existence.</p>
	</li>
	<li dir="ltr">
	<p dir="ltr">Sufficient contact information about the reporting party including the name of the copyright owner, the name and title of the person contacting the web host on the owner&rsquo;s behalf, the address, telephone number and, if available, e-mail address.</p>
	</li>
	<li dir="ltr">
	<p dir="ltr">A statement that the complainant believes the material used is not authorized by the copyright owner, its agent, or the law.</p>
	</li>
	<li dir="ltr">
	<p dir="ltr">A statement indicates that the information notified is accurate and, under penalty of perjury, the complaining party is authorized to act on the behalf of the copyright owner that is allegedly violated.</p>
	</li>
</ul>

<p dir="ltr">Please note that under Section 512(f) of the DMCA, anyone who knowingly misrepresents that material or activity is infringing may be subject to liability for damages.</p>
