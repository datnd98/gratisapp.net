<header>
	<div class="container">
		<div id="header" class="row">
			<div class="col-md-4">
				<div class="logo">
					<div class="mg-0  hidden-sm hidden-xs"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logoWeb.png" alt="" class="logo"></a></div>
					<div class="mg-0 logo_mobile hidden-md hidden-lg"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/Logo_res.png" alt=""></a></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form_search">
					<form action="<?php echo base_url('search'); ?>" class="search_form">
						<input type="text" placeholder="Keyword, Package Name or Google Play URL..." id="input_search" onkeyup="livesearch(this.value)" autocomplete="off" name="q">
						<button class="btn_submit" type="submit">
							<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M8.97683 0C4.0298 0 0 4.02842 0 8.97378C0 13.9191 4.0298 17.9554 8.97683 17.9554C11.0898 17.9554 13.0332 17.2151 14.5691 15.9861L18.3083 19.722C18.497 19.9029 18.749 20.0026 19.0104 19.9999C19.2718 19.9973 19.5217 19.8924 19.7067 19.7078C19.8916 19.5231 19.9969 19.2735 19.9999 19.0122C20.003 18.7509 19.9035 18.4988 19.7229 18.3099L15.9837 14.572C17.2142 13.0343 17.9556 11.0885 17.9556 8.97378C17.9556 4.02842 13.9238 0 8.97683 0ZM8.97683 1.99463C12.8455 1.99463 15.9584 5.10641 15.9584 8.97378C15.9584 12.8412 12.8455 15.9608 8.97683 15.9608C5.10813 15.9608 1.99528 12.8412 1.99528 8.97378C1.99528 5.10641 5.10813 1.99463 8.97683 1.99463Z" fill="#999999" />
							</svg>
						</button>
					</form>

				</div>
			</div>
			<div class="col-md-4 ">
				<div class="main-nav text-right">
					<div class="hamburger-menu btn_show_menu" onclick="show_menu()" id="btn_show_menu">
						<div class="hamburger">
							<span class="hamburger_line_1"></span>
							<span class="hamburger_line_2"></span>
							<span class="hamburger_line_3"></span>
						</div>
					</div>
					<div class="menu-site" id="menu-site">
						<div class="menu-box" id="menu-box">
							<div class="bg-menu  hidden-md hidden-lg " onclick="show_menu()"></div>
							<div class="close hidden-md hidden-lg hidden-sm" onclick="show_menu()">
								<svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M0.488365 9.99991C0.39178 9.99992 0.29736 9.9713 0.217047 9.91765C0.136734 9.864 0.0741367 9.78773 0.0371729 9.6985C0.00020916 9.60927 -0.00946044 9.51108 0.00938724 9.41635C0.0282349 9.32162 0.0747531 9.23461 0.143058 9.16633L9.16635 0.143031C9.25793 0.0514498 9.38214 0 9.51166 0C9.64117 0 9.76538 0.0514498 9.85696 0.143031C9.94855 0.234612 10 0.358823 10 0.488338C10 0.617853 9.94855 0.742064 9.85696 0.833645L0.833672 9.85694C0.788367 9.90234 0.73454 9.93834 0.675281 9.96287C0.616023 9.98741 0.552501 9.99999 0.488365 9.99991Z" fill="white" />
									<path d="M9.51169 9.99991C9.44756 9.99999 9.38403 9.98741 9.32477 9.96287C9.26552 9.93834 9.21169 9.90234 9.16638 9.85694L0.143092 0.833645C0.0515108 0.742064 6.10352e-05 0.617853 6.10352e-05 0.488338C6.10352e-05 0.358823 0.0515108 0.234612 0.143092 0.143031C0.234673 0.0514498 0.358884 0 0.488399 0C0.617914 0 0.742125 0.0514498 0.833706 0.143031L9.857 9.16633C9.9253 9.23461 9.97182 9.32162 9.99067 9.41635C10.0095 9.51108 9.99985 9.60927 9.96288 9.6985C9.92592 9.78773 9.86332 9.864 9.78301 9.91765C9.7027 9.9713 9.60828 9.99992 9.51169 9.99991Z" fill="white" />
								</svg>

							</div>
							<ul class="main-menu">
								<li class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
								<li class=""><a href="<?php echo base_url('apps'); ?>">Apps</a></li>
								<li class=""><a href="<?php echo base_url('games'); ?>">Games</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div id="search_results" class="input_search"></div>
		</div>

	</div>
</header>