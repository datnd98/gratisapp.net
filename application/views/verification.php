<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url() ?>assets/css/loading-bar.css"/>
<script type="text/javascript" src="<?php echo asset_url() ?>assets/js/loading-bar.js"></script>
<!-- START CONTENT -->
<?php
header("Access-Control-Allow-Origin: https://verify.apktrending.com");
?>
<div class="content">
	<!-- Stasrt Upload file apk -->

	<div class="upload">
		<div class="upload__titile">
			<p class="upload__text">Signature Verification Online</p>
			<img src="<?php echo asset_url() ?>assets/images/safe.png" alt="safe">
		</div>
		<div class="upload__apk">
			<span style="width: 146px;height: 146px;position: relative;margin: 0 auto;border: dashed 5px #959595;">
				<input style="opacity: 0;filter: alpha(opacity=0);width: 140px;height: 140px;position: absolute;display: block;cursor: pointer;" style="" type="file" id="file_upload" value="CLICK UPLOAD APK FILE" accept=".apk">
				<img style="height: 140px;line-height: 140px;width: 140px;" src="<?php echo asset_url() ?>assets/images/uploadbtn.webp" alt="uploadbtn">
			</span>
		</div>
		<div class="upload__max-file-size">
			<div id="modal_synchronized_order" style="display: none" class="modal-ld" role="dialog" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog" style="position: relative;">
					<img id="spinner1" data-src="<?php echo asset_url() ?>assets/images/spinner.svg" alt="Loadding" class="lazyload">
					<div class="modal-content">
						<div class="text-center">
							<div id="progress_bar_order"
								 class="ldBar label-center"
								 style="width:60%;height:60%;margin:auto"
								 data-value="0"
								 data-preset="text"
							>
							</div>
						</div>
					</div>
				</div>
			</div>
			<h3 style="font-weight: 1000 !important;text-align: center;color: #24cd77;" id="loading"></h3>
			<p class="upload__text">Maximum file size: 100MB</p>
			<div id="details-apk">
				APK Signature verification will verify Android app package signature to keep Android 
				users away from potentially harmful APK files before APK installing.
			</div>
		</div>
		<div id="list-danger"></div>
		<div class="verifiinfo-box" id="alert-shield" style="display: none;">
			<output id="list-shield"></output>
			<img src="<?php echo asset_url() ?>assets/images/safe.png" alt="safe2">
		</div>
	</div>


	<!-- End Upload file apk -->

	<!-- Start Comment part -->

	<div class="comment" style="">
		<div class="comment__wrapper">
			<div class="comment__content">
				<p>Leave a comment</p>
				<form action="#" method="post" id="comment">
					<textarea rows="5" placeholder="Message..." required></textarea>
				</form>
				<div class="comment__action">
					
					<div class="comment__btn-submit">
						<button type="submit" form="comment" value="Submit">COMMENT</button>
					</div>
					<div class="comment__heading">
						<p>2 Comments</p>
					</div>
					<div class="comment__list">
						<div class="comment__list-parent">
							<div class="comment__ava">
								<img src="<?php echo asset_url() ?>assets/images/avatar/avatar-3.jpg" alt="ava1">
							</div>
							<div class="comment__detail">
								<div class="comment__detail-info">
									<a href="#">
										<p class="comment__detail-name">Ms.Hoa</p>
									</a>
									<p class="comment__detail-time">12 October 2016, 20:05</p>
								</div>
								<p class="comment__cmt">The most well-known dummy text is the 'Lorem Ipsum', which
									is said originated the 16th centuryThe most well-known dummy text is the 'Lorem Ipsum', which
									is said originated the 16th centuryThe most well-known dummy text is the 'Lorem Ipsum', which
									is said originated the 16th centuryThe most well-known dummy text is the 'Lorem Ipsum', which
									is said originated the 16th centuryThe most well-known dummy text is the 'Lorem Ipsum', which
									is said originated the 16th centuryThe most well-known dummy text is the 'Lorem Ipsum', which
									is said originated the 16th centuryThe most well-known dummy text is the 'Lorem Ipsum', which
									is said originated the 16th century.</p>
								<a href="" class="comment__reply">Reply</a>
							</div>
						</div>
						<div class="comment__list-parent">
							<div class="comment__ava">
								<img src="<?php echo asset_url() ?>assets/images/avatar/avatar-2.jpg" alt="ava1">
							</div>
							<div class="comment__detail">
								<div class="comment__detail-info">
									<a href="#"><a href="#"><a href="#">
												<p class="comment__detail-name">Ms.Hoa</p>
											</a></a></a>
									<p class="comment__detail-time">12 October 2016, 20:05</p>
								</div>
								<p class="comment__cmt">The most well-known dummy text is the 'Lorem Ipsum', which
									is
									said originated the 16th century.</p>
								<a href="" class="comment__reply">Reply</a>
							</div>
						</div>
						<div class="comment__list-child">
							<div class="comment__ava">
								<img src="<?php echo asset_url() ?>assets/images/avatar/avatar-3.jpg" alt="ava1">
							</div>
							<div class="comment__detail">
								<div class="comment__detail-info">
									<a href="#"><a href="#">
											<p class="comment__detail-name">Ms.Hoa</p>
										</a></a>
									<p class="comment__detail-time">12 October 2016, 20:05</p>
								</div>
								<p class="comment__cmt">The most well-known dummy text is the 'Lorem Ipsum', which
									is
									said originated the 16th century.</p>
								<a href="" class="comment__reply">Reply</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- End Comment part -->

	</div>
</div>

<!-- END CONTENT -->
<script>
	function getExtension(filename) {
		var parts = filename.split('.');
		return parts[parts.length - 1];
	}
	$(function () {
		$("#file_upload").change(function(e){
			var data = new FormData();
			// var files = $('#file_upload')[0].files[0];
			var files = $('#file_upload').prop('files')[0];

			var type = getExtension(files.name);
			// console.log(type);


			data.append('file',files);

			if (files.size > 104857600) {
				$('#alert-danger').show();
				$('#alert-shield').hide();
				document.getElementById('list-danger').innerHTML =
					'<ul>' +
						'<li><strong style="color:red"> Max file size larger than 100M </strong></li>' +
					'</ul>';
			} else if (type != 'apk') {
				$('#alert-danger').show();
				$('#alert-shield').hide();
				document.getElementById('list-danger').innerHTML =
					'<ul>' +
						'<li><strong style="color:red"> File type is not apk </strong></li>' +
					'</ul>';
			} else {
				uploadFile(data);
			}

		});
	});
	
	function uploadFile(data) {
		$('#list-danger').html("");
		$.ajax({	
			xhr: function() {
				var xhr = new XMLHttpRequest();

				xhr.upload.addEventListener("progress", function(evt) {
					// console.log(event, event.lengthComputable, event.total);
					if (evt.lengthComputable) {
						setTimeout(function(){
							var percentComplete = evt.loaded / evt.total;
							percentComplete = parseInt(percentComplete * 100);
							if (percentComplete == 100) {
								// $('#loading').show();
								$('#loading').html('Processing . . . ');
								$('#modal_synchronized_order').hide();
								$('#file').attr('disable', false);
							}
							var ldBar1 = new ldBar('#progress_bar_order');
							ldBar1.set(percentComplete);
						}, 1000);

					}
				}, false);

				return xhr;
			},
			url: 'https://verify.apktrending.com/upload',
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData:false,
			beforeSend: function () {
				$('#spinner1').show();
				$('#modal_synchronized_order').show();
			},
			complete: function () {
				$('#spinner1').hide();
				$('#loading').html('');
			},
			success: function (result) {
				var result = JSON.parse(result);
				$('#loading').html('');
				// console.log(result.success);
				if (result.success) {
					var html =
						'<ul>' +
							'<li><strong>[Official] This ' + result.name +' APK is the unmodified official version.</strong></li>' +
							'<li>Name packet: '+ result.name_packet +'</li>' +
							'<li>SHA1: '+ result.SHA1 +'</li>' +
							'<li>Signature: '+ result.signature +'</li>' +
							'<li>Size: ' + result.size +'</li>'
						'</ul>';
					$('#alert-shield').show();
					$('#details-apk').hide();
					$('#list-shield').html(html);
				} else {
					$('#alert-danger').show();
					$('#details-apk').hide();
					var html =
						'<ul>' +
							'<li><strong style="color:red">' + result.error +'</strong></li>' +
						'</ul>';
					$('#list-danger').html(html);
				}
			},
		});
	}
</script>
