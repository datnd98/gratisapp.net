<main class="page_home">
    <section class="section_banner text-center mt-4 mb-4">
        <div class="container">
            <a href="#" title="" style="    display: revert;">
                <!--                img_desktop-->
                <img src="<?php echo base_url() ?>assets/images/banner.png" alt="" class="hidden-sm hidden-xs">
                <!--                img_mobile-->
                <img src="<?php echo base_url() ?>assets/images/ads_mobile.png" alt="" class="hidden-lg hidden-md">
            </a>
        </div>
    </section>

    <section class="section_hot_app mt-4">
        <div class="container">
            <!--Show_Hot_App_Desktop-->
            <div class="row hidden-xs">
                <div class=" col-sm-8 col-md-8 col-lg-8">
                    <div class="item_app">
                        <div class="images">
                            <a href="<?php echo base_url($game_obb_hot[0]['urlTitle'] . '/' . $game_obb_hot[0]['appid']) ?>" title="">
                                <img src="<?php
                                            $game_hot = json_decode($game_obb_hot[0]['thumbnails'], true)['image'];
                                            echo $game_hot[1][2] . "=s1200"; ?>-rw" alt="">
                            </a>
                        </div>
                        <div class="description">
                            <h2 class="title"><a href="<?php echo base_url($game_obb_hot[0]['urlTitle'] . '/' . $game_obb_hot[0]['appid']) ?>" title=""><?php echo $game_obb_hot[0]['title']; ?></a></h2>
                            <div class="rankting">
                                <div class="item_rate flex">
                                    <span class="" style="width: <?php echo round($game_obb_hot[0]['score'] / 5 * 100, 2) ?>%;"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end_item_app-->
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <div class="item_app item_right">
                        <div class="images">
                            <a href="<?php echo base_url($game_obb_hot[1]['urlTitle'] . '/' . $game_obb_hot[1]['appid']) ?>" title="">
                                <img src="<?php
                                            $game_hot = json_decode($game_obb_hot[1]['thumbnails'], true)['image'];
                                            echo $game_hot[1][2] . "=s750"; ?>-rw" alt="">
                            </a>
                        </div>
                        <div class="description">
                            <h2 class="title"><a href="<?php echo base_url($game_obb_hot[1]['urlTitle'] . '/' . $game_obb_hot[1]['appid']) ?>" title=""><?php echo $game_obb_hot[1]['title']; ?></a></h2>
                            <div class="rankting">
                                <div class="item_rate flex">
                                    <span class="" style="width: <?php echo round($game_obb_hot[1]['score'] / 5 * 100, 2) ?>%;"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end_item_right-->
                    <div class="item_app item_right">
                        <div class="images">
                            <a href="<?php echo base_url($game_obb_hot[4]['urlTitle'] . '/' . $game_obb_hot[4]['appid']) ?>" title="">
                                <img src="<?php
                                            $game_hot = json_decode($game_obb_hot[4]['thumbnails'], true)['image'];
                                            echo $game_hot[0][2] . "=s750"; ?>-rw" alt="">
                            </a>
                        </div>
                        <div class="description">
                            <h2 class="title"><a href="<?php echo base_url($game_obb_hot[4]['urlTitle'] . '/' . $game_obb_hot[4]['appid']) ?>" title=""><?php echo $game_obb_hot[4]['title']; ?></a></h2>
                            <div class="rankting">
                                <div class="item_rate flex">
                                    <span class="" style="width: <?php echo round($game_obb_hot[4]['score'] / 5 * 100, 2) ?>%;"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end_item_right-->
                </div>
            </div>
        </div>
        <!--Show_Hot_App_Mobile-->
        <div class="container hidden-lg hidden-md hidden-sm">
            <div class="carousel-multiple-items owl_banner_home " id="carousel-multiple-items">
                <?php for ($i = 0; $i < 2; $i++) : ?>
                    <div class="item">
                        <div class="item_app">
                            <div class="images">
                                <a href="<?php echo base_url($game_obb_hot[$i]['urlTitle'] . '/' . $game_obb_hot[$i]['appid']) ?>" title="">
                                    <img src="<?php
                                                $game_hot = json_decode($game_obb_hot[$i]['thumbnails'], true)['image'];
                                                echo $game_hot[0][2] . "=s1200";
                                                ?>-rw" alt="">
                                </a>
                            </div>
                            <div class="description">
                                <h2 class="title"><a href="<?php echo base_url($game_obb_hot[$i]['urlTitle'] . '/' . $game_obb_hot[$i]['appid']) ?>" title=""><?php echo $game_obb_hot[$i]['title']; ?></a></h2>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </section>

    <section class="section_featured mt-5">
        <div class="container">
            <div class="list_featured mb-1">

                <div class="item_featured">
                    <a href="<?php echo base_url('topgame24h') ?>" class="btn_top_view">
                        <span class="icon"><img src="<?php echo base_url('games'); ?>assets/images/Group.png" alt=""></span>
                        <span class="txt">Top Featured Games</span>
                    </a>
                </div>

                <div class="item_featured">
                    <a href="<?php echo base_url('toppopularapps') ?>" class="btn_top_view bg_EDECFE">
                        <span class="icon"><img src="<?php echo base_url('apps'); ?>assets/images/Group1.png" alt=""></span>
                        <span class="txt">Top Featured Apps</span>
                    </a>
                </div>

                <div class="item_featured">
                    <a href="" class="btn_top_view bg_E7F5FF">
                        <span class="icon"><img src="<?php echo base_url() ?>assets/images/Group3.png" alt=""></span>
                        <span class="txt">All Categories</span>
                    </a>
                </div>
            </div>
            <div class="box_line"></div>
        </div>
    </section>
    <section class="section_content_app">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="title-head mb-3">
                        <h2 class="mg-0 title">Top New Apps</h2>
                        <a href="<?php echo base_url('topnewapps') ?>" class="btn_view_more" title="Xem thêm">View all</a>
                    </div>
                    <div class="row list_app">
                        <?php for ($j = 0; $j < sizeof($topapps); $j++) : ?>
                            <div class="col-md-6 col-sm-6">
                                <div class="item_box_app">
                                    <div class="images_app">
                                        <a href="<?php echo base_url($topapps[$j]['urlTitle'] . '/' . $topapps[$j]['appid']) ?>">
                                            <img src="<?php echo $topapps[$j]['cover'] ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="content_right">
                                        <h3 class="title mg-0"><a href="<?php echo base_url($topapps[$j]['urlTitle'] . '/' . $topapps[$j]['appid']) ?>"><?php echo $topapps[$j]['title'] ?></a></h3>
                                        <div class="rankting">
                                            <div class="item_rate flex">
                                                <span class="" style="width: <?php echo round($topapps[$j]['score'] / 5 * 100, 2) ?>%;"></span>
                                            </div>
                                        </div>
                                        <span class="date"><?php
                                                            $today = strtotime(date('Y-m-d'));
                                                            $time = floor(($today - $topapps[$j]['uploaddate']) / 86400);
                                                            if ($time > 365) {
                                                                if (floor($time / 365) > 1) {
                                                                    echo floor($time / 365) . " years ago";
                                                                } else {
                                                                    echo floor($time / 365) . " year ago";
                                                                }
                                                            } elseif($time > 31){
                                                                if (floor($time / 31) > 1) {
                                                                    echo floor($time / 31) . " months ago";
                                                                } else {
                                                                    echo floor($time / 31) . " month ago";
                                                                }
                                                            } elseif($time > 1){
                                                                echo $time . " days ago";
                                                            } elseif($time == 1){
                                                                echo $time . " day ago";
                                                            } else{
                                                                echo floor(($today - $topapps[$j]['uploaddate']) / 3600) . " hours ago";
                                                            }
                                                            ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <div class="box_line"></div>
                    <!--End_List_Apps-->
                    <div class="title-head mb-3">
                        <h2 class="mg-0 title">Top Popular Apps</h2>
                        <a href="<?php echo base_url('toppopularapps') ?>" class="btn_view_more" title="Xem thêm">View all</a>
                    </div>
                    <div class="row list_app list_app_mb">
                        <?php for ($j = 0; $j < sizeof($topapps_hot); $j++) : ?>
                            <div class="col-md-6 col-xs-6">
                                <div class="item_box_app">
                                    <div class="images_app">
                                        <a href="<?php echo base_url($topapps_hot[$j]['urlTitle'] . '/' . $topapps_hot[$j]['appid']) ?>">
                                            <img src="<?php echo $topapps_hot[$j]['cover'] ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="content_right">
                                        <h3 class="title mg-0"><a href="<?php echo base_url($topapps_hot[$j]['urlTitle'] . '/' . $topapps_hot[$j]['appid']) ?>"><?php echo $topapps_hot[$j]['title'] ?></a></h3>
                                        <div class="rankting">
                                            <div class="item_rate flex">
                                                <span class="" style="width: <?php echo round($topapps_hot[$j]['score'] / 5 * 100, 2) ?>%;"></span>
                                            </div>
                                        </div>
                                        <span class="date"><?php
                                                            $today = strtotime(date('Y-m-d'));
                                                            $time = floor(($today - $topapps_hot[$j]['uploaddate']) / 86400);
                                                            if ($time > 365) {
                                                                if (floor($time / 365) > 1) {
                                                                    echo floor($time / 365) . " years ago";
                                                                } else {
                                                                    echo floor($time / 365) . " year ago";
                                                                }
                                                            } elseif($time > 31){
                                                                if (floor($time / 31) > 1) {
                                                                    echo floor($time / 31) . " months ago";
                                                                } else {
                                                                    echo floor($time / 31) . " month ago";
                                                                }
                                                            } elseif($time > 1){
                                                                echo $time . " days ago";
                                                            } elseif($time == 1){
                                                                echo $time . " day ago";
                                                            } else{
                                                                echo floor(($today - $topapps_hot[$j]['uploaddate']) / 3600) . " hours ago";
                                                            }
                                                            ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <div class="box_line"></div>
                    <!--End_List_Apps-->
                    <div class="title-head mb-3">
                        <h2 class="mg-0 title">Top Games in Last 24 Hours</h2>
                        <a href="<?php echo base_url('topgame24h') ?>" class="btn_view_more" title="Xem thêm">View all</a>
                    </div>
                    <div class="row list_app ">
                        <div class="col-md-12">
                            <div class="owl-carousel owl_list_app" id="owl_list_app">
                                <?php for ($j = 0; $j < sizeof($topgames_hot); $j++) : ?>
                                    <div class="item">
                                        <div class="item_box_app">
                                            <div class="images_app">
                                                <a href="<?php echo base_url($topgames_hot[$j]['urlTitle'] . '/' . $topgames_hot[$j]['appid']) ?>">
                                                    <img src="<?php echo $topgames_hot[$j]['cover'] ?>" alt="">
                                                </a>
                                            </div>
                                            <div class="content_right">
                                                <h3 class="title mg-0"><a href="<?php echo base_url($topgames_hot[$j]['urlTitle'] . '/' . $topgames_hot[$j]['appid']) ?>"><?php echo $topgames_hot[$j]['title'] ?></a></h3>
                                                <div class="rankting">
                                                    <div class="item_rate flex">
                                                        <span class="" style="width: <?php echo round($topgames_hot[$j]['score'] / 5 * 100, 2) ?>%;"></span>
                                                    </div>
                                                </div>
                                                <span class="date"><?php
                                                            $today = strtotime(date('Y-m-d'));
                                                            $time = floor(($today - $topgames_hot[$j]['uploaddate']) / 86400);
                                                            if ($time > 365) {
                                                                if (floor($time / 365) > 1) {
                                                                    echo floor($time / 365) . " years ago";
                                                                } else {
                                                                    echo floor($time / 365) . " year ago";
                                                                }
                                                            } elseif($time > 31){
                                                                if (floor($time / 31) > 1) {
                                                                    echo floor($time / 31) . " months ago";
                                                                } else {
                                                                    echo floor($time / 31) . " month ago";
                                                                }
                                                            } elseif($time > 1){
                                                                echo $time . " days ago";
                                                            } elseif($time == 1){
                                                                echo $time . " day ago";
                                                            } else{
                                                                echo floor(($today - $topgames_hot[$j]['uploaddate']) / 3600) . " hours ago";
                                                            }
                                                            ?></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php endfor; ?>
                            </div>
                        </div>
                        <div class="col-md-12 mt-4 mb-4 hidden-lg hidden-md">
                            <div class="banner_mobile ">
                                <a href=""><img src="<?php echo base_url() ?>assets/images/banner_mobile.png" alt="" style="width: 100%;"></a>
                            </div>
                        </div>

                    </div>
                    <div class="box_line hidden-sm hidden-xs"></div>
                    <!--End_List_Apps-->
                    <div class="title-head mb-3">
                        <h2 class="mg-0 title">Top New Games</h2>
                        <a href="<?php echo base_url('topnewgame') ?>" class="btn_view_more" title="Xem thêm">View all</a>
                    </div>
                    <div class="row list_app">
                        <?php for ($j = 0; $j < sizeof($topgames); $j++) : ?>
                            <div class="col-md-6 col-xs-6">
                                <div class="item_box_app">
                                    <div class="images_app">
                                        <a href="<?php echo base_url($topgames[$j]['urlTitle'] . '/' . $topgames[$j]['appid']) ?>">
                                            <img src="<?php echo $topgames[$j]['cover'] ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="content_right">
                                        <h3 class="title mg-0"><a href="<?php echo base_url($topgames[$j]['urlTitle'] . '/' . $topgames[$j]['appid']) ?>"><?php echo $topgames[$j]['title'] ?></a></h3>
                                        <div class="rankting">
                                            <div class="item_rate flex">
                                                <span class="" style="width: <?php echo round($topgames[$j]['score'] / 5 * 100, 2) ?>%;"></span>
                                            </div>
                                        </div>
                                        <span class="date"><?php
                                                            $today = strtotime(date('Y-m-d'));
                                                            $time = floor(($today - $topgames[$j]['uploaddate']) / 86400);
                                                            if ($time > 365) {
                                                                if (floor($time / 365) > 1) {
                                                                    echo floor($time / 365) . " years ago";
                                                                } else {
                                                                    echo floor($time / 365) . " year ago";
                                                                }
                                                            } elseif($time > 31){
                                                                if (floor($time / 31) > 1) {
                                                                    echo floor($time / 31) . " months ago";
                                                                } else {
                                                                    echo floor($time / 31) . " month ago";
                                                                }
                                                            } elseif($time > 1){
                                                                echo $time . " days ago";
                                                            } elseif($time == 1){
                                                                echo $time . " day ago";
                                                            } else{
                                                                echo floor(($today - $topgames[$j]['uploaddate']) / 3600) . " hours ago";
                                                            }
                                                            ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="img_sile_bar text-center hidden-xs hidden-sm">
                        <a href="#" title="">
                            <picture><img src="<?php echo base_url() ?>assets/images/img_ads.png" alt=""></picture>
                        </a>
                    </div>
                    <div class="box_download_app mt-4 mb-4 hidden-xs hidden-sm">
                        <div class="imgage"><a href=""><img src="<?php echo base_url() ?>assets/images/Logo_res.png" alt=""></a></div>
                        <div class="box_content ">
                            <h3 class="title mt-0"><a href="">Install Choilieng App</a></h3>
                            <p>For your faster download experience</p>
                            <a href="" class="btn_defaulf mt-1">Download</a>
                        </div>
                    </div>
                    <div class="box_tag mb-4">
                        <h3 class="title_default mt-0">Hot Tags</h3>
                        <div class="list_tag">
                            <a href="" title="">Among Us</a>
                            <a href="" title="">Tiktok</a>
                            <a href="" title="">Facebook</a>
                            <a href="" title="">GAM</a>
                            <a href="" title="">LOL</a>
                            <a href="" title="">PUBG</a>
                            <a href="" title="">Pinterest</a>
                            <a href="" title="">PUBG</a>
                            <a href="" title="">Pinterest</a>
                            <a href="" title="">Insta</a>
                            <a href="" title="">LOL</a>

                        </div>
                    </div>
                    <div class="box_social">
                        <p>Follow gratisapp.net on</p>
                        <ul class="list_social text-center mt-2">
                            <li><a href="" title="Facebook"><img src="<?php echo base_url() ?>assets/images/Path.svg" alt=""></a></li>
                            <li><a href="" title="Pinterest"><img src="<?php echo base_url() ?>assets/images/Pinterest.svg" alt=""></a></li>
                            <li><a href="" title="Youtube"><img src="<?php echo base_url() ?>assets/images/Youtube.svg" alt=""></a></li>
                            <li><a href="" title="Twitter"><img src="<?php echo base_url() ?>assets/images/Twitter.svg" alt=""></a></li>
                        </ul>
                    </div>
                    <!--banner_mobile-->
                    <div class="mt-4">
                        <a href="" class="hidden-lg hidden-md "><img src="<?php echo base_url() ?>assets/images/banner_mobile_bt.png" alt="" style="width: 100%">
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>