<main>
	<section id="apk-box-01" class="apk-box apk-box--detail py-3">
		<div class="container">
			<nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white p-0">
                <li class="breadcrumb-item"><a href="/"><i class="fat fat-home"></i> Home</a></li>
                <li class="breadcrumb-item">Download <?php echo $title ?></li>
            </ol>
            </nav>
            
    		<div class="row">
    			<?php $this->load->view($page); ?>
    		</div>
		</div>
	</section>
</main>