<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="footer-main">
                    <ul class="widget">
                        <li><a href="<?php echo base_url() ?>">Home</a></li>
                        <li><a href="<?php echo base_url('about') ?>">Contact us</a></li>
                        <li><a href="<?php echo base_url('privacy-policy') ?>">Privacy</a></li>
                    </ul>
                    <div class="line"></div>
                    <p class="copyright">© <a href="<?php echo base_url() ?>">gratisapp.net</a> - 2020</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo base_url() ?>assets/js/tiny-slider.js"></script>
<script src="<?php echo base_url() ?>assets/js/index.min.js"></script>

<script>
    function show_menu() {
        var element = document.getElementById("menu-site");
        var y = document.getElementById("menu-box");
        element.classList.toggle("opened");
        y.classList.toggle('opened');
    }

    function showResults() {
        var div = document.getElementById('search_results')
        if (document.getElementById("input_search").value.length > 0) {
            div.classList.add("opened")
        } else {
            div.classList.remove("opened");
        }
    }

    function showResults1() {
        var div = document.getElementById('select-dropdown__list-1')
        if (document.getElementById("input_drive").value.length > 0) {
            div.classList.add("active")
        } else {
            div.classList.remove("active");
        }
    }
    //keydown
    var ul = document.getElementById('menu_results');
    var liSelected;
    var index = -1;

    document.addEventListener('keydown', function(event) {
        var len = ul.getElementsByTagName('li').length - 1;
        if (event.which === 40) {
            index++;
            //down
            if (liSelected) {
                removeClass(liSelected, 'selected');
                next = ul.getElementsByTagName('li')[index];
                if (typeof next !== undefined && index <= len) {

                    liSelected = next;
                } else {
                    index = 0;
                    liSelected = ul.getElementsByTagName('li')[0];
                }
                addClass(liSelected, 'selected');
                console.log(index);
            } else {
                index = 0;

                liSelected = ul.getElementsByTagName('li')[0];
                addClass(liSelected, 'selected');
            }
        } else if (event.which === 38) {

            //up
            if (liSelected) {
                removeClass(liSelected, 'selected');
                index--;
                console.log(index);
                next = ul.getElementsByTagName('li')[index];
                if (typeof next !== undefined && index >= 0) {
                    liSelected = next;
                } else {
                    index = len;
                    liSelected = ul.getElementsByTagName('li')[len];
                }
                addClass(liSelected, 'selected');
            } else {
                index = 0;
                liSelected = ul.getElementsByTagName('li')[len];
                addClass(liSelected, 'selected');
            }
        } else if (event.which === 13) {
            chose = ul.getElementsByTagName('li')[index]
            chose.children[0].click()
        }
    }, false);

    function removeClass(el, className) {
        if (el.classList) {
            el.classList.remove(className);
        } else {
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    };

    function addClass(el, className) {
        if (el.classList) {
            el.classList.add(className);
        } else {
            el.className += ' ' + className;
        }
    };
</script>

</body>

</html>