<style type="text/css">
*, *::before, *::after {
    box-sizing: border-box;
}
main{
			min-height: 76.9vh;
		}
	@media only screen and (max-width: 1024px){
		#apk-box-01 .apk-sidebar {
		margin-top: 0;
		display: none;
	}
}
.link_blue{
	text-decoration: none;
	color: #4f87de;
	font-weight: 500;
}
.search_title{
	display: flex;
	justify-content: space-around;
	align-items: center;
	height: 50px;
	border-bottom: 1px solid #eee;
	border-top: 1px solid #eee;
	margin-left: -15px;
	margin-right: -15px;
	margin-bottom: 25px;
}
.search_item{
	font-size: 16px;
	font-weight: 500;
	/* border-right: 1px solid #CCC; */
}
.search_game{
	/* border-right: 1px solid #CCC; */
	width: 50%;
	/* border-right: 1px solid #CCC; */
	text-align: center;
}
.search_dev{
	width: 50%;
	border-left: 1px solid #CCC;
	text-align: center;
}
.rating_score{
	font-size: 14px;
}
#search-top-mobile{
	white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
#form-mb button{
	display: none;
}
		@media screen and (min-width: 1088px){
		  .container {
		      max-width: 960px;
		      width: 960px;
		  }
		}

		@media screen and (min-width: 1280px){
		  .container {
		      max-width: 1152px;
		      width: 1152px;
		  }
		}
		@media screen and (min-width: 1472px){
		  .container {
		      max-width: 1176px;
		      width: 1176px;
		  }
		}
		body{
			margin:0;
			overflow-x: hidden;
			font-family: "Poppins";

			/* font-size: 0.875rem;
			font-weight: 400;
			line-height: 1.7;
			color: #666666;
			text-align: left;
			background-color: #fff; */
		}
		a{
			text-decoration: none;
		}
		img {
		    max-width: 100%;
		    height: auto;
		    vertical-align: middle;
		    border: 0;
		}
		.container {
		    margin: 0 auto;
		    position: relative;
		    padding-right: 15px;
			padding-left: 15px;
		}
		.row {
		    display: flex;
		    flex-wrap: wrap;
		    margin-right: -15px;
		    margin-left: -15px;
		}
		.header-mobile{
			display: none;
		}
		.search-mobile{
			display: none;
		}
		.menu-mobile {
		    display: none;
		}
		.align-items-center {
		    align-items: center !important;
		}

		.d-flex {
		    display: flex !important;
		}
		header * {
		    color: #fff;
		}
		header {
		    background-color: #4f87de;
		}
		article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
		    display: block;
		}
	/*	.search-auto-complete{
			display: none;
		}*/
		@media (min-width: 768px){
			.apk-logo {
			    padding: 0;
			}
			.text-md-center {
			text-align: center !important;
		}
		}
		.apk-logo {
		    position: relative;
	        padding: 5px 0;
		}
		.apk-search.input-group, #search-top {
		    border-radius: 5px;
		    border: 0px;
			white-space: nowrap;
			/*overflow: hidden;*/
			text-overflow: ellipsis;
		}
		.apk-search {
		    overflow: inherit;
		    position: relative;
		    background-color: #fff;
		    overflow: hidden;
		    border-radius: 0.25rem;
		}
		.apk-search input {
		    padding-right: 37px;
		}
		.form-control {
		    display: block;
		    width: 100%;
		    /*height: calc(2.2375rem + 2px);*/
		    padding: 0.375rem 0.75rem;
		    font-size: 0.875rem;
		    line-height: 1.7;
		    color: #495057;
		    background-color: #fff;
		    background-clip: padding-box;
		    border: 1px solid #ced4da;
		    border-radius: 0.25rem;
		    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.input-group {
		    position: relative;
		    display: flex;
		    flex-wrap: wrap;
		    align-items: stretch;
		    width: 100%;
		}

		.input-group > .form-control, .input-group > .custom-select, .input-group > .custom-file {
		    position: relative;
		    flex: 1 1 auto;
		    width: 1%;
		    margin-bottom: 0;
		}
		.btn:not(:disabled):not(.disabled) {
		    cursor: pointer;
		}
		.apk-search .btn {
		    position: absolute;
		    top: 50%;
		    right: 0;
		    z-index: 3;
		    width: 37px;
		    -webkit-transform: translateY(-50%);
		    transform: translateY(-50%);
		}
		.btn-link:hover {
		    color: #2360bd;
		    text-decoration: none;
		    background-color: transparent;
		    border-color: transparent;
		}
		.btn-link {
		    font-weight: 400;
		    color: #4f87de;
		    background-color: transparent;
		}
		.btn {
		    display: inline-block;
		    font-weight: 400;
		    text-align: center;
		    white-space: nowrap;
		    vertical-align: middle;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    border: 1px solid transparent;
		    padding: 0.375rem 0.75rem;
		    font-size: 0.875rem;
		    line-height: 1.7;
		    border-radius: 0.25rem;
		    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.col-md-2,.col-xl-5, .col-lg-4,.col-lg-8,.col-xl-9,.col-xl-3,.col-xl-4,.col-md-12  {
		    position: relative;
		    width: 100%;
		    min-height: 1px;
		    padding-right: 15px;
		    padding-left: 15px;
		}
		.collapse:not(.show) {
		    display: none;
		}
		.navbar-collapse {
		    flex-basis: 100%;
		    flex-grow: 1;
		    align-items: center;
		}
		.ml-auto, .mx-auto {
		    margin-left: auto !important;
		}
		.navbar-nav {
		    display: flex;
		    flex-direction: column;
		    padding-left: 0;
		    margin-bottom: 0;
		    list-style: none;
		    margin-top: 4px;
		}
		.nav-link {
		    display: block;
		    padding: 0.5rem 1rem;
		}
		.dropdown:hover>.dropdown-menu {
		    display: block;
		}
		.navbar-nav .dropdown-menu {
		    position: static;
		    float: none;
		}
		.dropdown-menu {
		    z-index: 1000;
		    display: none;
		    float: left;
		    min-width: 10rem;
		    padding: 0.5rem 0;
		    margin: 0.125rem 0 0;
		    font-size: 0.875rem;
		    color: #666666;
		    text-align: left;
		    list-style: none;
		    background-color: #fff;
		    background-clip: padding-box;
		    border: 1px solid rgba(0, 0, 0, 0.15);
		    border-radius: 0.25rem;
		}
		.dropdown-item:active {
		    color: #fff;
		    text-decoration: none;
		    background-color: #007bff;
		}
		.dropdown-item:hover, .dropdown-item:focus {
		    color: #16181b;
		    text-decoration: none;
		    background-color: #f8f9fa;
		}
		a:hover {
		    color: #2360bd;
		    text-decoration: none;
		}
		.dropdown-item {
		    display: block;
		    padding: 0.25rem 1.5rem;
		    clear: both;
		    font-weight: 400;
		    color: #212529;
		    text-align: inherit;
		    white-space: nowrap;
		    background-color: transparent;
		    border: 0;
		}
		.apk-downloader .apk-downloader__title {
		    font-size: 15px;
		    margin-bottom: 8px;
		}
		.text-color-1 {
		    color: #4f87de !important;
		}
		.font-weight-bold {
		    font-weight: 700 !important;
		}
		.text-uppercase {
		    text-transform: uppercase !important;
		}
		.apk-box {
		    padding:  0;
			margin-top: 50px;
		}
		.apk-downloader img {
		    border-radius: 5px;
		    margin-top: 3px;
		}
		.icon-feature {
		    width: 25px;
		    height: 23px;
		    margin-right: 10px;
		    margin-top: 2px;
		    float: left;
		}
		.apk-app {
		    margin-top: 30px;
			display: flex;
		}
		.apk-app__img {
		    width: 100px;
		    overflow: hidden;
		    border-radius: 0.25rem;
		}
		.mr-4, .mx-4 {
		    /*margin-right: 1.5rem !important;*/
		}
		.apk-app__content {
		    /* padding-left: 124px; */
			overflow: hidden;
		    border-radius: 0.25rem;
			margin-left: 15px;
		}
		.float-left {
		    float: left !important;
		}
		.apk-app__author {
		    font-size: 13px;
		}
		.font-weight-light {
		    font-weight: 300 !important;
		}
		.apk-app__title{
			margin:0;
			margin-bottom: 4px;
		}
		.apk-app__published {
		    font-size: 12px;
		}
		.text-muted {
		    color: #6c757d !important;
		}
		.star-rating {
		    display: inline-block;
		    position: relative;
		}
		.star-rating::before {
		    font-size: 15px;
		    content: "★★★★★";
		}
		.star-rating__size::before {
		    font-size: 15px;
		    color: #fcd700;
		    content: "★★★★★";
		}
		.star-rating__size {
			display: inline-block;
			position: absolute;
			top: 0;
			left: 0;
			z-index: 1;
			width: 0;
			overflow: hidden;
		}
		.apk-box__title {
		    position: relative;
		    z-index: 1;
		    background-color: #fff;
		}
		#apk-box-04 .apk-box__title{
		    background-color: #72c874;
		    color: #fff;
		}
		.font-weight-normal {
		    font-weight: 400 !important;
		}
		.clearfix::after {
		    display: block;
		    clear: both;
		    content: "";
		}
		.border-bottom {
		    border-bottom: 1px solid #dee2e6 !important;
		}
		.apk-socials label {
			font-size: 18px;
			font-weight: 500;
		}
		.apk-socials img {
		height: 100%;
		width: auto;
	}
		.font-weight-bold {
		    font-weight: 700 !important;
		}

		.mb-0, .my-0 {
		    margin-bottom: 0 !important;
		}
		.d-block {
		    display: block !important;
		}
		.mb-lg-3 {
		    margin-bottom: 0.25rem !important;
		}
		.apk-app__title a{
			font-size:15px;
			display: block;
		}
		.border-top {
		    border-top: 1px solid #dee2e6 !important;
		}
		.apk-box__content {
		    position: relative;
		    margin-top: -7px;
		}
		.apk-app__title a:hover, .apk-app__title a:focus,.apk-app__author a:hover, .apk-app__author a:focus {
		    color: #4f87de;
		}
		.apk-app__title a,.apk-app__author a{
		    color: #333333;
		}
		/*live search*/
		.apk-search {
		    overflow: inherit;
		    position: relative;
		}
		.search-auto-complete{
			display: none;
		}
		.show-livesearch{
			/* display: block; */
			/* display: none; */
		}
		.search-auto-complete {
		    position: absolute;
		    background-color: #fff;
		    z-index: 99;
		    top: 35px;
		    width: 100%;
		    -webkit-box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
		    -moz-box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
		    box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
		}
		.search-auto-complete ul {
		    padding: 0px;
		    margin-top: 0;
	        margin-bottom: 1rem;
            list-style: none;
		}	
		.search-auto-complete ul li {
		    padding: 10px 0px;
		    border-bottom: 1px solid #e0e0e0;
		}
		.search-auto-complete ul .apk-app .apk-app__img {
		    width: 50px;
		    margin-right: 0px !important;
		}
		.search-auto-complete ul .apk-app .apk-app__content {
			/*padding-left: 65px;*/
		}
		.search-auto-complete ul .apk-app .apk-app__author{
		  color: #666;
		  margin-top: -5px;
		}
		.search-auto-complete .apk-app{
			margin-top:0px;
		}
		.search-auto-complete ul li:last-child {
		    border-bottom: 0px;
		}
		/*end live search*/

		.apk-box--full .apk-apps__wrapper .apk-app {
		    position: relative;
		    padding: 5px;
		    margin-top: 0;
		    width: 100%;
		}

		.apk-box--full .apk-apps__wrapper .apk-app__img a::after {
		    content: "";
		    transition: all 0.2s ease-in-out;
		    top: 0;
		    left: 0;
		    right: 0;
		    bottom: 0;
		    position: absolute;
		    background-color: rgba(0, 0, 0, 0.5);
		    z-index: 0;
		    opacity: 0;
		    visibility: hidden;
		}
		.apk-box--full .apk-apps__wrapper .apk-app__img {
		    position: relative;
		    width: 100%;
		    margin-right: 0 !important;
		}
		.apk-app__img {
		    width: 100px;
		    overflow: hidden;
		    border-radius: 0.25rem;
		}
		.apk-box--full .apk-apps__wrapper .apk-app--small .apk-app__content {
		    display: none;
		}
		.apk-box--full .apk-apps__wrapper .apk-app__content {
		    position: absolute;
		    top: auto;
		    right: 5px;
		    bottom: 5px;
		    left: 5px;
		    z-index: 1;
		    padding: 15px 20px;
		    color: #fff;
		    overflow: hidden;
		    background: rgba(0, 0, 0, 0);
		    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(0, 0, 0, 0)), color-stop(0%, rgba(0, 0, 0, 0)), color-stop(30%, rgba(0, 0, 0, 0)), color-stop(90%, rgba(0, 0, 0, 0.85)), color-stop(100%, rgba(0, 0, 0, 0.85)));
		    background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0) 30%, rgba(0, 0, 0, 0.85) 90%, rgba(0, 0, 0, 0.85) 100%);
		    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#000000', endColorstr='#000000', GradientType=0 );
		}
		.apk-box--full .apk-apps__wrapper .apk-app__content a {
		    color: #fff;
		}
		.apk-box--lightgreen.apk-box--full {
		    background-color: #72c874;
		}
		.apk-box--full {
		    background-image: none;
		}
		.apk-apps .apk-app__title a{
			font-weight: 500;
		}
		footer {
		    background-color: #333333;
		    padding: 1em 0;
		}
		.text-white {
		    color: #fff !important;
		}
		.apk-nav-footer li:first-child, .apk-nav-footer li:last-child {
		    margin-left: 0;
		    border-right-color: transparent;
		}
		.apk-nav-footer a {
		    font-size: 13px;
		}
		.apk-nav-footer a:hover {
		    opacity: 0.5;
		}
		.apk-nav-footer li {
		    padding: 8px 0;
		    border-bottom: 1px solid #666666;
		}
		footer .col-md-8{
			position: relative;
		    width: 100%;
		    min-height: 1px;
		    padding-right: 15px;
		    padding-left: 15px;
		}
		ul{
			list-style: none;
			padding: 0px;
		}
		.pt-5, .py-5 {
		    padding-top: 3rem !important;
		}
		.justify-content-center {
		    justify-content: center !important;
		}
		.pagination {
		    display: flex;
		    padding-left: 0;
		    list-style: none;
		    border-radius: 0.25rem;
		}
		.sr-only {
		    position: absolute;
		    width: 1px;
		    height: 1px;
		    padding: 0;
		    overflow: hidden;
		    clip: rect(0, 0, 0, 0);
		    white-space: nowrap;
		    border: 0;
		}
		.page-item:first-child .page-link {
		    border-top-left-radius: 0px;
		    border-bottom-left-radius: 0px;
		}
		.page-item.active .page-link {
		    z-index: 1;
		    color: #fff;
		    background-color: #4f87de;
		    border-color: #4f87de;
		}
		.page-item.active .page-link {
		    z-index: 1;
		    color: #fff;
		    background-color: #007bff;
		    border-color: #007bff;
		}
		.page-item:first-child .page-link {
		    margin-left: 0;
		    border-top-left-radius: 0.25rem;
		    border-bottom-left-radius: 0.25rem;
		}
		.page-link:not(:disabled):not(.disabled) {
		    cursor: pointer;
		}
		.page-link:hover {
		    z-index: 2;
		    color: #2360bd;
		    text-decoration: none;
		    background-color: #e9ecef;
		    border-color: #dee2e6;
		}
		.page-link {
		    position: relative;
		    display: block;
		    padding: 0.5rem 0.75rem;
		    margin-left: -1px;
		    line-height: 1.25;
		    color: #4f87de;
		    background-color: #fff;
		    border: 1px solid #dee2e6;
		}
		.btn-burger {
		    position: absolute;
		    top: 3px;
		    left: 0;
		    display: block;
		    width: 46px;
		    height: 46px;
		    border-radius: 50%;
		    cursor: pointer;
		    z-index: 100;
		}
		.search-btn img{
			padding-top: 15px;
		}
		.burger-line-wrap {
			position: absolute;
    left: 50%;
    top: 46%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    width: 21px;
    height: 14px;
		}
		.btn-burger.clicked .burger-line:first-of-type {
		    top: 47%;
		    -webkit-transform: rotate(135deg) translate(-1px,0);
		    transform: rotate(135deg) translate(-1px,0);
		}
		.btn-burger.clicked .burger-line:last-of-type {
		    bottom: 47%;
		    -webkit-transform: rotate(-135deg) translate(-1px,0);
		    transform: rotate(-135deg) translate(-1px,0);
		}
		.btn-burger.clicked .burger-line:nth-of-type(2) {
		    opacity: 0;
		}
		.burger-line:first-of-type {
		    top: 0;
		    left: 0;
		}
		.burger-line:nth-of-type(2) {
		    top: 47%;
		    left: 0;
		    opacity: 1;
		}
		.burger-line:last-of-type {
		    bottom: 0;
		    left: 0;
		}
		.burger-line {
		    display: block;
		    position: absolute;
		    width: 100%;
		    height: 1px;
		    background: #fff;
		    transform-origin: center center;
		    transition: all .2s ease-in-out;
		}
		.apk-socials a {
		    display: inline-block;
		    font-size: 16px;
		    width: 32px;
		    height: 25px;
		    line-height: 25px;
		    /* opacity: .5; */
			margin-right: 25px;
		}
		.d-block {
		    display: block!important;
		}
		@media (min-width: 768px){
			.col-md-2 {
			    flex: 0 0 16.66667%;
			    max-width: 16.66667%;
			}
			.col-md-8 {
			    flex: 0 0 66.66667%;
			    max-width: 66.66667%;
			}
			.col-md-12 {
			    flex: 0 0 100%;
			    max-width: 100%;
			}
			.col-xl-9 {
			    flex: 0 0 75%;
			    max-width: 75%;
			}
			.col-xl-3 {
			    flex: 0 0 25%;
			    max-width: 25%;
			}
			.col-md-6 {
			    flex: 0 0 50%;
			    max-width: 50%;
			}
			footer {
			    /* padding: 75px 0; */
			}
			.justify-content-md-center {
			    justify-content: center !important;
			}
			.apk-nav-footer li {
			    margin-left: -5px;
			    padding: 0 15px;
			    border-right: 1px solid #fff;
			    border-bottom: none;
			}
			.d-md-inline-block {
			    display: inline-block !important;
			}
		}
		@media (min-width: 992px){
			.col-lg-4 {
			    /* flex: 0 0 30%; */
			    max-width: 33.33333%;
			}
			.col-lg-6 {
			    /* flex: 0 0 50%; */
			    max-width: 50%;
			}
			.col-lg-8 {
			    /* flex: 0 0 66.66667%; */
			    max-width: 66.66667%;
			}
			.navbar-expand-lg .navbar-collapse {
			    display: flex !important;
			    flex-basis: auto;
			}
			.navbar-expand-lg .navbar-nav {
			    flex-direction: row;
			}
			.navbar-expand-lg .navbar-nav .nav-link {
			    padding-right: 15px;
			    padding-left: 15px;
			}
			.navbar-expand-lg .navbar-nav .dropdown-menu {
			    position: absolute;
			}
			.navbar-expand-lg {
			    flex-flow: row nowrap;
			    justify-content: flex-start;
			}
			header {
			    padding: 18px 0 17px;
			}
			.apk-box:first-child {
			    padding-top: 50px;
			}

		}
		@media (min-width: 1200px){
			.col-xl-5 {
			    flex: 0 0 41.66667%;
			    max-width: 41.66667%;
			}
		}
		@media only screen and (max-width: 1024px){
			#apk-box-01{
				margin-top: 0;
				display: none;
			}

			.header-mobile {
			    display: flex;
			    position: fixed;
			    top: 0;
			    left: 0;
			    z-index: 999;
			    width: 100%;
			    height: 50px;
			    background-color: #4f87de;
			}
			.header-web {
			    display: none;
			}
			.header-mobile .hambergur {
			    position: absolute;
			    top: 0;
			    left: 0;
			    width: 50px;
			    height: 50px;
			    font-size: 25px;
			    color: #fff;
			    text-align: center;
			    cursor: pointer;
			}
			.header-mobile .apk-logo {
			    width: calc(100% - 130px);
			    text-align: center;
			    margin: auto;
			    padding: 0;
			}
			.header-mobile .apk-logo img {
			    width: 90px;
			}
			.header-mobile .search-btn {
			    color: #fff;
			    font-size: 20px;
			    position: absolute;
			    top: 0;
			    right: 0;
			    width: 50px;
			    height: 50px;
			    text-align: center;
			    vertical-align: middle;
			    cursor: pointer;
			}
			.header-mobile .search-btn img{
				height: 38px;
			}
			.header-mobile .apk-logo img {
			    width: 200px;
			}
			.search-mobile.active {
			    display: block;
			}
			.search_cancel.active {
			    display: block;
			}
			.search-mobile {
			    display: none;
			    position: fixed;
			    top: 0;
			    left: 0;
			    z-index: 999;
			    width: 100%;
			}
			.search-mobile .apk-search input {
			    border-radius: 5px;
				/*padding-right: 0;*/
		        /* padding: 10px 30px 10px 10px; */
			}
			.search-mobile .apk-search button {
			    font-size: 20px;
			    right: 5px;
			    top: 24px;
			}
			.search-mobile .apk-search {
			    width: calc(100% - 130px);
				position: absolute;
			    top: 7px;
		        right: 60px;
    			left: 60px;
			}
			.menu-mobile {
			    width: 100%;
			    position: fixed;
			    top: 50px;
			    left: 0px;
			    height: calc(100vh - 50px);
			    background-color: #fff;
			    z-index: 99;
			}
			.menu-mobile.active-mb {
			    display: block;
			}
			.menu-mobile ul {
			    list-style: none;
			    padding-left: 0;
			    margin:0;
			}
			.menu-mobile ul li a {
			    display: block;
			    padding: 10px 15px;
			    border-bottom: 1px solid #ddd;
			}
			.apk-box{
				/* padding-top: 72px; */
			}
			#apk-box-05{
				padding-top: 0px;
			}
			.apk-box--full .apk-apps__wrapper .apk-app img{
				width: 100%;
			}
			.apk-box--full .apk-apps__wrapper .apk-app {
			    position: relative;
			    padding: 5px;
			    width: calc(100% - 10px);
			}
			.apk-app--small{
				width: calc(50% - 10px) !important;
			}
			.apk-apps .row .col-md-6 {
			    width: calc(100%/3 - 20px);
			    padding-left: 10px;
			    padding-right: 10px;
			}
			.apk-apps .row .col-md-6 .apk-app .apk-app__content {
			    /* width: 100%; */
			    padding-left: 0;
			    margin-top: 0;
				float: left;
				margin-left: 15px;
				max-width: 70%;
			}
			.apk-apps .apk-app__img{
				width: 90px;
			}
			.apk-apps .apk-app__title a{
				font-weight: 500;
			    overflow: hidden;
			    text-overflow: ellipsis;
			    white-space: nowrap;
			    display: inline-block;
			    width: 100%;
			}
			#apk-box-05 .apk-apps .row{
				display: block;
				overflow-x: scroll;
				overflow-y: hidden;
				white-space: nowrap;
				-ms-overflow-style: none;
			}
			#apk-box-05 .apk-apps .row::-webkit-scrollbar {
			  display: none;
			}

			#apk-box-05 .apk-apps .row .col-md-6{
				width: 100%;
				/* display: inline-block; */
			}
			#apk-box-05 .apk-apps .row .apk-app{
				margin-top: 10px;
				display: flex;
				height: 100px;
    			border-bottom: 1px solid #eee;
			}
			#apk-box-05 .apk-apps .apk-app__img{
				/* float: none !important; */
				width: 90px;
				min-width: 90px;
			}
			#apk-box-05 .apk-apps .apk-app__img img{
				border-radius: 12px;
			}
			#apk-box-05 .apk-apps .apk-app__author{
				display: none;
			}
			#apk-box-05 .apk-apps .apk-app__title{
				margin-bottom: 0px;
				overflow: hidden;
				text-overflow: ellipsis;
				white-space: nowrap;
			}
			#apk-box-04 .apk-apps__wrapper {
				display: block;
			    overflow-x: scroll;
			    overflow-y: hidden;
			    white-space: nowrap;
			    -ms-overflow-style: none;
			}
			#apk-box-04 .apk-apps__wrapper .row::-webkit-scrollbar {
			  display: none;
			}
			#apk-box-04 .apk-apps__wrapper .apk-app{
				display: inline-block;
				float: none !important;
			}
			#apk-box-04 .apk-app--banner {
				display: none !important;
			}
			#apk-box-04{
				padding-top: 25px;
			}
			.pagination {
		    	display: block;
				overflow-x: scroll;
				overflow-y: hidden;
				white-space: nowrap;
			}
			li.page-item {
			    display: inline-block;
			}
		}
		
		.apk-box--blue .apk-box__title span{
		  color: #4f87de;
		}
		.apk-box--blue .apk-box__title span:last-child{
		  color: #666 ;
		}
		.apk-box--red .apk-box__title span{
		  color: #ee6150;
		}
		.apk-box--red .apk-box__title span:last-child{
		  color: #666 ;
		}
		.apk-box--lightgreen .apk-box__title span{
		  color: #fff;
		}
		.apk-box--lightgreen .apk-box__title span:last-child{
		  color: #666 ;
		}
		.apk-box--green .apk-box__title span{
		  color: #58be5b;
		}
		.watch{
			fill:#58be5b;
		}
		.apk-box--green .apk-box__title span:last-child{
		  color: #666 ;
		}
		.apk-box--full .apk-apps__wrapper .apk-app--banner {
		    display: none;
		    bottom: 0;
		    height: 138px;
		}
		@media (min-width: 576px){
			.apk-box--full .apk-apps__wrapper .apk-app {
		    	width: 50%;
			}
		}
		@media (min-width: 992px){
			.apk-box--full .apk-apps__wrapper .apk-app {
			    width: 24%;
			}
			.apk-box--full .apk-apps__wrapper .apk-app--small {
			    width: 11.6% !important;
			}
			.apk-box--full .apk-apps__wrapper .apk-app--banner {
			    display: block;
			}
			#apk-box-05 .widget-advertisement  {
			    width: 25%;
			}
		}

		.follow_icon {
		display: flex;
		/* justify-content: space-between; */
		padding: 1em 0;
		/* width: 80%; */
		}
	</style>