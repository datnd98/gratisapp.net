<style type="text/css">

		*,
		*::before,
		*::after {
		  box-sizing: border-box;
		}
		@media only screen and (max-width: 1024px){
			#apk-box-01 .apk-sidebar {
			margin-top: 0;
			display: none;
		}
	}
		@media screen and (min-width: 1088px){
		  .container {
		      max-width: 960px;
		      width: 960px;
		  }
		}

		@media screen and (min-width: 1280px){
		  .container {
		      max-width: 1152px;
		      width: 1152px;
		  }
		}
		@media screen and (min-width: 1472px){
		  .container {
		      max-width: 1176px;
		      width: 1176px;
		  }
		}
		body{
			margin:0;
			overflow-x: hidden;
			font-family: "Poppins";
		    font-size: 0.875rem;
		    font-weight: 400;
		    line-height: 1.7;
		    color: #666666;
		    text-align: left;
		    background-color: #fff;
		}
		main{
			min-height: 76.9vh;

		}
		a{
			text-decoration: none;
		}
		ul{
			list-style: none;
			padding: 0px;
		}
		h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
		    margin-bottom: 0.5rem;
		    font-family: inherit;
		    font-weight: 500;
		    line-height: 1.2;
		    color: inherit;
		}
		.clearfix::after {
		    display: block;
		    clear: both;
		    content: "";
		}
		.container {
		    margin: 0 auto;
	        width: 100%;
		    padding-right: 15px;
		    padding-left: 15px;
		    margin-right: auto;
		    margin-left: auto;
		}
		.row {
		    display: flex;
		    flex-wrap: wrap;
		    margin-right: -15px;
		    margin-left: -15px;
		}
		
		.search-mobile{
			display: none;
		}
		.menu-mobile {
		    display: none;
		}
		.align-items-center {
		    align-items: center !important;
		}

		.d-flex {
		    display: flex !important;
		}
		
		article, aside, figcaption, figure, footer, hgroup, main, nav, section {
		    display: block;
		}
		img {
		    max-width: 100%;
		    height: auto;
		    vertical-align: middle;
		    border: 0;
		}
		a {
		    color: #4f87de;
		    text-decoration: none;
		    background-color: transparent;
		    -webkit-text-decoration-skip: objects;
		}
		.apk-app__title a {
		    color: #333333;
		}
		.search-auto-complete{
			display: none;
		}
		.show-livesearch{
			/* display: block; */
		}
		.search-auto-complete {
		    position: absolute;
		    background-color: #fff;
		    z-index: 99;
		    top: 35px;
		    width: 100%;
		    -webkit-box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
		    -moz-box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
		    box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
		}
		.search-auto-complete ul {
		    padding: 0px;
		    margin-top: 0;
	        margin-bottom: 1rem;
	        list-style: none;
		}	
		.search-auto-complete ul li {
		    padding: 10px 0px;
		    border-bottom: 1px solid #e0e0e0;
		}
		.search-auto-complete ul .apk-app .apk-app__img {
		    width: 50px;
		    margin-right: 0px !important;
		}
		.search-auto-complete ul .apk-app .apk-app__content {
		  padding-left: 65px;
		}
		.search-auto-complete ul .apk-app .apk-app__author{
		  color: #666;
		  margin-top: -5px;
		}
		.search-auto-complete .apk-app{
			margin-top:0px;
		}
		.search-auto-complete ul li:last-child {
		    border-bottom: 0px;
		}
		.apk-sidebar__content li {
		    border-bottom: 1px dashed #ececec;
		}
		.col-md-2,.col-xl-5, .col-lg-4,.col-lg-8,.col-xl-9,.col-xl-3,.col-xl-4  {
		    position: relative;
		    width: 100%;
		    min-height: 1px;
		    padding-right: 15px;
		    padding-left: 15px;
		}
		@media (min-width: 768px){
			.apk-logo {
			    padding: 0;
			}
		}
		.apk-logo {
		    position: relative;
	        padding: 5px 0;
		}
		.apk-search.input-group, #search-top {
		    border-radius: 5px;
		    border: 0px;
		}
		.apk-search {
		    overflow: inherit;
		    position: relative;
		    background-color: #fff;
		    overflow: hidden;
		    border-radius: 0.25rem;
		}
		.apk-search input {
		    padding-right: 37px;
		}
		.form-control {
		    display: block;
		    width: 100%;
		    /*height: calc(2.2375rem + 2px);*/
		    padding: 0.375rem 0.75rem;
		    font-size: 0.875rem;
		    line-height: 1.7;
		    color: #495057;
		    background-color: #fff;
		    background-clip: padding-box;
		    border: 1px solid #ced4da;
		    border-radius: 0.25rem;
		    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.input-group {
		    position: relative;
		    display: flex;
		    flex-wrap: wrap;
		    align-items: stretch;
		    width: 100%;
		}
		.apk-search.input-group, #search-top {
		    border-radius: 5px;
		    border: 0px;
			white-space: nowrap;
			overflow: hidden;
			text-overflow: ellipsis;
		}
		.input-group > .form-control, .input-group > .custom-select, .input-group > .custom-file {
		    position: relative;
		    flex: 1 1 auto;
		    width: 1%;
		    margin-bottom: 0;
		}
		.btn:not(:disabled):not(.disabled) {
		    cursor: pointer;
		}
		.apk-search .btn {
		    position: absolute;
		    top: 50%;
		    right: 0;
		    z-index: 3;
		    width: 37px;
		    -webkit-transform: translateY(-50%);
		    transform: translateY(-50%);
		}
		.btn-link:hover {
		    color: #2360bd;
		    text-decoration: none;
		    background-color: transparent;
		    border-color: transparent;
		}
		.btn-link {
		    font-weight: 400;
		    color: #4f87de;
		    background-color: transparent;
		}
		.btn {
		    display: inline-block;
		    font-weight: 400;
		    text-align: center;
		    white-space: nowrap;
		    vertical-align: middle;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    border: 1px solid transparent;
		    padding: 0.375rem 0.75rem;
		    font-size: 0.875rem;
		    line-height: 1.7;
		    border-radius: 0.25rem;
		    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.col-md-2,.col-xl-5, .col-lg-4 {
		    position: relative;
		    width: 100%;
		    min-height: 1px;
		    padding-right: 15px;
		    padding-left: 15px;
		}
		.collapse:not(.show) {
		    display: none;
		}
		.navbar-collapse {
		    flex-basis: 100%;
		    flex-grow: 1;
		    align-items: center;
		}
		.ml-auto, .mx-auto {
		    margin-left: auto !important;
		}
		.navbar-nav {
		    display: flex;
		    flex-direction: column;
		    padding-left: 0;
		    margin-bottom: 0;
		    list-style: none;
		    margin-top: 4px;
		}
		.nav-link {
		    display: block;
		    padding: 0.5rem 1rem;
		}
		.dropdown:hover>.dropdown-menu {
		    display: block;
		}
		.navbar-nav .dropdown-menu {
		    position: static;
		    float: none;
		}
		.dropdown-menu {
		    z-index: 1000;
		    display: none;
		    float: left;
		    min-width: 10rem;
		    padding: 0.5rem 0;
		    margin: 0.125rem 0 0;
		    font-size: 0.875rem;
		    color: #666666;
		    text-align: left;
		    list-style: none;
		    background-color: #fff;
		    background-clip: padding-box;
		    border: 1px solid rgba(0, 0, 0, 0.15);
		    border-radius: 0.25rem;
		}
		.dropdown-item:active {
		    color: #fff;
		    text-decoration: none;
		    background-color: #007bff;
		}
		.dropdown-item:hover, .dropdown-item:focus {
		    color: #16181b;
		    text-decoration: none;
		    background-color: #f8f9fa;
		}
		a:hover {
		    color: #2360bd;
		    text-decoration: none;
		}
		.dropdown-item {
		    display: block;
		    padding: 0.25rem 1.5rem;
		    clear: both;
		    font-weight: 400;
		    color: #212529;
		    text-align: inherit;
		    white-space: nowrap;
		    background-color: transparent;
		    border: 0;
		}
		
		.detailItemWrap .apk-detail__img {
		    position: absolute;
		    width: 20%;
		    top: 0;

		}
		.detailItemWrap .titleItem {
		    padding-left: calc(20% + 30px);
		    width: 100%;
		}
		.detailItemWrap .desItem {
		    padding-left: calc(20% + 30px);
		    width: 100%;
		}
		.detailItemWrap .apk-detail__download {
		    padding-left: calc(20% + 30px);
		    width: 100%;
		    margin-top: 10px;
		}
		.pb-3, .py-3 {
		    padding-bottom: 1rem !important;
		    padding-top: 1rem !important;
		}
		.star-rating {
		    display: inline-block;
		    position: relative;
		}
		.star-rating::before {
		    font-size: 15px;
		    content: "★★★★★";
		}
		.star-rating__size::before {
		    font-size: 15px;
		    color: #fcd700;
		    content: "★★★★★";
		}
		.star-rating__size {
			display: inline-block;
			position: absolute;
			top: 0;
			left: 0;
			z-index: 1;
			width: 0;
			overflow: hidden;
		}
		.apk-detail__title {
		    position: relative;
		}
		.font-weight-light {
		    font-weight: 300 !important;
		}
		h1, .h1 {
		    font-size: 1.75rem;
		    margin-top: 0;
   			margin-bottom: 0.5rem;
		}
		.detailItemWrap .apk-detail__download {
		    padding-left: calc(20% + 30px);
		    width: 100%;
		    margin-top: 10px;
		}
		.mb-5, .my-5 {
		    margin-bottom: 3rem !important;
		}
		.apk-detail__download .btn:hover, .apk-detail__download .btn:focus {
		    opacity: 0.7;
		}
		.btn:not(:disabled):not(.disabled) {
		    cursor: pointer;
		}
		.apk-detail__download .btn {
		    padding-left: 40px;
		    padding-right: 40px;
		    box-shadow: 0 5px 10px -5px #58be5b;
		    background-image: linear-gradient(0deg, #91ce6b 0%, #91c443 100%);
		    border-radius: 50px;
		}
		.btn:hover, .btn:focus {
		    text-decoration: none;
		}
		.font-weight-bold {
		    font-weight: 700 !important;
		}
		.text-white {
		    color: #fff !important;
		}

		.text-uppercase {
		    text-transform: uppercase !important;
		}
		.btn {
		    display: inline-block;
		    font-weight: 400;
		    text-align: center;
		    white-space: nowrap;
		    vertical-align: middle;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    border: 1px solid transparent;
		    padding: 0.375rem 0.75rem;
		    font-size: 0.875rem;
		    line-height: 1.7;
		    border-radius: 0.25rem;
		    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
		}
		.how-to-install {
		    border: 1px solid #4f87de;
		    padding: 6px 20px;
		    border-radius: 30px;
		    margin-top: 2px;
		    display: inline-block;
		    margin-left: 20px;
		}
		.nav-link {
		    background-color: #4f87de;
	        padding-left: 1.5rem;
			padding-right: 1.5rem;
			color: #666666;
			border-radius: 50px;
			display: initial;
    		padding: 0.5rem 1rem;
    		color: #fff;
    		font-size: 14px;
   			margin-bottom: 0px;
		}
		.table {
		    background-color: #f1f1f1;
	        width: 100%;
    		margin-top: 30px;
    		margin-bottom: 20px;
		}
		.rounded {
		    border-radius: 0.25rem !important;
		}
		.border {
		    border: 1px solid #dee2e6 !important;
		}
		.table th, .table td {
		    padding: 0.75rem;
		    vertical-align: top;
		    border-top: 1px solid #dee2e6;
		}
		.font-weight-bold {
		    font-weight: 700 !important;
		}
		.pl-5, .px-5 {
		    padding-left: 3rem !important;
		}
		.w-25 {
		    width: 25% !important;
		}
		.border-right {
		    border-right: 1px solid #dee2e6 !important;
		}
		.screenshots-box{
		    overflow: hidden;
		    overflow-x: scroll;
		    overflow-y: hidden;
		    white-space: nowrap;
		    -ms-overflow-style: none;
		}
		.screenshots-box::-webkit-scrollbar {
		  display: none;
		}
		.screenshots-box ul {
		    list-style: none;
		    display: flex;
		    padding: 0;
		}
		.screenshots-box ul li{
			padding: 0 10px;
		}
		.screenshots-box ul li:first-child{
			padding-left: 0px;
		}
		.apk-sidebar .apk-app {
		    margin-top: 0;
		    padding: 20px 0;
		}
		.apk-sidebar .apk-app__img {
		    width: 48px;
		}
		.apk-app__img {
		    width: 100px;
		    overflow: hidden;
		    border-radius: 0.25rem;
		}
		.apk-app__img a {
		    display: block;
		    font-size: 0;
		}
		.apk-sidebar .apk-app__content {
		    padding-left: 68px;
		}

		.apk-app__content {
		    border-radius: 0.25rem;
		}
		.float-left {
		    float: left !important;
		}
		h6, .h6 {
		    font-size: 0.875rem;
		    margin-top:0px;
		}
		.apk-app__author a {
		    color: #333333;
		}
		.apk-app__author {
		    font-size: 11px;
		}
		.apk-sidebar {
		    margin-top: 30px;
		}
		.apk-sidebar:first-child{
			margin-top: 0px;
		}
		.apk-socials {
			padding-top: 0px !important;
		}
		.apk-socials img {
		height: 100%;
		width: auto;
	}
		footer {
		    background-color: #333333;
		    padding: 15px 0;
		}
		.text-white {
		    color: #fff !important;
		}
		.apk-nav-footer li:first-child, .apk-nav-footer li:last-child {
		    margin-left: 0;
		    border-right-color: transparent;
		}
		.apk-nav-footer a {
		    font-size: 13px;
		}
		.apk-nav-footer a:hover {
		    opacity: 0.5;
		}
		.item-version{
			border-radius: 6px;
		    background: #ffffff;
		    box-shadow: 0 2px 3px 0 rgba(0,0,0,0.15);
		    -webkit-transition: all .2s linear;
		    transition: all .2s linear;
		    padding: 10px;
		}
		.list-version{
			margin: 10px 0;
			background: #dee2e6;
		}
		.item-version:hover {
		    -webkit-box-shadow: 0 15px 30px rgba(0,0,0,0.1);
		    box-shadow: 0 15px 30px rgba(0,0,0,0.1);
		    -webkit-transform: translate3d(0, -1px, 0);
		    transform: translate3d(0, -1px, 0);
		}
		.list-version .col-md-6{
			padding: 10px;
			display: inline-block;
		}
		.fat-cloud-down svg {
		    fill: #58be5b;
		   	width: 20px;
		   	height: 20px;
		   	display: block;
		}
		.apk-app__download{
		    padding: 10px 0;
		}
		.apk-app__download .fat-cloud-down{
			float: left;
		}
		.total-variants{
			float:right;
		}
		.apk-nav-footer li {
		    padding: 8px 0;
		    border-bottom: 1px solid #666666;
		}
		footer .col-md-8{
			position: relative;
		    width: 100%;
		    min-height: 1px;
		    padding-right: 15px;
		    padding-left: 15px;
		}
		.apkdownload{
			background: #F1F7FF;
			text-align: center;
			padding: 32px 134px;
		}
		.apkdownload h1{
			font-size: 25px;
			color: #4F87DF;
		    word-break: break-word;
		}
		.apkdownload a{
			font-size: 16px;
			color: #4F87DF;
		}
		.apkdownload #text-download{
			font-style: normal;
			font-weight: bold;
			font-size: 25px;
			line-height: 29px;
			color: #333333;
		}
		.tutorial{
		    margin-left: 0px;
		    margin-right: 0px;
		    background: #FFFFFF;
		    border: 1px solid #E7E7E7;
		    box-sizing: border-box;
		    margin-top: 20px;
		}
		.video-tutorial{
		    padding-left: 0px;
/*			border: 1px solid #E7E7E7;
			box-sizing: border-box;*/
		}
		.video-tutorial iframe{
			width: 100%;
			height: 100%;
			min-height: 317px;
		}
		.info-tutorial h3{
			padding-top: 10px;
			font-weight: bold;
			line-height: 21px;
			color: #000000;
			font-size: 16px;
		}
		.info-tutorial .btn-help{
			background: #4F87DF;
			border-radius: 5px;
			padding: 10px 0px;
			font-weight: 500;
			font-size: 16px;
			line-height: 19px;
			color: #FFFFFF;
			margin-top: 10px;
			display: inline-block;
			width: 100%;
			text-align: center;
		}
		.report h3{
			font-style: normal;
			font-weight: bold;
			font-size: 20px;
			line-height: 23px;
			margin-top: 0px;
			color: #4F87DF;
			padding-left: 20px;
			padding-top: 29px;
		    width: 100%;
		}
		.report{
			background: #FEFBF4;
		}
		.report ul{
   			overflow: hidden;
		    overflow-x: scroll;
		    overflow-y: hidden;
		    white-space: nowrap;
		    padding: 10px;
		}

		.report ul li{
			background: #FFFFFF;
			box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15);
			border-radius: 5px;
			margin: 0 10px;
			display: inline-block;
		}
		.report ul li a{
			font-weight: bold;
			font-size: 16px;
			line-height: 19px;
			text-align: center;
			color: #000000;
			padding: 25px 20px;
			text-align: center;
		    display: inline-block;
		}
		.row h2.title-page{
			font-style: normal;
			font-weight: bold;
			font-size: 20px;
			line-height: 23px;
			width: 100%;
			color: #4F87DF;
			padding-left: 15px;
			overflow: hidden;
		   
		}
		.list-post{
			display: flex;
			flex-wrap: wrap;
			width: 100%;
		}
		.list-post .single-post{
			width: calc(100%/5);
			padding: 10px 15px;
		}
		.list-post .single-post a{
			font-style: normal;
			font-weight: bold;
			font-size: 16px;
			line-height: 19px;
			color: #333333;
			overflow: hidden;
			text-overflow: ellipsis;
		    white-space: nowrap;
		    display: inline-block;
		    width: 100%;
		}
		.btn-burger {
			position: absolute;
			top: 3px;
			left: 0;
			display: block;
			width: 46px;
			height: 46px;
			border-radius: 50%;
			cursor: pointer;
			z-index: 100;
		}
		.search-btn img{
			padding-top: 15px;
		}
		.burger-line-wrap {
			position: absolute;
			left: 50%;
			top: 46%;
			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
			width: 21px;
			height: 14px;
		}
		.btn-burger.clicked .burger-line:first-of-type {
		    top: 47%;
		    -webkit-transform: rotate(135deg) translate(-1px,0);
		    transform: rotate(135deg) translate(-1px,0);
		}
		.btn-burger.clicked .burger-line:last-of-type {
		    bottom: 47%;
		    -webkit-transform: rotate(-135deg) translate(-1px,0);
		    transform: rotate(-135deg) translate(-1px,0);
		}
		.btn-burger.clicked .burger-line:nth-of-type(2) {
		    opacity: 0;
		}
		.burger-line:first-of-type {
		    top: 0;
		    left: 0;
		}
		.burger-line:nth-of-type(2) {
		    top: 47%;
		    left: 0;
		    opacity: 1;
		}
		.burger-line:last-of-type {
		    bottom: 0;
		    left: 0;
		}
		.burger-line {
		    display: block;
		    position: absolute;
		    width: 100%;
		    height: 1px;
		    background: #fff;
		    transform-origin: center center;
		    transition: all .2s ease-in-out;
		}
		.apk-socials a {
		    display: inline-block;
		    font-size: 16px;
		    width: 32px;
		    height: 25px;
		    line-height: 25px;
		    /* opacity: .5; */
			margin-right: 25px;
		}
		.d-block {
		    display: block!important;
		}
		@media (min-width: 576px){
			.d-sm-inline-block {
			    display: inline-block !important;
			}
			.widget-list-app {
				margin: 0 -15px;
			}
		}
		@media (min-width: 768px){
			.col-md-2 {
			    flex: 0 0 16.66667%;
			    max-width: 16.66667%;
			}
			.col-md-8 {
			    flex: 0 0 66.66667%;
			    max-width: 66.66667%;
			}
			.col-md-12 {
			    flex: 0 0 100%;
			    max-width: 100%;
			}
			.col-xl-9 {
			    flex: 0 0 75%;
			    max-width: 75%;
			}
			.col-xl-3 {
			    flex: 0 0 25%;
			    max-width: 25%;
			}
			.col-md-6 {
			    flex: 0 0 50%;
			    max-width: 50%;
			}
			footer {
			    /* padding: 75px 0; */
			}
			.text-md-center {
			    text-align: center !important;
			}
			.justify-content-md-center {
			    justify-content: center !important;
			}
			.apk-nav-footer li {
			    margin-left: -5px;
			    padding: 0 15px;
			    border-right: 1px solid #fff;
			    border-bottom: none;
			}
			.d-md-inline-block {
			    display: inline-block !important;
			}
		}
		@media (min-width: 992px){
			.col-lg-4 {
			    flex: 0 0 calc(100%/3);
			    max-width: 33.33333%;
			}
			.col-lg-6 {
			    flex: 0 0 50%;
			    max-width: 50%;
			}
			.col-lg-8 {
			    flex: 0 0 66.66667%;
			    max-width: 66.66667%;
			}
			.navbar-expand-lg .navbar-collapse {
			    display: flex !important;
			    flex-basis: auto;
			}
			.navbar-expand-lg .navbar-nav {
			    flex-direction: row;
			}
			.navbar-expand-lg .navbar-nav .nav-link {
			    padding-right: 15px;
			    padding-left: 15px;
			}
			.navbar-expand-lg .navbar-nav .dropdown-menu {
			    position: absolute;
			}
			.navbar-expand-lg {
			    flex-flow: row nowrap;
			    justify-content: flex-start;
			}
			
			.apk-box:first-child {
			    padding-top: 50px;
			}
			.widget-list-app {
			    margin: 0;
			}

		}
		@media (min-width: 1200px){
			.col-xl-5 {
			    flex: 0 0 41.66667%;
			    max-width: 41.66667%;
			}
			.col-xl-9 {
			    flex: 0 0 75%;
			    max-width: 75%;
			}
			.col-xl-3 {
			    flex: 0 0 25%;
			    max-width: 25%;
			}
		}
		
		@media only screen and (max-width: 1024px){
			
			.search-mobile.active {
			    display: block;
			}
			.search-mobile {
			    display: none;
			    position: fixed;
			    top: 0;
			    left: 0;
			    z-index: 999;
			    width: 100%;
			}
			.search-mobile .apk-search input {
			    border-radius: 5px;
				padding-right: 0;
			    /* border-top-right-radius: 5px !important;
			    border-bottom-right-radius: 5px !important;
		        padding: 10px 30px 10px 10px; */
			}
			.search-mobile .apk-search button {
			    font-size: 20px;
			    right: 5px;
			    top: 24px;
			}
			.search-mobile .apk-search {
				width: calc(100% - 130px);
				position: absolute;
				top: 7px;
				right: 60px;
				left: 60px;
			}
			#form-mb button{
	display: none;
}
#search-top-mobile{
	white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
			.menu-mobile {
			    width: 100%;
			    position: fixed;
			    top: 60px;
			    left: 0px;
			    height: calc(100vh - 60px);
			    background-color: #fff;
			    z-index: 99;
			}
			.menu-mobile.active-mb {
			    display: block;
			}
			.menu-mobile ul {
			    list-style: none;
			    padding-left: 0;
			    margin:0;
			}
			.menu-mobile ul li a {
			    display: block;
			    padding: 10px 15px;
			    border-bottom: 1px solid #ddd;
			}
			#apk-box-01{
				padding-top: 72px !important;
			}
			.table-responsive td {
			    white-space: pre-wrap;
			    word-break: break-word;
			    padding: 10px;
			    padding-left: 15px !important;
			}
			.table-responsive td:nth-child(1) {
			    white-space: nowrap;
			}
			.table-responsive td {
			    white-space: pre-wrap;
			    word-break: break-word;
			    padding: 10px;
			    padding-left: 15px !important;
			}
			.apk-sidebar .apk-app__img {
			    width: 80px;
			}
			.apk-sidebar .apk-app__content {
			    padding-left: 100px;
			}
			.list-version{
				display: block;
				overflow-x: scroll;
				overflow-y: hidden;
				white-space: nowrap;
				-ms-overflow-style: none;
			}
			.list-version::-webkit-scrollbar {
			  display: none;
			}
			.apk-nav-footer li {
			    border-bottom: 1px solid #3e3e3e;
			}
			.apkdownload h1{
		    	word-break: break-word;
			}
			.apkdownload{
				padding: 20px 20px;
			}
			.video-tutorial{
				padding-right: 0px;
			}
			.info-tutorial .btn-help{
				margin-bottom: 10px;
			}
			.report ul{
				 -ms-overflow-style: none;
			}
			.report ul::-webkit-scrollbar {
			  display: none;
			}
			.list-post .single-post{
				width: calc(100%/3);
				padding: 10px;
			}
		}
		@media only screen and (max-width: 575px){
			.apk-detail .apk-detail__title {
			    margin-top: 12px;
			}
			.apk-detail .apk-detail__title {
			    font-size: 1.3rem;
			}
			.apk-detail__download p.mb-5 {
			    margin-bottom: 20px !important;
			}
			.apk-detail__download .btn {
			    width: 100%;
			}
			.apk-detail .apk-detail__download .how-to-install {
			    margin-left: 0px;
			    margin-top: 15px;
			    width: 100%;
			}
			.list-post .single-post{
				width: calc(100%/2);
				padding: 10px;
			}
		}

		@media only screen and (max-width: 415px){
			.detailItemWrap .titleItem .headTitleItem {
			    margin-top: 0px;
			    float: left;
			    width: calc(100% + 100px);
			    margin-left: -100px;
			    border-top: 1px solid #eee;
			    padding-top: 8px;
			    margin-bottom: 8px;
			    display: flex;
			    justify-content: space-around;
			}
			.detailItemWrap .titleItem h1 {
			    padding-left: 20px;
			    float: left;
			    min-height: 90px;
			}
			.detailItemWrap .desItem {
			    float: left;
			    width: 100%;
			    padding-left: 0;
			    border-top: 1px solid #eee;
			    padding-top: 8px;
			    margin-bottom: 8px;
			    display: flex;
			    justify-content: space-around;
			}
			.detailItemWrap .apk-detail__download {
			    float: left;
			    width: 100%;
			    padding-left: 0;
			}
			.detailItemWrap .titleItem {
			    padding-left: 0;
			    float: left;
			    width: calc(100% - 100px);
			}
			.detailItemWrap .apk-detail__img {
			    position: relative;
			    float: left;
			    width: 100px;
			}
			.list-post .single-post{
				width: calc(100%/1);
				padding: 10px;
			}
		}
		#text-download:after{
  content: ' .';
  animation: dots 1s steps(5, end) infinite;}
		@keyframes dots {
  0%, 20% {
    color: rgba(0,0,0,0);
    text-shadow:
      .25em 0 0 rgba(0,0,0,0),
      .5em 0 0 rgba(0,0,0,0);}
  40% {
    color: black;
    text-shadow:
      .25em 0 0 rgba(0,0,0,0),
      .5em 0 0 rgba(0,0,0,0);}
  60% {
    text-shadow:
      .25em 0 0 black,
      .5em 0 0 rgba(0,0,0,0);}
  80%, 100% {
    text-shadow:
      .25em 0 0 black,
      .5em 0 0 black;}}
	</style>