<style type="text/css">
	*, *::before, *::after {
		box-sizing: border-box;
	}

	main{
		min-height: 76.9vh;
	}
	@media only screen and (max-width: 1024px){
		#apk-box-01 .apk-sidebar {
			margin-top: 0;
			display: none;
		}
	}
	@media only screen and (max-width: 300px){
		body > div.content {
			margin-top : 25%;
			padding: 0 15px;
		}
		div.upload__max-file-size > div#details-apk {
			width: 50%;
		}
	}
	@media only screen and (max-width: 450px){
		div > div#details-apk {
			width: 75%;
		}
	}
	@media only screen and (max-width: 1050px){
		div.content {
			margin-top : 15%;
			padding: 0 15px;
		}
		div.content > div.upload,div.comment {
			width: 100%;
			flex-wrap: unset;
		}
		div.upload__titile > p {
			font-size: 12px;
		}
		div#details-apk {
			width: 100%;
			font-size: 12px;
		}
	}
	.modal-dialog div.ldBar-label {
		color: #24cd77;
		font-size: 32px;
		opacity: 0.6;
	}
	#spinner1 {
		display:none;
		position: absolute;
		left: 50%;
		top: 50%;
		height: 195px;
		z-index: 9999;
		transform: translate( -50%,-50%) !important;
		opacity: 0.3;
	}
	#details-apk {
		font-size: 13px;
		line-height: 23px;
		color: #777;
		width: 560px;
		text-align: center;
		margin: 35px auto 45px auto;
	}
	@media screen and (min-width: 1088px){
	  .container {
		  max-width: 960px;
		  width: 960px;
	  }
	}

	@media screen and (min-width: 1280px){
	  .container {
		  max-width: 1152px;
		  width: 1152px;
	  }
	}
	@media screen and (min-width: 1472px){
	  .container {
		  max-width: 1176px;
		  width: 1176px;
	  }
	}
	body{
		margin:0;
		overflow-x: hidden;
		font-family: "Poppins";
	}
	*, *::before, *::after {
		box-sizing: border-box;
	}
	a{
		text-decoration: none;
	}
	img {
		height: auto;
		vertical-align: middle;
		border: 0;
	}
	.container {
		margin: 0 auto;
		position: relative;
		padding-right: 15px;
		padding-left: 15px;

	}
	.row {
		display: flex;
		flex-wrap: wrap;
		margin-right: -15px;
		margin-left: -15px;
	}
	.header-mobile{
		display: none;
	}
	.search-mobile{
		display: none;
	}
	.menu-mobile {
		display: none;
	}
	.align-items-center {
		align-items: center !important;
	}

	.d-flex {
		display: flex !important;
	}
	header * {
		color: #fff;
		outline: none;
	}
	header {
		background-color: #4f87de;
	}
	article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
		display: block;
	}
/*	.search-auto-complete{
		display: none;
	}*/
	@media (min-width: 768px){
		.apk-logo {
			padding: 0;
		}
	}
	.apk-logo {
		position: relative;
		padding: 5px 0;
	}
	.apk-search.input-group, #search-top {
		border-radius: 5px;
		border: 0px;
	}
	.apk-search {
		overflow: inherit;
		position: relative;
		background-color: #fff;
		overflow: hidden;
		border-radius: 0.25rem;
	}
	.apk-search input {
		padding-right: 37px;
	}
	.form-control {
		display: block;
		width: 100%;
		/*height: calc(2.2375rem + 2px);*/
		padding: 0.375rem 0.75rem;
		font-size: 0.875rem;
		line-height: 1.7;
		color: #495057;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid #ced4da;
		border-radius: 0.25rem;
		transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
	}
	.input-group {
		position: relative;
		display: flex;
		flex-wrap: wrap;
		align-items: stretch;
		width: 100%;
	}
	.apk-search.input-group, #search-top {
		border-radius: 5px;
		border: 0px;
	}
	.input-group > .form-control, .input-group > .custom-select, .input-group > .custom-file {
		position: relative;
		flex: 1 1 auto;
		width: 1%;
		margin-bottom: 0;
	}
	.btn:not(:disabled):not(.disabled) {
		cursor: pointer;
	}
	.apk-search .btn {
		position: absolute;
		top: 50%;
		right: 0;
		z-index: 3;
		width: 37px;
		-webkit-transform: translateY(-50%);
		transform: translateY(-50%);
	}
	.btn-link:hover {
		color: #2360bd;
		text-decoration: none;
		background-color: transparent;
		border-color: transparent;
	}
	.btn-link {
		font-weight: 400;
		color: #4f87de;
		background-color: transparent;
	}
	.btn {
		display: inline-block;
		font-weight: 400;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		border: 1px solid transparent;
		padding: 0.375rem 0.75rem;
		font-size: 0.875rem;
		line-height: 1.7;
		border-radius: 0.25rem;
		transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
	}
	.col-md-2,.col-xl-5, .col-lg-4,.col-lg-8,.col-xl-9,.col-xl-3,.col-xl-4,.col-md-12  {
		position: relative;
		width: 100%;
		min-height: 1px;
		padding-right: 15px;
		padding-left: 15px;
	}
	.collapse:not(.show) {
		display: none;
	}
	.navbar-collapse {
		flex-basis: 100%;
		flex-grow: 1;
		align-items: center;
	}
	.ml-auto, .mx-auto {
		margin-left: auto !important;
	}
	.navbar-nav {
		display: flex;
		flex-direction: column;
		padding-left: 0;
		margin-bottom: 0;
		list-style: none;
		margin-top: 4px;
	}
	.nav-link {
		display: block;
		padding: 0.5rem 1rem;
	}
	.dropdown:hover>.dropdown-menu {
		display: block;
	}
	.navbar-nav .dropdown-menu {
		position: static;
		float: none;
	}
	.dropdown-menu {
		z-index: 1000;
		display: none;
		float: left;
		min-width: 10rem;
		padding: 0.5rem 0;
		margin: 0.125rem 0 0;
		font-size: 0.875rem;
		color: #666666;
		text-align: left;
		list-style: none;
		background-color: #fff;
		background-clip: padding-box;
		border: 1px solid rgba(0, 0, 0, 0.15);
		border-radius: 0.25rem;
	}
	.dropdown-item:active {
		color: #fff;
		text-decoration: none;
		background-color: #007bff;
	}
	.dropdown-item:hover, .dropdown-item:focus {
		color: #16181b;
		text-decoration: none;
		background-color: #f8f9fa;
	}
	a:hover {
		color: #2360bd;
		text-decoration: none;
	}
	.dropdown-item {
		display: block;
		padding: 0.25rem 1.5rem;
		clear: both;
		font-weight: 400;
		color: #212529;
		text-align: inherit;
		white-space: nowrap;
		background-color: transparent;
		border: 0;
	}
	.apk-downloader .apk-downloader__title {
		font-size: 15px;
		margin-bottom: 8px;
	}
	.text-color-1 {
		color: #4f87de !important;
	}
	.font-weight-bold {
		font-weight: 700 !important;
	}
	.text-uppercase {
		text-transform: uppercase !important;
	}
	.apk-box {
		padding: 25px 0;
	}
	.apk-downloader img {
		border-radius: 5px;
		margin-top: 3px;
	}
	.icon-feature {
		width: 25px;
		height: 23px;
		margin-right: 10px;
		margin-top: 2px;
		float: left;
	}
	.apk-app {
		margin-top: 30px;
	}
	.apk-app__img {
		width: 100px;
		overflow: hidden;
		border-radius: 0.25rem;
	}
	.mr-4, .mx-4 {
		margin-right: 1.5rem !important;
	}
	.apk-app__content {
		padding-left: 124px;
		border-radius: 0.25rem;
	}
	.float-left {
		float: left !important;
	}
	.apk-app__author {
		font-size: 13px;
	}
	.font-weight-light {
		font-weight: 300 !important;
	}
	.apk-app__title{
		margin:0;
		margin-bottom: 4px;
	}
	.apk-app__published {
		font-size: 12px;
	}
	.text-muted {
		color: #6c757d !important;
	}
	.star-rating {
		display: inline-block;
		position: relative;
	}
	.star-rating::before {
		font-size: 15px;
		content: "★★★★★";
	}
	.star-rating__size::before {
		font-size: 15px;
		color: #fcd700;
		content: "★★★★★";
	}
	.star-rating__size {
		display: inline-block;
		position: absolute;
		top: 0;
		left: 0;
		z-index: 1;
		width: 0;
		overflow: hidden;
	}
	.apk-box__title {
		position: relative;
		z-index: 1;
		background-color: #fff;
	}
	#apk-box-04 .apk-box__title{
		background-color: #72c874;
		color: #fff;
	}
	#apk-box-04 .apk-app__img img {
		width: 100%;
		height: 140px;
	}
	.font-weight-normal {
		font-weight: 400 !important;
	}
	.clearfix::after {
		display: block;
		clear: both;
		content: "";
	}
	.border-bottom {
		border-bottom: 1px solid #dee2e6 !important;
	}
	.apk-socials label {
		font-size: 18px;
		font-weight: 500;
	}
	.apk-socials img {
		height: 100%;
		width: auto;
	}
	.apk-socials a {
		    display: inline-block;
		    font-size: 16px;
		    width: 32px;
		    height: 25px;
		    line-height: 25px;
		    /* opacity: .5; */
			margin-right: 25px;
		}
	.font-weight-bold {
		font-weight: 700 !important;
	}

	.mb-0, .my-0 {
		margin-bottom: 0 !important;
	}
	.d-block {
		display: block !important;
	}
	.mb-lg-3 {
		margin-bottom: 0.25rem !important;
	}
	.apk-app__title a{
		font-size:15px;
		display: block;
	}
	.border-top {
		border-top: 1px solid #dee2e6 !important;
	}
	.apk-box__content {
		position: relative;
		margin-top: -7px;
	}
	.apk-app__title a:hover, .apk-app__title a:focus,.apk-app__author a:hover, .apk-app__author a:focus {
		color: #4f87de;
	}
	.apk-app__title a,.apk-app__author a{
		color: #333333;
	}
	/*live search*/
	.apk-search {
		overflow: inherit;
		position: relative;
	}
	.search-auto-complete{
		display: none;
	}
	.show-livesearch{
		/* display: block; */
	}
	.search-auto-complete {
		position: absolute;
		background-color: #fff;
		z-index: 99;
		top: 35px;
		width: 100%;
		-webkit-box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
		-moz-box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
		box-shadow: 3px 3px 16px 0px rgba(0,0,0,0.2);
	}
	.search-auto-complete ul {
		padding: 0px;
		margin-top: 0;
		margin-bottom: 1rem;
		list-style: none;
	}	
	.search-auto-complete ul li {
		padding: 10px 0px;
		border-bottom: 1px solid #e0e0e0;
	}
	.search-auto-complete ul .apk-app .apk-app__img {
		width: 50px;
		margin-right: 0px !important;
	}
	.search-auto-complete ul .apk-app .apk-app__content {
	  padding-left: 65px;
	}
	.search-auto-complete ul .apk-app .apk-app__author{
	  color: #666;
	  margin-top: -5px;
	}
	.search-auto-complete .apk-app{
		margin-top:0px;
	}
	.search-auto-complete ul li:last-child {
		border-bottom: 0px;
	}
	/*end live search*/

	.apk-box--full .apk-apps__wrapper .apk-app {
		position: relative;
		padding: 5px;
		margin-top: 0;
		width: 100%;
	}

	.apk-box--full .apk-apps__wrapper .apk-app__img a::after {
		content: "";
		transition: all 0.2s ease-in-out;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		position: absolute;
		background-color: rgba(0, 0, 0, 0.5);
		z-index: 0;
		opacity: 0;
		visibility: hidden;
	}
	.apk-box--full .apk-apps__wrapper .apk-app__img {
		position: relative;
		width: 100%;
		margin-right: 0 !important;
	}
	.apk-app__img {
		width: 100px;
		overflow: hidden;
		border-radius: 0.25rem;
	}
	.apk-box--full .apk-apps__wrapper .apk-app--small .apk-app__content {
		display: none;
	}
	.apk-box--full .apk-apps__wrapper .apk-app__content {
		position: absolute;
		top: auto;
		right: 5px;
		bottom: 5px;
		left: 5px;
		z-index: 1;
		padding: 15px 20px;
		color: #fff;
		overflow: hidden;
		background: rgba(0, 0, 0, 0);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(0, 0, 0, 0)), color-stop(0%, rgba(0, 0, 0, 0)), color-stop(30%, rgba(0, 0, 0, 0)), color-stop(90%, rgba(0, 0, 0, 0.85)), color-stop(100%, rgba(0, 0, 0, 0.85)));
		background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0) 30%, rgba(0, 0, 0, 0.85) 90%, rgba(0, 0, 0, 0.85) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#000000', endColorstr='#000000', GradientType=0 );
	}
	.apk-app__content {
		padding-left: 124px;
		border-radius: 0.25rem;
	}
	.apk-box--full .apk-apps__wrapper .apk-app__content a {
		color: #fff;
	}
	.apk-box--lightgreen.apk-box--full {
		background-color: #72c874;
	}
	.apk-box--full {
		background-image: none;
	}
	.apk-apps .apk-app__title a{
		font-weight: 500;
	}
	footer {
		background-color: #333333;
		padding: 15px 0;
	}
	.text-white {
		color: #fff !important;
	}
	.apk-nav-footer li:first-child, .apk-nav-footer li:last-child {
		margin-left: 0;
		border-right-color: transparent;
	}
	.apk-nav-footer a {
		font-size: 13px;
	}
	.apk-nav-footer a:hover {
		opacity: 0.5;
	}
	.apk-nav-footer li {
		padding: 8px 0;
		border-bottom: 1px solid #666666;
	}
	footer .col-md-8{
		position: relative;
		width: 100%;
		min-height: 1px;
		padding-right: 15px;
		padding-left: 15px;
	}
	ul{
		list-style: none;
		padding: 0px;
	}
	.pt-5, .py-5 {
		padding-top: 3rem !important;
	}
	.justify-content-center {
		justify-content: center !important;
	}
	.pagination {
		display: flex;
		padding-left: 0;
		list-style: none;
		border-radius: 0.25rem;
	}
	.sr-only {
		position: absolute;
		width: 1px;
		height: 1px;
		padding: 0;
		overflow: hidden;
		clip: rect(0, 0, 0, 0);
		white-space: nowrap;
		border: 0;
	}
	.page-item:first-child .page-link {
		border-top-left-radius: 0px;
		border-bottom-left-radius: 0px;
	}
	.page-item.active .page-link {
		z-index: 1;
		color: #fff;
		background-color: #4f87de;
		border-color: #4f87de;
	}
	.page-item.active .page-link {
		z-index: 1;
		color: #fff;
		background-color: #007bff;
		border-color: #007bff;
	}
	.page-item:first-child .page-link {
		margin-left: 0;
		border-top-left-radius: 0.25rem;
		border-bottom-left-radius: 0.25rem;
	}
	.page-link:not(:disabled):not(.disabled) {
		cursor: pointer;
	}
	.page-link:hover {
		z-index: 2;
		color: #2360bd;
		text-decoration: none;
		background-color: #e9ecef;
		border-color: #dee2e6;
	}
	.page-link {
		position: relative;
		display: block;
		padding: 0.5rem 0.75rem;
		margin-left: -1px;
		line-height: 1.25;
		color: #4f87de;
		background-color: #fff;
		border: 1px solid #dee2e6;
	}
	.btn-burger {
		position: absolute;
		top: 3px;
		left: 0;
		display: block;
		width: 46px;
		height: 46px;
		border-radius: 50%;
		cursor: pointer;
		z-index: 100;
	}
	.search-btn img{
		padding-top: 15px;
	}
	.burger-line-wrap {
		position: absolute;
    left: 50%;
    top: 46%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    width: 21px;
    height: 14px;
	}
	.btn-burger.clicked .burger-line:first-of-type {
		top: 47%;
		-webkit-transform: rotate(135deg) translate(-1px,0);
		transform: rotate(135deg) translate(-1px,0);
	}
	.btn-burger.clicked .burger-line:last-of-type {
		bottom: 47%;
		-webkit-transform: rotate(-135deg) translate(-1px,0);
		transform: rotate(-135deg) translate(-1px,0);
	}
	.btn-burger.clicked .burger-line:nth-of-type(2) {
		opacity: 0;
	}
	.burger-line:first-of-type {
		top: 0;
		left: 0;
	}
	.burger-line:nth-of-type(2) {
		top: 47%;
		left: 0;
		opacity: 1;
	}
	.burger-line:last-of-type {
		bottom: 0;
		left: 0;
	}
	.burger-line {
		display: block;
		position: absolute;
		width: 100%;
		height: 1px;
		background: #fff;
		transform-origin: center center;
		transition: all .2s ease-in-out;
	}
	@media (min-width: 768px){
		.text-md-center {
			text-align: center !important;
		}
		.col-md-2 {
			flex: 0 0 16.66667%;
			max-width: 16.66667%;
		}
		.col-md-8 {
			flex: 0 0 66.66667%;
			max-width: 66.66667%;
		}
		.col-md-12 {
			flex: 0 0 100%;
			max-width: 100%;
		}
		.col-xl-9 {
			flex: 0 0 75%;
			max-width: 75%;
		}
		.col-xl-3 {
			flex: 0 0 25%;
			max-width: 25%;
		}
		.col-md-6 {
			flex: 0 0 50%;
			max-width: 50%;
		}
		footer {
			/* padding: 75px 0; */
		}
		.justify-content-md-center {
			justify-content: center !important;
		}
		.apk-nav-footer li {
			margin-left: -5px;
			padding: 0 13px;
			border-right: 1px solid #fff;
			border-bottom: none;
		}
		.d-md-inline-block {
			display: inline-block !important;
		}
		
	}
	@media (min-width: 992px){
		.col-lg-4 {
			flex: 0 0 30%;
			max-width: 33.33333%;
		}
		.col-lg-6 {
			flex: 0 0 50%;
			max-width: 50%;
		}
		.col-lg-8 {
			flex: 0 0 66.66667%;
			max-width: 66.66667%;
		}
		.navbar-expand-lg .navbar-collapse {
			display: flex !important;
			flex-basis: auto;
		}
		.navbar-expand-lg .navbar-nav {
			flex-direction: row;
		}
		.navbar-expand-lg .navbar-nav .nav-link {
			padding-right: 15px;
			padding-left: 15px;
		}
		.navbar-expand-lg .navbar-nav .dropdown-menu {
			position: absolute;
		}
		.navbar-expand-lg {
			flex-flow: row nowrap;
			justify-content: flex-start;
		}
		header {
			padding: 18px 0 17px;
		}
		.apk-box:first-child {
			padding-top: 50px;
		}

	}
	@media (min-width: 1200px){
		.col-xl-5 {
			flex: 0 0 41.66667%;
			max-width: 41.66667%;
		}
	}
	@media only screen and (max-width: 1024px){
		.header-mobile {
			display: flex;
			position: fixed;
			top: 0;
			left: 0;
			z-index: 999;
			width: 100%;
			height: 50px;
			background-color: #4f87de;
		}
		.header-web {
			display: none;
		}
		.header-mobile .hambergur {
			position: absolute;
			top: 0;
			left: 0;
			width: 50px;
			height: 50px;
			font-size: 25px;
			color: #fff;
			text-align: center;
			cursor: pointer;
		}
		.header-mobile .apk-logo {
			width: calc(100% - 130px);
			text-align: center;
			margin: auto;
			padding: 0;
		}
		.header-mobile .apk-logo img {
			width: 200px;
		}
		.header-mobile .search-btn {
			color: #fff;
			font-size: 20px;
			position: absolute;
			top: 0;
			right: 0;
			width: 50px;
			height: 50px;
			text-align: center;
			vertical-align: middle;
			cursor: pointer;
		}
		.search-mobile.active {
			display: block;
		}
		.search-mobile {
			display: none;
			position: fixed;
			top: 0;
			left: 0;
			z-index: 999;
			width: 100%;
		}
		.search-mobile .apk-search input {
			border-radius: 5px;
			border-top-right-radius: 5px !important;
			border-bottom-right-radius: 5px !important;
			padding: 10px 30px 10px 10px;
		}
		.search-mobile .apk-search button {
			font-size: 20px;
			right: 5px;
			top: 24px;
		}
		.search-mobile .apk-search {
			width: calc(100% - 120px);
			position: absolute;
			top: 8px;
			right: 60px;
			left: 60px;
		}
		.menu-mobile {
			width: 100%;
			position: fixed;
			top: 60px;
			left: 0px;
			height: calc(100vh - 60px);
			background-color: #fff;
			z-index: 99;
		}
		.menu-mobile.active-mb {
			display: block;
		}
		.menu-mobile ul {
			list-style: none;
			padding-left: 0;
			margin:0;
		}
		.menu-mobile ul li a {
			display: block;
			padding: 10px 15px;
			border-bottom: 1px solid #ddd;
		}
		.apk-box{
			padding-top: 72px;
		}
		#apk-box-05{
			padding-top: 0px;
		}
		.apk-box--full .apk-apps__wrapper .apk-app img{
			width: 100%;
		}
		.apk-box--full .apk-apps__wrapper .apk-app {
			position: relative;
			padding: 5px;
			width: calc(100% - 10px);
		}
		.apk-app--small{
			width: calc(50% - 10px) !important;
		}
		.apk-apps .row .col-md-6 {
			width: calc(100%/3 - 20px);
			padding-left: 10px;
			padding-right: 10px;
		}
		.apk-apps .row .col-md-6 .apk-app .apk-app__content {
			width: 100%;
			padding-left: 0;
			margin-top: 10px;
		}
		.apk-apps .apk-app__img{
			width: 100%;
		}
		.apk-apps .apk-app__title a{
			font-weight: 500;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
			display: inline-block;
			width: 100%;
		}
		#apk-box-05 .apk-apps .row{
			display: block;
			overflow-x: scroll;
			overflow-y: hidden;
			white-space: nowrap;
			-ms-overflow-style: none;
		}
		#apk-box-05 .apk-apps .row::-webkit-scrollbar {
		  display: none;
		}

		#apk-box-05 .apk-apps .row .col-md-6{
			width: 100px;
			display: inline-block;
		}
		#apk-box-05 .apk-apps .row .apk-app{
			margin-top: 0px;
		}
		#apk-box-05 .apk-apps .apk-app__img{
			float: none !important;
		}
		#apk-box-05 .apk-apps .apk-app__author{
			display: none;
		}
		#apk-box-05 .apk-apps .apk-app__title{
			margin-bottom: 0px;
		}
		#apk-box-04 .apk-apps__wrapper {
			display: block;
			overflow-x: scroll;
			overflow-y: hidden;
			white-space: nowrap;
			-ms-overflow-style: none;
		}
		#apk-box-04 .apk-apps__wrapper .row::-webkit-scrollbar {
		  display: none;
		}
		#apk-box-04 .apk-apps__wrapper .apk-app{
			display: inline-block;
			float: none !important;
		}
		#apk-box-04 .apk-app--banner {
			display: none !important;
		}
		#apk-box-04{
			padding-top: 25px;
		}
		#apk-box-04 .apk-app__img img {
			width: 100%;
			max-height: 180px;
		}
	}
	@media (max-width: 376px){
		#apk-box-04 .apk-app__img img {
			width: 100%;
			max-height: 160px;
		}
	}
	.apk-box--blue .apk-box__title span{
	  color: #4f87de;
	}
	.apk-box--blue .apk-box__title span:last-child{
	  color: #666 ;
	}
	.apk-box--red .apk-box__title span{
	  color: #ee6150;
	}
	.apk-box--red .apk-box__title span:last-child{
	  color: #666 ;
	}
	.apk-box--lightgreen .apk-box__title span{
	  color: #fff;
	}
	.apk-box--lightgreen .apk-box__title span:last-child{
	  color: #666 ;
	}
	.apk-box--green .apk-box__title span{
	  color: #58be5b;
	}
	.watch{
		fill:#58be5b;
	}
	.apk-box--green .apk-box__title span:last-child{
	  color: #666 ;
	}
	.apk-box--full .apk-apps__wrapper .apk-app--banner {
		display: none;
		bottom: 0;
		height: 150px;
	}
	@media (min-width: 576px){
		.apk-box--full .apk-apps__wrapper .apk-app {
			width: 50%;
		}
	}
	@media (min-width: 992px){
		.apk-box--full .apk-apps__wrapper .apk-app {
			width: 25%;
		}
		.apk-box--full .apk-apps__wrapper .apk-app--small {
			width: 12.5% !important;
		}
		.apk-box--full .apk-apps__wrapper .apk-app--banner {
			display: block;
		}
		#apk-box-05 .widget-advertisement  {
			width: 25%;
		}
	}

.nav__list{
    text-decoration: none;
    list-style: none;
    display: flex;
    flex-wrap: wrap;
    font-family: "Poppins";
    font-size: 1.7rem;

}
.nav__item{
    margin-right: 3rem;
}
.nav__item >a{
    text-decoration: none;
    font-size: 1.6rem;
    color:#ffffff;
}
@media screen and(max-width: 60rem){
    *{font-size: 5px;}
}
/*css contenrt*/
.content{
    display: flex;
    flex-direction: column;
    align-items: center;
}
/* css upload part*/
.upload{
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    background-color: #f5f5f5;
	width: 1150px;
    margin: 3% auto;
    border-radius: 0.5rem;
	box-shadow: 0px 2px 10px 0 rgba(0,0,0,0.15)
}
.upload__titile{
    display: inline-flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
}
.upload__titile >p{
    font-size: 2rem;
    color: #666666;
}
.upload__titile >img{
    width: 1.5rem;
    height: 1.6rem;
    margin-left: 1rem;
}
.upload__max-file-size >p{
	font-size: 15px;
    line-height: 24px;
    color: #24dc83;
    width: 500px;
    text-align: center;
    margin: 10px auto 0 auto;
}
.upload__apk{
    display: flex;
    flex-direction: column;
    align-items: center;
    flex-wrap: wrap;
}
.upload__apk >a{
    margin-bottom: 2rem;
}
.upload__apk >p{
    font-size: 1.4rem;
    color: #333333;
    margin-top: 1rem;
}
.upload__button >span >i{
    color: #ffffff;
    font-size: larger;
}
.upload__button{
    box-shadow: 0 6px 10px -5px #58be5b;
    background-image: linear-gradient(0deg, #91ce6b 0%, #91c443 100%);
    border-radius: 50px;
    text-decoration: none;
    color: #ffffff;
    font-size: 1.3rem;
    font-weight: bold;
    padding: 1rem 2rem;
}
.upload__button:hover{
    background-image: linear-gradient(0deg, #96be7d 0%, #afee51 100%);
}
.verifiinfo-box{
	width: 553px;
	margin: auto;
	background: #DFF0D8;
	border-radius: 10px;
	color: #3c763d;
	padding: 1rem;
	display: flex;
	align-items: flex-end;
    margin-bottom: 3%;
}
.verifiinfo-box> i {
    color: #91ce6b;
    padding-top: 0px;
    margin-right: 1rem;
    font-size: 2rem;

}
.verifiinfo-box ul{
    width: 100%;
    list-style: none;
    display: flex;
    flex-direction: column;
    padding: 0px;
}
.verifiinfo-box >ul >li{
    font-size: 1.4rem;
    display: flex;
    flex-wrap: wrap;
}
.verifiinfo-box >ul >li> strong{
    font-size: 1.4rem;
}
/*css comment part*/
.comment{
	margin: auto;
    width: 1150px;
    padding-right: 0px;
    padding-left: 0px;
    margin-bottom: 4rem;
    display: flex;
    justify-content: center;
}
.comment__wrapper{
    width: 100%;
    background-color: #ffffff;
    border-radius: 3px;
    box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1), 0 2px 4px rgba(0, 0, 0, 0.1);
}
.comment__content{ 
    display: flex;
    flex-direction: column;
    padding: 1.5rem 1.5rem;
}
.comment__content>p,.comment__heading>p{
    font-size: 1rem;
    font-weight: 500;
    color: #333333;
	margin: 0;
	margin-bottom: 1rem;
}
.comment__content>form>textarea{
    border: 1px solid #dbdbdb;
    margin-top: 0px; 
    margin-bottom: 0px; 
    width: 100%;
    height: 6.6rem;
    border-radius: 0.3rem;
    font-size: 1.5rem;
    padding: 1rem 1rem;
}
.comment__action{
    display: flex;
    flex-direction: column;
}
.comment__meta{
    display: flex;
    width: 100%;
    justify-content: space-between;
    margin: 1rem 0rem;
}
.comment__meta-left{
    display: flex;
    align-items: center;
}
.emotion{
    display: inline-block;
    position: relative;
    float: left;
}
.emotion:hover .emo-list{
    display: block;
}
.emotion .lin-insert-em{
    width: 24px;
    display: inline-block;
    cursor: pointer;
}
.emotion .lin-insert-em img{
    width: 100%;
}
.emotion .emo-list{
    width: 208px;
    background-color: #fff;
    border-radius: 5px;
    border: 1px solid #eee;
    padding: 10px;
    position: absolute;
    top: 30px;
    left: -5px;
    display: none;
}
.emotion .emo-list:before {
    position: absolute;
    content: '';
    width: 0;
    height: 0;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-bottom: 5px solid #ccc;
    top: -6px;
    left: 10px;
}
.emotion .emo-list .emo-icon{
    width: 20%;
    max-width: 24px;
    display: inline-block;
    margin: 2px 5px;
    cursor: pointer;
}
.emotion .emo-list .emo-icon img{
    width: 100%;
}
.emotion{
    margin-right: 1rem;
}
.fa-image,.fa-youtube{
    margin: 0rem 1rem;
    font-size: 2.2rem;
    color: #666666;
}
.rating {
    width: auto;
    font-size: 22px;
    overflow:hidden;
    float: right;
    margin-top: -6px;
}
.rating input {
    float: right;
    opacity: 0;
    position: absolute;
}
.rating a,
.rating label {
    float:right;
    color: #aaa;
    text-decoration: none;
    -webkit-transition: color .4s;
    -moz-transition: color .4s;
    -o-transition: color .4s;
    transition: color .4s;
}
.rating label:hover ~ label,
.rating input:focus ~ label,
.rating label:hover,
.rating a:hover,
.rating a:hover ~ a,
.rating a:focus,
.rating a:focus ~ a		{
    color: orange;
    cursor: pointer;
}
.rating2 {
    direction: rtl;
}
.rating2 a {
    float:none
}
.comment__meta-right{
    display: flex;  
    width: auto;
    font-size: 22px;
    overflow: hidden;
}
.comment__meta-right>a{
    color: #aaa;
    text-decoration: none;
    font-size: 1.6rem;
    transition: color .4s;
}
.comment__btn-submit:focus{
	outline: none;
}
.comment__btn-submit{
    display: flex;
    flex-direction: row-reverse;
}
.comment__btn-submit>button{
    color: #fff;
    background-color: #4f87de;
    border: none;
    border-radius: .3rem;
    padding: .8rem 4.8rem;
    font-size: 1.4rem;
    font-weight: 700;
    cursor: pointer;
}
.comment__btn-submit>button:hover{
    background-color: #2b71e0;
}
.comment__list{
    display: flex;
    flex-direction: column;
    width: 100%;
}
.comment__list-parent ,.comment__list-child{
    display: flex;
    margin-top: 2rem;
}
.comment__list-child{
    padding-left: 12.4rem;
}
.comment__detail{
    display: flex;
    flex-direction: column;
	padding: 0 1rem;
}
.comment__detail-info{
    display: flex;
    flex-wrap: wrap;
}
.comment__detail-info>a{
    text-decoration: none;
}
.comment__detail-name{
    color: #6c757d;
    font-size: 1.3rem;
    font-weight: bolder;
}
.comment__detail-time{
    text-overflow: ellipsis;
    color: #666666;
    font-size: 0.8rem;
    font-weight: 400;
    padding: 18px;
    margin-bottom: 0;
}
.comment__cmt{
    text-overflow: ellipsis;
    color: #666666;
    font-size: 0.9rem;
    font-weight: 400;
}
.comment__cmt{
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
    overflow: hidden;
    display: -webkit-box;
    width: 100%;
	margin: 0;
}
.comment__reply{
    font-size: 0.9rem;
    color: #6c757d;
    padding-bottom: 5px;
    border-bottom: 1px dashed #dbdbdb;
    text-decoration: none;
    margin-top: 2rem;
}
/*css for footer*/
.footer{
    width: 100%;
    display: flex;
    justify-content: center;
    background-color: #333;
    color: #ffffff;
    height: 21rem;
    align-items: center;
    position: sticky;
    top: 100%;
}
.footer__wrapper{
    width: 100%;
}
.footer__list{
    padding: 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    list-style: none;
}
.footer__list >li{
    padding-right: 1rem;
    border-right: 1px solid #fff;
    font-size: 1.3rem;
    margin-left: 1.65rem;
}
.footer__list >li:last-child{
    border-right: none;
}
.footer__list > li >a{
    text-decoration: none;
    font-size: 1.3rem;
    color: #ffffff;
}

@media screen and (max-width :65rem){
    /* mobile-header */
    
    .nav__item_mb{
        padding: 1rem 1.5rem;
        border-bottom: 1px solid #ddd;
    }
    .drop_down>ul>li>a{
        font-size: 1.8rem;
        text-decoration: none;
    }    
    
    .logo_mb{
        justify-content: center;
    }
    .logo_mb >a >img{
        width: 17rem;
    }
    .search_mb{
        width: 65%;
    }
    .search_mb>input{
        font-size: 1.5rem;
        width:100%;
        border-radius: .5rem;
        border: none;
        height: 5rem;
    }
    /* mobile content */
    .upload__text{
        text-align: center;
    }
    .upload__titile>p{
        font-size: 2rem;
    }
    .verifiinfo-box{
        width: 100%;
        display: flex;
    }
    .verifiinfo-box i{
        display: none;
    }
    .verifiinfo-box >ul >li{
        font-size: 80%;
    }
    .verifiinfo-box >ul >li> strong{
        font-size: 90%;
    }
    .comment__list-child{
        padding-left: 0rem;
    }
    /* mobile footer */
    .footer__list{
        flex-direction: column;
    }
    .footer__list >li{
        padding-right: 0rem;
        margin-top: 0px;
        margin-left: 0px;
        border-bottom: 1px solid #fff;
        border-right: 0px;
        width: 100%;
        padding-bottom: .7rem;
        padding-top: .7rem;
        padding-left: 1.5rem;
    }

}

	</style>