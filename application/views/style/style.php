<style type="text/css">
    /* ============================== Main =============================== */
    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        outline: none !important;
    }

    /* vietnamese */
    @font-face {
        font-family: 'Roboto';
        font-style: italic;
        font-weight: 400;
        font-display: swap;
        src: local('Roboto Italic'), local('Roboto-Italic'), url(../fonts/KFOkCnqEu92Fr1Mu51xHIzIFKw.woff2) format('woff2');
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
    }

    /* latin */
    @font-face {
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: local('Roboto'), local('Roboto-Regular'), url(../fonts/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* vietnamese */
    @font-face {
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 500;
        font-display: swap;
        src: local('Roboto Medium'), local('Roboto-Medium'), url(../fonts/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2');
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
    }

    /* latin */
    @font-face {
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 500;
        font-display: swap;
        src: local('Roboto Medium'), local('Roboto-Medium'), url(../fonts/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');
        unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
    }

    /* vietnamese */
    @font-face {
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 700;
        font-display: swap;
        src: local('Roboto Bold'), local('Roboto-Bold'), url(../fonts/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2');
        unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
    }

    body {
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        color: #555555;
        margin: 0 auto;
        line-height: 22px;
    }

    img {
        max-width: 100%;
        height: auto;
    }

    ul {
        padding-inline-start: 0px;
    }

    a {
        transition: all 0.5s;
        cursor: pointer;
    }

    a,
    a:hover,
    a:focus {
        text-decoration: none;
    }

    a {
        color: #8acac6;
        transition: all 0.3s;
    }

    a:active,
    a:focus,
    a:hover {
        outline: 0;
    }

    .btn:focus {
        outline: none !important;
    }

    ul {
        margin: 0;
        list-style: none;
    }

    .clear {
        clear: both;
        height: 0;
    }

    .mg-0 {
        margin: 0 !important;
    }

    .mt-0 {
        margin-top: 0px !important;
    }

    .mt-1 {
        margin-top: 10px !important;
    }

    .mt-2 {
        margin-top: 20px !important;
    }

    .mt-3 {
        margin-top: 30px !important;
    }

    .mt-4 {
        margin-top: 40px !important;
    }

    .mt-5 {
        margin-top: 50px !important;
    }

    .mt-6 {
        margin-top: 60px !important;
    }

    .mt-10 {
        margin-top: 100px !important;
    }

    .mb-0 {
        margin-bottom: 0px !important;
    }

    .mb-1 {
        margin-bottom: 10px !important;
    }

    .mb-2 {
        margin-bottom: 20px !important;
    }

    .mb-3 {
        margin-bottom: 30px !important;
    }

    .mb-4 {
        margin-bottom: 40px !important;
    }

    .mb-5 {
        margin-bottom: 50px !important;
    }

    .mb-10 {
        margin-bottom: 100px !important;
    }

    .mb-6 {
        margin-bottom: 60px !important;
    }

    .ml-1 {
        margin-left: 10px !important;
    }

    .ml-2 {
        margin-left: 20px !important;
    }

    .ml-3 {
        margin-left: 30px !important;
    }

    .ml-4 {
        margin-left: 40px !important;
    }

    .ml-5 {
        margin-left: 50px !important;
    }

    .ml-6 {
        margin-left: 60px !important;
    }

    .mr-1 {
        margin-right: 10px !important;
    }

    .mr-2 {
        margin-right: 20px !important;
    }

    .mr-3 {
        margin-right: 30px !important;
    }

    .mr-4 {
        margin-right: 40px !important;
    }

    .mr-5 {
        margin-right: 50px !important;
    }

    .mr-6 {
        margin-right: 60px !important;
    }

    .pd-0 {
        padding: 0 !important;
    }

    .pb-1 {
        padding-bottom: 10px !important;
    }

    .pb-2 {
        padding-bottom: 20px !important;
    }

    .pb-3 {
        padding-bottom: 30px !important;
    }

    .pb-4 {
        padding-bottom: 40px !important;
    }

    .pb-5 {
        padding-bottom: 50px !important;
    }

    .pb-6 {
        padding-bottom: 60px !important;
    }

    .pb-10 {
        padding-bottom: 100px !important;
    }

    .pt-0 {
        padding-top: 0px !important;
    }

    .pt-1 {
        padding-top: 10px !important;
    }

    .pt-2 {
        padding-top: 20px !important;
    }

    .pt-3 {
        padding-top: 30px !important;
    }

    .pt-4 {
        padding-top: 40px !important;
    }

    .pt-5 {
        padding-top: 50px !important;
    }

    .pt-6 {
        padding-top: 60px !important;
    }

    .pl-1 {
        padding-left: 10px !important;
    }

    .pl-2 {
        padding-left: 20px !important;
    }

    .pl-3 {
        padding-left: 30px !important;
    }

    .pl-4 {
        padding-left: 40px !important;
    }

    .pl-5 {
        padding-left: 50px !important;
    }

    .pl-6 {
        padding-left: 60px !important;
    }

    .pr-1 {
        padding-right: 10px !important;
    }

    .pr-2 {
        padding-right: 20px !important;
    }

    .pr-3 {
        padding-right: 30px;
    }

    .pr-4 {
        padding-right: 40px;
    }

    .pr-5 {
        padding-right: 50px;
    }

    .pr-6 {
        padding-right: 60px;
    }

    /*lay-out*/
    .container {
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto
    }

    @media (min-width: 768px) {
        .container {
            width: 750px
        }
    }

    @media (min-width: 992px) {
        .container {
            width: 970px
        }
    }

    @media (min-width: 1200px) {
        .container {
            width: 1170px
        }
    }

    .row {
        margin-right: -15px;
        margin-left: -15px
    }

    .col-lg-1,
    .col-lg-10,
    .col-lg-11,
    .col-lg-12,
    .col-lg-2,
    .col-lg-3,
    .col-lg-4,
    .col-lg-5,
    .col-lg-6,
    .col-lg-7,
    .col-lg-8,
    .col-lg-9,
    .col-md-1,
    .col-md-10,
    .col-md-11,
    .col-md-12,
    .col-md-2,
    .col-md-3,
    .col-md-4,
    .col-md-5,
    .col-md-6,
    .col-md-7,
    .col-md-8,
    .col-md-9,
    .col-sm-1,
    .col-sm-10,
    .col-sm-11,
    .col-sm-12,
    .col-sm-2,
    .col-sm-3,
    .col-sm-4,
    .col-sm-5,
    .col-sm-6,
    .col-sm-7,
    .col-sm-8,
    .col-sm-9,
    .col-xs-1,
    .col-xs-10,
    .col-xs-11,
    .col-xs-12,
    .col-xs-2,
    .col-xs-3,
    .col-xs-4,
    .col-xs-5,
    .col-xs-6,
    .col-xs-7,
    .col-xs-8,
    .col-xs-9 {
        position: relative;
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px
    }

    .col-xs-1,
    .col-xs-10,
    .col-xs-11,
    .col-xs-12,
    .col-xs-2,
    .col-xs-3,
    .col-xs-4,
    .col-xs-5,
    .col-xs-6,
    .col-xs-7,
    .col-xs-8,
    .col-xs-9 {
        float: left
    }

    .col-xs-12 {
        width: 100%
    }

    .col-xs-11 {
        width: 91.66666667%
    }

    .col-xs-10 {
        width: 83.33333333%
    }

    .col-xs-9 {
        width: 75%
    }

    .col-xs-8 {
        width: 66.66666667%
    }

    .col-xs-7 {
        width: 58.33333333%
    }

    .col-xs-6 {
        width: 50%
    }

    .col-xs-5 {
        width: 41.66666667%
    }

    .col-xs-4 {
        width: 33.33333333%
    }

    .col-xs-3 {
        width: 25%
    }

    .col-xs-2 {
        width: 16.66666667%
    }

    .col-xs-1 {
        width: 8.33333333%
    }

    @media (min-width: 768px) {

        .col-sm-1,
        .col-sm-10,
        .col-sm-11,
        .col-sm-12,
        .col-sm-2,
        .col-sm-3,
        .col-sm-4,
        .col-sm-5,
        .col-sm-6,
        .col-sm-7,
        .col-sm-8,
        .col-sm-9 {
            float: left
        }

        .col-sm-12 {
            width: 100%
        }

        .col-sm-11 {
            width: 91.66666667%
        }

        .col-sm-10 {
            width: 83.33333333%
        }

        .col-sm-9 {
            width: 75%
        }

        .col-sm-8 {
            width: 66.66666667%
        }

        .col-sm-7 {
            width: 58.33333333%
        }

        .col-sm-6 {
            width: 50%
        }

        .col-sm-5 {
            width: 41.66666667%
        }

        .col-sm-4 {
            width: 33.33333333%
        }

        .col-sm-3 {
            width: 25%
        }

        .col-sm-2 {
            width: 16.66666667%
        }

        .col-sm-1 {
            width: 8.33333333%
        }

        .col-sm-pull-12 {
            right: 100%
        }

        .col-sm-pull-11 {
            right: 91.66666667%
        }

        .col-sm-pull-10 {
            right: 83.33333333%
        }

        .col-sm-pull-9 {
            right: 75%
        }

        .col-sm-pull-8 {
            right: 66.66666667%
        }

        .col-sm-pull-7 {
            right: 58.33333333%
        }

        .col-sm-pull-6 {
            right: 50%
        }

        .col-sm-pull-5 {
            right: 41.66666667%
        }

        .col-sm-pull-4 {
            right: 33.33333333%
        }

        .col-sm-pull-3 {
            right: 25%
        }

        .col-sm-pull-2 {
            right: 16.66666667%
        }

        .col-sm-pull-1 {
            right: 8.33333333%
        }

        .col-sm-pull-0 {
            right: auto
        }

        .col-sm-push-12 {
            left: 100%
        }

        .col-sm-push-11 {
            left: 91.66666667%
        }

        .col-sm-push-10 {
            left: 83.33333333%
        }

        .col-sm-push-9 {
            left: 75%
        }

        .col-sm-push-8 {
            left: 66.66666667%
        }

        .col-sm-push-7 {
            left: 58.33333333%
        }

        .col-sm-push-6 {
            left: 50%
        }

        .col-sm-push-5 {
            left: 41.66666667%
        }

        .col-sm-push-4 {
            left: 33.33333333%
        }

        .col-sm-push-3 {
            left: 25%
        }

        .col-sm-push-2 {
            left: 16.66666667%
        }

        .col-sm-push-1 {
            left: 8.33333333%
        }

        .col-sm-push-0 {
            left: auto
        }

        .col-sm-offset-12 {
            margin-left: 100%
        }

        .col-sm-offset-11 {
            margin-left: 91.66666667%
        }

        .col-sm-offset-10 {
            margin-left: 83.33333333%
        }

        .col-sm-offset-9 {
            margin-left: 75%
        }

        .col-sm-offset-8 {
            margin-left: 66.66666667%
        }

        .col-sm-offset-7 {
            margin-left: 58.33333333%
        }

        .col-sm-offset-6 {
            margin-left: 50%
        }

        .col-sm-offset-5 {
            margin-left: 41.66666667%
        }

        .col-sm-offset-4 {
            margin-left: 33.33333333%
        }

        .col-sm-offset-3 {
            margin-left: 25%
        }

        .col-sm-offset-2 {
            margin-left: 16.66666667%
        }

        .col-sm-offset-1 {
            margin-left: 8.33333333%
        }

        .col-sm-offset-0 {
            margin-left: 0
        }
    }

    @media (min-width: 992px) {

        .col-md-1,
        .col-md-10,
        .col-md-11,
        .col-md-12,
        .col-md-2,
        .col-md-3,
        .col-md-4,
        .col-md-5,
        .col-md-6,
        .col-md-7,
        .col-md-8,
        .col-md-9 {
            float: left
        }

        .col-md-12 {
            width: 100%
        }

        .col-md-11 {
            width: 91.66666667%
        }

        .col-md-10 {
            width: 83.33333333%
        }

        .col-md-9 {
            width: 75%
        }

        .col-md-8 {
            width: 66.66666667%
        }

        .col-md-7 {
            width: 58.33333333%
        }

        .col-md-6 {
            width: 50%
        }

        .col-md-5 {
            width: 41.66666667%
        }

        .col-md-4 {
            width: 33.33333333%
        }

        .col-md-3 {
            width: 25%
        }

        .col-md-2 {
            width: 16.66666667%
        }

        .col-md-1 {
            width: 8.33333333%
        }

        .col-md-pull-12 {
            right: 100%
        }

        .col-md-pull-11 {
            right: 91.66666667%
        }

        .col-md-pull-10 {
            right: 83.33333333%
        }

        .col-md-pull-9 {
            right: 75%
        }

        .col-md-pull-8 {
            right: 66.66666667%
        }

        .col-md-pull-7 {
            right: 58.33333333%
        }

        .col-md-pull-6 {
            right: 50%
        }

        .col-md-pull-5 {
            right: 41.66666667%
        }

        .col-md-pull-4 {
            right: 33.33333333%
        }

        .col-md-pull-3 {
            right: 25%
        }

        .col-md-pull-2 {
            right: 16.66666667%
        }

        .col-md-pull-1 {
            right: 8.33333333%
        }

        .col-md-pull-0 {
            right: auto
        }

        .col-md-push-12 {
            left: 100%
        }

        .col-md-push-11 {
            left: 91.66666667%
        }

        .col-md-push-10 {
            left: 83.33333333%
        }

        .col-md-push-9 {
            left: 75%
        }

        .col-md-push-8 {
            left: 66.66666667%
        }

        .col-md-push-7 {
            left: 58.33333333%
        }

        .col-md-push-6 {
            left: 50%
        }

        .col-md-push-5 {
            left: 41.66666667%
        }

        .col-md-push-4 {
            left: 33.33333333%
        }

        .col-md-push-3 {
            left: 25%
        }

        .col-md-push-2 {
            left: 16.66666667%
        }

        .col-md-push-1 {
            left: 8.33333333%
        }

        .col-md-push-0 {
            left: auto
        }

        .col-md-offset-12 {
            margin-left: 100%
        }

        .col-md-offset-11 {
            margin-left: 91.66666667%
        }

        .col-md-offset-10 {
            margin-left: 83.33333333%
        }

        .col-md-offset-9 {
            margin-left: 75%
        }

        .col-md-offset-8 {
            margin-left: 66.66666667%
        }

        .col-md-offset-7 {
            margin-left: 58.33333333%
        }

        .col-md-offset-6 {
            margin-left: 50%
        }

        .col-md-offset-5 {
            margin-left: 41.66666667%
        }

        .col-md-offset-4 {
            margin-left: 33.33333333%
        }

        .col-md-offset-3 {
            margin-left: 25%
        }

        .col-md-offset-2 {
            margin-left: 16.66666667%
        }

        .col-md-offset-1 {
            margin-left: 8.33333333%
        }

        .col-md-offset-0 {
            margin-left: 0
        }
    }

    @media (min-width: 1200px) {

        .col-lg-1,
        .col-lg-10,
        .col-lg-11,
        .col-lg-12,
        .col-lg-2,
        .col-lg-3,
        .col-lg-4,
        .col-lg-5,
        .col-lg-6,
        .col-lg-7,
        .col-lg-8,
        .col-lg-9 {
            float: left
        }

        .col-lg-12 {
            width: 100%
        }

        .col-lg-11 {
            width: 91.66666667%
        }

        .col-lg-10 {
            width: 83.33333333%
        }

        .col-lg-9 {
            width: 75%
        }

        .col-lg-8 {
            width: 66.66666667%
        }

        .col-lg-7 {
            width: 58.33333333%
        }

        .col-lg-6 {
            width: 50%
        }

        .col-lg-5 {
            width: 41.66666667%
        }

        .col-lg-4 {
            width: 33.33333333%
        }

        .col-lg-3 {
            width: 25%
        }

        .col-lg-2 {
            width: 16.66666667%
        }

        .col-lg-1 {
            width: 8.33333333%
        }

        .col-lg-pull-12 {
            right: 100%
        }

        .col-lg-pull-11 {
            right: 91.66666667%
        }

        .col-lg-pull-10 {
            right: 83.33333333%
        }

        .col-lg-pull-9 {
            right: 75%
        }

        .col-lg-pull-8 {
            right: 66.66666667%
        }

        .col-lg-pull-7 {
            right: 58.33333333%
        }

        .col-lg-pull-6 {
            right: 50%
        }

        .col-lg-pull-5 {
            right: 41.66666667%
        }

        .col-lg-pull-4 {
            right: 33.33333333%
        }

        .col-lg-pull-3 {
            right: 25%
        }

        .col-lg-pull-2 {
            right: 16.66666667%
        }

        .col-lg-pull-1 {
            right: 8.33333333%
        }

        .col-lg-pull-0 {
            right: auto
        }

        .col-lg-push-12 {
            left: 100%
        }

        .col-lg-push-11 {
            left: 91.66666667%
        }

        .col-lg-push-10 {
            left: 83.33333333%
        }

        .col-lg-push-9 {
            left: 75%
        }

        .col-lg-push-8 {
            left: 66.66666667%
        }

        .col-lg-push-7 {
            left: 58.33333333%
        }

        .col-lg-push-6 {
            left: 50%
        }

        .col-lg-push-5 {
            left: 41.66666667%
        }

        .col-lg-push-4 {
            left: 33.33333333%
        }

        .col-lg-push-3 {
            left: 25%
        }

        .col-lg-push-2 {
            left: 16.66666667%
        }

        .col-lg-push-1 {
            left: 8.33333333%
        }

        .col-lg-push-0 {
            left: auto
        }

        .col-lg-offset-12 {
            margin-left: 100%
        }

        .col-lg-offset-11 {
            margin-left: 91.66666667%
        }

        .col-lg-offset-10 {
            margin-left: 83.33333333%
        }

        .col-lg-offset-9 {
            margin-left: 75%
        }

        .col-lg-offset-8 {
            margin-left: 66.66666667%
        }

        .col-lg-offset-7 {
            margin-left: 58.33333333%
        }

        .col-lg-offset-6 {
            margin-left: 50%
        }

        .col-lg-offset-5 {
            margin-left: 41.66666667%
        }

        .col-lg-offset-4 {
            margin-left: 33.33333333%
        }

        .col-lg-offset-3 {
            margin-left: 25%
        }

        .col-lg-offset-2 {
            margin-left: 16.66666667%
        }

        .col-lg-offset-1 {
            margin-left: 8.33333333%
        }

        .col-lg-offset-0 {
            margin-left: 0
        }
    }

    @media (max-width: 767px) {
        .hidden-xs {
            display: none !important
        }
    }

    @media (min-width: 768px) and (max-width: 991px) {
        .hidden-sm {
            display: none !important
        }
    }

    @media (min-width: 992px) and (max-width: 1199px) {
        .hidden-md {
            display: none !important
        }
    }

    @media (min-width: 1200px) {
        .hidden-lg {
            display: none !important
        }
    }

    .row:after {
        clear: both;
    }

    .container:before,
    .row:after,
    .row:before {
        display: table;
        content: " ";
    }

    :after,
    :before {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .h1,
    .h2,
    .h3,
    .h4,
    .h5,
    .h6,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: inherit;
        font-weight: 500;
        line-height: 1.1;
        color: inherit
    }

    .h1 .small,
    .h1 small,
    .h2 .small,
    .h2 small,
    .h3 .small,
    .h3 small,
    .h4 .small,
    .h4 small,
    .h5 .small,
    .h5 small,
    .h6 .small,
    .h6 small,
    h1 .small,
    h1 small,
    h2 .small,
    h2 small,
    h3 .small,
    h3 small,
    h4 .small,
    h4 small,
    h5 .small,
    h5 small,
    h6 .small,
    h6 small {
        font-weight: 400;
        line-height: 1;
        color: #777
    }

    .h1,
    .h2,
    .h3,
    h1,
    h2,
    h3 {
        margin-top: 20px;
        margin-bottom: 10px
    }

    .h1 .small,
    .h1 small,
    .h2 .small,
    .h2 small,
    .h3 .small,
    .h3 small,
    h1 .small,
    h1 small,
    h2 .small,
    h2 small,
    h3 .small,
    h3 small {
        font-size: 65%
    }

    .h4,
    .h5,
    .h6,
    h4,
    h5,
    h6 {
        margin-top: 10px;
        margin-bottom: 10px
    }

    .h4 .small,
    .h4 small,
    .h5 .small,
    .h5 small,
    .h6 .small,
    .h6 small,
    h4 .small,
    h4 small,
    h5 .small,
    h5 small,
    h6 .small,
    h6 small {
        font-size: 75%
    }

    .h1,
    h1 {
        font-size: 36px
    }

    .h2,
    h2 {
        font-size: 30px
    }

    .h3,
    h3 {
        font-size: 24px
    }

    .h4,
    h4 {
        font-size: 18px
    }

    .h5,
    h5 {
        font-size: 14px
    }

    .h6,
    h6 {
        font-size: 12px
    }

    p {
        margin: 0 0 10px
    }

    .text-left {
        text-align: left
    }

    .text-right {
        text-align: right
    }

    .text-center {
        text-align: center
    }

    .text-justify {
        text-align: justify
    }

    /*end_main*/
    /*Home Page*/
    header {
        height: 90px;
        background: #8acac6;
    }

    #header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        position: relative;
        height: 90px;
    }

    #header .logo {
        display: inline-block;
        /* height: 90px; */
    }

    .form_search {
        position: relative;
    }

    .form_search input {
        padding: 10px 27px 10px 48px;
        min-width: 370px;
        border-radius: 99px;
        border: none;
        background: #F6F6F6;
        color: rgba(153, 153, 153, 1);
        line-height: 20px;
        letter-spacing: 0.16px;

    }

    .form_search .btn_submit {
        border: none;
        background: none;
        cursor: pointer;
        position: absolute;
        left: 18px;
        top: 10px;
        height: 20px;
    }

    .main-menu li {
        color: rgba(255, 255, 255, 1);
        font-size: 14px;
        display: inline-block;
        line-height: 16px;
    }

    .main-menu li:not(:last-child) {
        margin-right: 6px;
    }

    .main-menu li.active a,
    .main-menu li a:hover {
        padding: 7px 21px;
        background: rgba(255, 255, 255, 1);
        border-radius: 4px;
        color: rgba(76, 64, 247, 1);

    }

    .main-menu li a {
        color: inherit;
        font-weight: 500;
        padding: 7px 21px;
        letter-spacing: 0.16px;

    }

    footer {
        height: 112px;
        background: rgba(15, 13, 48, 1);
        padding-top: 25px;
        margin-top: 80px;
        text-align: center;
    }

    .footer-main ul {
        margin: auto;
        display: inline-block;
    }

    .footer-main ul li {
        display: inline-block;
    }

    .footer-main ul li:not(:last-child) {
        margin-right: 46px;
    }

    .footer-main ul li a,
    .footer-main p {
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 16px;
        letter-spacing: 0.16px;
        font-feature-settings: 'pnum'on, 'lnum'on;
        color: #FFFFFF;
        opacity: 0.78;

    }

    .footer-main ul li a:hover {
        text-decoration: underline;
    }

    .footer-main ul li a {
        text-transform: uppercase;
    }

    .line {
        display: block;
        width: 328px;
        border-top: 1px solid #FFFFFF;
        opacity: 0.1;
        margin: 16px auto;
    }

    .copyright a {
        color: #FFBDBD;
    }

    .item_app {
        position: relative;
    }

    .item_app .description {
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 24px;
        padding-right: 30px;
        background: #8acac6;
        opacity: 0.95;
    }

    .item_app.item_right .description {
        padding: 9px 16px 10px 24px;
    }

    .item_app.item_right .description .title {
        font-size: 16px;
    }

    .item_app .images img {
        max-height: 415px;
        width: 100%;
    }

    .item_app.item_right .images img {
        max-height: 200px;
    }

    .item_app.item_right:not(:last-child) {
        margin-bottom: 16px;
    }

    .rankting {
        display: flex;
    }

    .rankting .icon:not(:last-child) {
        margin-right: 6px;
    }

    .item_app.item_right .rankting .icon:not(:last-child) {
        margin-right: 4px;
    }

    .rankting .icon.active path {
        fill: #FDCC0D;
    }

    .description .title {
        font-weight: 500;
        font-size: 24px;
        line-height: 21px;
        letter-spacing: 0.16px;
        color: #FFFFFF;
        margin: 0;
    }

    .description .title a {
        color: inherit;
    }

    .description .title a:hover {
        text-decoration: underline;
    }

    .btn_top_view {
        display: flex;
        align-items: center;
        padding: 17px 17px 17px 29px;
        background: #FFEFEF;
        border-radius: 4px;
    }

    .owl_banner_home .ec__nav {
        top: 50%;
        border-top: 3px solid #fff !important;
        border-right: 3px solid #fff !important;
        width: 10px !important;
        height: 10px !important;
    }

    .list_tag a:hover {
        background: rgb(255 226 226);
        text-decoration: underline;
    }

    .images_app img:hover {
        transform: scale(1.1);
        transition: all .3s ease-out;
    }

    .list_social li a:hover {
        box-shadow: 0px 1px 7px rgba(0, 0, 0, 0.05);
    }

    .btn_top_view:hover,
    .btn_view_more:hover {
        filter: brightness(96%);
        box-shadow: 0px 1px 7px rgba(0, 0, 0, 0.05);
    }

    .btn_view_more:hover,
    .btn_down:hover {
        text-decoration: underline;
    }

    .btn_top_view .icon {
        height: 54px;
        margin-right: 22px;
    }

    .btn_top_view .txt {
        font-size: 20px;
        line-height: 20px;
        font-weight: 500;
        letter-spacing: 0.16px;
        color: rgba(17, 17, 17, 1);
    }

    .bg_EDECFE {
        background: #EDECFE !important;
    }

    .bg_E7F5FF {
        background: #E7F5FF !important;
    }

    .box_line {
        height: 8px;
        background: #EEEEEE;
        width: 100%;
        margin: 40px 0;
    }

    .title-head {
        display: flex;
        align-items: flex-start;
        justify-content: space-between;
    }

    .title-head .title {
        font-weight: 700;
        font-size: 24px;
        line-height: 32px;
        letter-spacing: 0.16px;
        color: rgba(17, 17, 17, 1);
    }

    .btn_view_more,
    .btn_defaulf {
        display: inline-block;
        padding: 6.5px 16px;
        color: rgba(255, 255, 255, 1);
        font-size: 13px;
        line-height: 19px;
        letter-spacing: 0.16px;
        background: #8acac6;
        border-radius: 10px;
    }

    .item_box_app {
        display: flex;
        align-items: center;
        margin-bottom: 20px;
    }

    .item_box_app .images_app {
        width: 80px;
        height: 80px;
        margin-right: 12px;
        display: inline-table;
    }

    .content_right .title {
        font-size: 15px;
        font-weight: 500;
        line-height: 21px;
        color: rgba(17, 17, 17, 1);
        letter-spacing: 0.16px;
        display: -webkit-box;
        overflow: hidden;
        -webkit-line-clamp: 1;
        text-overflow: ellipsis;
        -webkit-box-orient: vertical;
    }

    .content_right .title:hover,
    .box_download_app .box_content .title:hover {
        color: rgba(76, 64, 247, 1);
    }

    .content_right .title a {
        color: inherit;
        font-weight: 500;
    }

    .select_category {
        background-color: #F1F3F8 !important;
    }

    .content_right .rankting .icon:not(:last-child) {
        margin-right: 4px;
    }

    span.date {
        font-size: 11px;
        color: rgba(153, 153, 153, 1);
        line-height: 14px;
    }

    .owl_list_app .item_box_app {
        flex-wrap: wrap;
        padding: 0 5px;
    }

    .owl_list_app .item_box_app .images_app {
        margin: 0;
    }

    .owl_list_app .content_right .title {
        margin-top: 8px !important;
        -webkit-line-clamp: 2;
    }

    .box_download_app {
        border-radius: 4px;
        padding: 32px 24px;
        background: #EDECFE;
        display: flex;
        align-items: start;
    }

    .box_download_app .imgage {
        margin-right: 16px;
    }

    .box_download_app .box_content .title {
        font-size: 18px;
        line-height: 22px;
        font-weight: 500;
        color: rgba(17, 17, 17, 1);
    }

    .box_download_app .box_content .title a {
        color: inherit;
    }

    .box_download_app .box_content p {
        font-weight: 400;
        font-size: 14px;
        color: rgba(153, 153, 153, 1);
    }

    .title_default {
        font-size: 20px;
        color: rgba(17, 17, 17, 1);
        font-weight: 700;
        font-size: 20px;
        line-height: 26px;
    }

    .list_tag a {
        display: inline-block;
        padding: 4px 16px;
        background: rgba(255, 226, 226, 0.6);
        border-radius: 10px;
        margin-right: 12px;
        margin-bottom: 12px;
        font-size: 13px;
        font-weight: 400;
        color: rgba(76, 64, 247, 1);
        line-height: 19px;
    }

    .item_app .images a {
        display: grid;
    }

    .list_social li {
        display: inline-block;
    }

    .list_social li:not(:last-child) {
        margin-right: 22px;
    }

    .box_social {
        background: #F1F1F1;
        padding: 24px 20px 30px;
    }

    .box_social p {
        font-size: 16px;
        line-height: 22px;
        color: #111111;
        text-align: center;
    }

    .btn_readmore:hover,
    .btn_defaulf:hover {
        text-decoration: underline;
    }

    .list_featured {
        display: flex;
    }

    .list_featured .item_featured:not(:last-child) {
        margin-right: 30px;
    }

    .list_featured .item_featured {
        width: 33.33%;
    }

    .breadcrumb li {
        display: inline-block;
        font-weight: 400;
        font-size: 12px;
        line-height: 12px;
        color: rgba(153, 153, 153, 1);
    }

    .breadcrumb li a {
        color: inherit;
    }

    .breadcrumb li.active {
        color: rgba(76, 64, 247, 1);
    }

    .breadcrumb-item+.breadcrumb-item::before {
        display: inline-block;
        padding-right: 6px;
        color: #999999;
        padding-left: 6px;
        content: ">";
    }

    .breadcrumb {
        margin-bottom: 30px;
        /*display: flex;*/
        /*overflow: auto;*/
        /*flex-wrap: unset;*/
    }

    .txt_download .title {
        font-size: 24px;
        line-height: 30px;
        letter-spacing: 0.16px;
        color: rgba(17, 17, 17, 1);
    }

    .download_app {
        display: flex;
        justify-content: space-between;
    }

    .txt_download {
        width: 45%;
        margin-left: 16px;
    }

    .box_download {
        /*width: 40%;*/
    }

    .txt_download .title a {
        color: inherit;
    }

    .des_txt p {
        margin-bottom: 8px;
        font-size: 13px;
        color: rgba(111, 111, 111, 1);
        font-weight: 400;
    }

    .des_txt p span {
        font-weight: 500;
    }

    .rank_download {
        margin-top: 15px;
        display: flex;
        /*justify-content: space-between;*/
    }

    .rank_download .item:not(:last-child) {
        margin-right: 70px
    }

    .rank_download p {
        color: rgba(111, 111, 111, 1);
    }

    .view_rank {
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .view_rank .count_rank {
        font-weight: 500;
        font-size: 24px;
        color: rgba(76, 64, 247, 1);
        margin-right: 12px;
    }

    .btn_download {
        background: #8acac6;
        border-radius: 10px;
        padding: 12px 50px;
        width: 100%;
        display: block;
        color: #fff;
        font-weight: 500;
        text-transform: uppercase;
        min-width: 328px;
        height: 44px;
        text-align: center;
        border: none;
        outline: none;
        cursor: pointer;
    }

    .btn_download:hover {
        background: #8acac6db;
    }

    .btn_outline {
        background: #fff;
        border-radius: 10px;
        padding: 12px 50px;
        width: 100%;
        display: block;
        color: rgba(76, 64, 247, 1);
        font-weight: 500;
        min-width: 328px;
        text-align: center;
        border: 1px solid #8acac6;
        height: 44px;
    }

    .btn_download svg,
    .btn_outline svg {
        vertical-align: middle;
        margin-right: 13px;
    }

    .box_share_social {
        display: flex;
        margin-top: 20px;
        align-items: center;
        justify-content: center;
        font-size: 16px;
        color: rgba(17, 17, 17, 1);
    }

    .box_share_social .list_social {
        margin-left: 14px;
    }

    .box_share_social .list_social li:not(:last-child) {
        margin-right: 16px;
    }

    .title_head {
        display: flex;
        height: 56px;
        background: #EFEEFF;
        font-size: 20px;
        font-weight: 500;
        font-size: 20px;
        line-height: 24px;
        letter-spacing: 0.16px;
        color: #111111;
    }

    .title_head .txt {
        padding: 16px;
    }

    .title_head .count {
        background: #8acac6;
        height: 100%;
        padding: 16px;
        display: flex;
        font-size: 10px;
        text-transform: uppercase;
        line-height: 10px;
        color: #FFFFFF;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
    }

    .title_head .count strong {
        width: 100%;
        text-align: center;
        font-size: 14px;
    }

    .form-control {
        display: block;
        width: 100%;
        height: 44px;
        padding: 4px 16px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #6F6F6F;
        background-color: #fff;
        background-image: none;
        border: 1px solid #EFEEFF;
        box-sizing: border-box;
        border-radius: 10px;
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s
    }

    .form_layout label {
        color: rgba(17, 17, 17, 1);
        font-weight: 500;
        font-size: 14px;
        text-transform: uppercase;
        margin-bottom: 12px;
        display: flex;
    }

    .form_layout {
        background: rgba(239, 238, 255, 0.48);
        padding: 24px;
        padding-bottom: 36px;
    }

    .form_layout table {
        width: 100%;
        padding: 16px 0;
    }

    .form_layout table th,
    .form_layout table td {
        padding: 16px;
        text-align: left;
        border-bottom: 1px solid #E6E4F5;
    }

    .form_layout table td {
        color: #6F6F6F;
        font-size: 13px;
        font-size: 13px;
    }

    .form_layout table tr:last-child td {
        border: none;
    }

    .form_layout table tr {
        border-bottom: 1px solid #E6E4F5;
        line-height: 13px;
    }

    .form_layout table tr td span {
        color: #999999;
        font-size: 10px;
    }

    .btn_down {
        background: #8acac6;
        border-radius: 10px;
        font-weight: 500;
        font-size: 13px;
        padding: 9px;
        min-width: 138px;
        display: block;
        text-align: center;
        color: #ffffff;
        text-transform: uppercase;
        vertical-align: middle;
    }

    .btn_down svg {
        vertical-align: middle;
        margin-right: 5px;
    }

    .owl_app_screen.owl-theme .owl-nav {
        margin-top: 10px;
        position: absolute;
        top: -65px;
        right: 0;
    }

    .owl_app_screen.owl-theme .owl-nav [class*=owl-]:hover {
        background: transparent;
    }

    .box_app_speecs ul {
        padding: 0 16px;
        padding-bottom: 20px;
        background: #F6F6F6;
    }

    .box_app_speecs li {
        display: flex;
        padding: 13px 0;
        color: rgba(111, 111, 111, 1);
    }

    .box_app_speecs li strong {
        min-width: 20%;
        color: rgba(17, 17, 17, 1);
    }

    .box_app_speecs li span {
        word-break: break-word;
        width: 70%;
    }

    .box_app_speecs li:not(:last-child) {
        border-bottom: 1px solid #EEEEEE;
    }

    .box_description p {
        line-height: 22px;
        letter-spacing: 0.16px;
        color: rgba(111, 111, 111, 1);
    }

    .btn_show_more {
        border: 1px solid #8acac6;
        box-sizing: border-box;
        border-radius: 10px;
        padding: 15px;
        display: block;
        width: 100%;
        color: #8acac6;
        font-size: 14px;
        text-align: center;
        font-weight: 500;
    }

    .btn_show_more:hover,
    .btn_outline:hover {
        background: #8acac6;
        color: #fff;
    }

    .btn_show_more:hover:after {
        border: solid #fff;
        border-width: 0 2px 2px 0;
    }

    .btn_show_more:after {
        content: "";
        border: solid #8acac6;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        margin-left: 6px;
        top: -3px;
        position: relative;
    }

    select {
        -webkit-appearance: none;
    }

    .aselect {
        position: relative;
    }

    .aselect:after {
        content: "";
        border: solid #6F6F6F;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
        margin-left: 6px;
        top: 40%;
        right: 20px;
        position: absolute;
    }

    /*.owl_app_screen .item{*/
    /*    padding-right: 30px;*/
    /*}*/
    .owl_app_screen.ec .ec__nav {
        bottom: auto;
        top: -40px;
    }

    .owl_app_screen.ec .ec__nav--prev {
        left: auto;
        right: 30px;
    }

    .owl_app_screen.ec .ec__nav {
        position: absolute;
        bottom: 10px;
        border-top: 2px solid #8acac6;
        border-right: 2px solid #8acac6;
        width: 10px;
        height: 10px;
        cursor: pointer;
    }

    /*Reponsive*/

    @media only screen and (max-device-width : 991px) {
        .aselect {
            width: 100%;
        }

        .aselect:after {
            top: 40%;
        }

        .breadcrumb {
            margin-bottom: 30px;
            display: flex;
            overflow: auto;
            flex-wrap: unset;
            padding: 0.75rem 0rem;

        }

        .breadcrumb li {
            white-space: nowrap;
        }

        .breadcrumb::-webkit-scrollbar {
            width: 10px;
            height: 2px;
        }

        .breadcrumb::-webkit-scrollbar-thumb {
            background: #999;
        }

        .breadcrumb::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /*.menu-box {*/
        /*    width: 100%;*/
        /*    height: 100%;*/
        /*    position: fixed!important;*/
        /*    top: 0;*/
        /*    z-index: 10000;*/
        /*    opacity: 0;*/
        /*    visibility: hidden;*/
        /*    -webkit-transition-duration: .4s;*/
        /*    -o-transition-duration: .4s;*/
        /*    transition-duration: .4s;*/
        /*    -webkit-transition-timing-function: cubic-bezier(.4,0,.2,1);*/
        /*    -o-transition-timing-function: cubic-bezier(.4,0,.2,1);*/
        /*    transition-timing-function: cubic-bezier(.4,0,.2,1);*/
        /*    -webkit-transition-property: opacity,-webkit-transform;*/
        /*    transition-property: opacity,-webkit-transform;*/
        /*    -o-transition-property: transform,opacity;*/
        /*    transition-property: transform,opacity;*/
        /*    transition-property: transform,opacity,-webkit-transform;*/
        /*}*/
        /*.main-nav.opened {*/
        /*    -webkit-transform: none;*/
        /*    -ms-transform: none;*/
        /*    transform: none;*/
        /*    visibility: visible;*/
        /*    opacity: 1;*/

        /*}*/
        /*.main-nav.opened  {*/
        /*    width: 100%;*/
        /*}*/
        /*li.menu-item-has-children .sub-menu {*/
        /*    position: relative;*/
        /*    width: 100%;*/
        /*    left: 0;*/
        /*    top: auto !important;*/
        /*    margin-top: 0px !important;*/
        /*    overflow: hidden;*/
        /*    display: none;*/
        /*    transform: none!important;*/
        /*    -webkit-transform: none!important;*/
        /*    transition: none;*/
        /*}*/
        /*.main-menu {*/
        /*    position: relative;*/
        /*    z-index: 99;*/
        /*    width: 70%;*/
        /*    max-width: 95%;*/
        /*    height: 100%;*/
        /*    overflow-x: hidden;*/
        /*    overflow-y: auto;*/
        /*    background-color: #8acac6;*/
        /*    float: right;*/

        /*}*/
        /*.menu-box {*/
        /*    position: fixed!important;*/
        /*    top: 0px;*/
        /*    right: 0;*/
        /*    width: 0;*/
        /*    height: 100vh;*/
        /*    z-index: 999;*/
        /*    transition: all 0.3s;*/
        /*    overflow: hidden;*/
        /*}*/
        /*.menu-item-has-children a i{*/
        /*    position: absolute;*/
        /*    right: 15px;*/
        /*    top: 25px;*/
        /*    opacity: 0.5;*/
        /*}*/
        /*.menu-site .main-menu li a {*/
        /*    background-color: #8acac6;*/
        /*}*/
        /*.menu-item-has-children ul li{*/
        /*    border-bottom: 1px solid #e1e1e1;*/
        /*}*/
        /*.menu-site .main-menu>li {*/
        /*    display: block;*/
        /*    float: none;*/
        /*    text-align: left;*/
        /*    position: relative;*/
        /*}*/
        /*.menu-site .main-menu li a {*/
        /*    padding: 10px 40px;*/
        /*    display: block;*/
        /*    font-weight: 600;*/
        /*    font-size: 20px;*/
        /*    font-weight: 700;*/
        /*    line-height: 50px;*/
        /*    color: #fff;*/
        /*}*/
        /*.main-menu li.active a, .main-menu li a:hover {*/
        /*    text-decoration: underline;*/
        /*}*/
        /*li.menu-item-has-children .sub-menu li a{*/
        /*    background-color: #fff;*/
        /*    color: #333;*/
        /*}*/
        /*.menu-site .main-menu>li{*/
        /*    padding: 0;*/
        /*}*/
        /*.menu-box.opened {*/
        /*    width: 100%;*/
        /*    opacity: 1;*/
        /*    visibility:inherit;*/
        /*}*/
        /*.menu-site {*/
        /*    float: right;*/
        /*    width: 86%;*/
        /*    position: absolute;*/
        /*    z-index: 9999;*/
        /*    top: 0;*/
        /*    background: #fff;*/
        /*}*/
        /*.menu-box {*/
        /*    margin-left: 0;*/
        /*}*/

        /*.menu-box .bg-menu {*/
        /*    position: absolute;*/
        /*    left: 0;*/
        /*    top: 0;*/
        /*    width: 100%;*/
        /*    height: 100%;*/
        /*    background-color: hsla(0, 0%, 50%, 0.41);*/
        /*    background-color: rgba(36,38,43,.5);*/
        /*    z-index: 98;*/
        /*}*/
        /*.form_search input{*/
        /*    min-width: 218px;*/
        /*}*/
        /*.hamburger-menu{*/
        /*    position: relative;*/
        /*    cursor: pointer;*/
        /*}*/
        /*.hamburger-menu .hamburger .hamburger_line_1, .hamburger-menu .hamburger .hamburger_line_2, .hamburger-menu .hamburger .hamburger_line_3 {*/
        /*    position: absolute;*/
        /*    right: 0;*/
        /*    top: 0px;*/
        /*    width: 20px;*/
        /*    border-bottom: 2px solid currentColor;*/
        /*}*/
        /*.hamburger{*/
        /*    color: #fff;*/
        /*}*/
        /*.hamburger-menu .hamburger .hamburger_line_1{*/
        /*    width: 10px;*/
        /*}*/
        /*.hamburger-menu .hamburger .hamburger_line_2 {*/
        /*    top: 6px;*/
        /*}*/
        /*.hamburger-menu .hamburger .hamburger_line_3 {*/
        /*    top: 12px;*/

        /*}*/

        #header>* {
            padding: 0;
        }

        header,
        #header {
            height: 70px;
            position: relative;
        }

        .section_banner {
            margin: 16px 0 !important;
        }

        .section_hot_app {
            margin-top: 0 !important;
        }

        .description .title {
            font-size: 16px;
        }

        .item_app .description {
            padding: 10px 16px;
        }

        .owl-theme .owl-nav.disabled+.owl-dots {
            position: absolute;
            bottom: 6px;
            right: 0;
            margin: 0;
        }

        .owl-theme .owl-dots .owl-dot span {
            width: 4px;
            height: 4px;
        }

        .btn_top_view .icon {
            margin: 0 auto;
            margin-bottom: 15px;
        }

        .btn_top_view .txt {
            font-size: 10px;
            line-height: 18px;
            width: 100%;
            text-align: center;
        }

        .list_featured .item_featured:not(:last-child) {
            margin-right: 8px;
        }

        .section_featured {
            margin: 16px 0 !important;
        }

        .box_line {
            width: calc(100% + 30px);
            position: relative;
            margin: 32px 0 36px 0;
            display: flex;
            margin-left: -15px;
        }

        .title-head .title {
            font-size: 18px;
        }

        .title-head {
            margin-bottom: 20px !important;
        }

        .item_box_app {
            margin-bottom: 16px;
        }

        .owl_related .item_box_app {
            flex-wrap: wrap;
        }

        /*.item_box_app .images_app {*/
        /*    width: 48px;*/
        /*    height: 48px;*/

        /*}*/
        .list_app_mb .images_app {
            width: 48px;
            height: 48px;
        }

        footer {
            margin-top: 40px;
        }

        .download_app {
            flex-wrap: wrap;
        }

        .download_app .images {
            width: 20%;
        }

        .download_app .txt_download {
            width: 80%;
            margin-left: 0;
            padding-left: 16px;
            margin-bottom: 20px;
        }

        .rank_download .item:not(:last-child) {
            margin-right: 70px;
            padding-left: 10px;
        }

        .box_share_social .list_social {
            display: flex;
            align-items: center;
        }

        .box_share_social {
            align-items: center;
            justify-content: flex-start;
            margin-top: 40px;
        }

        .title_head {
            line-height: 20px;
            height: 64px;
        }

        .title_head .txt {
            font-size: 16px;
        }

        .form-group .row {
            display: block !important;
        }

        .full_mobile {
            margin-left: -15px;
            margin-right: -15px;
        }

        .form_layout table tr:nth-child(2n) td {
            border-bottom: 1px solid #E6E4F5;
        }

        .form_layout table tr td,
        .form_layout table tr:last-child td {
            border: none;
        }

        .btn_down {
            display: inline-block;
            float: right;
        }

        .box_app_speecs li strong {
            width: 30%;
        }

        .select_category {
            max-width: 100% !important;
            margin-top: 20px;
        }
    }

    #owl_app_screen-ow::after {
        width: 35%;
        position: absolute;
        right: 0;
        content: "";
        height: 100%;
        background: linear-gradient(270deg, #FFFFFF 28.15%, rgba(255, 255, 255, 0) 106.69%);
        opacity: 0.9;
        border-radius: 4px;
    }

    .owl_app_screen {
        position: relative;
    }

    /*Mobile*/
    @media only screen and (min-device-width : 280px) and (max-device-width : 480px) {

        .form_layout table th,
        .form_layout table td {
            padding: 12px;
            line-height: 16px;
        }

        .btn_top_view {
            flex-wrap: wrap;
            align-items: flex-start;
            padding: 21px 5px 18px 5px;
            text-align: center;
            min-height: 144px;
        }

        .btn_show_menu {
            width: 20px;
            height: 18px;
        }

        .menu-box {
            width: 100%;
            height: 100%;
            position: fixed !important;
            top: 0;
            z-index: 10000;
            opacity: 0;
            visibility: hidden;
            -webkit-transition-duration: .4s;
            -o-transition-duration: .4s;
            transition-duration: .4s;
            -webkit-transition-timing-function: cubic-bezier(.4, 0, .2, 1);
            -o-transition-timing-function: cubic-bezier(.4, 0, .2, 1);
            transition-timing-function: cubic-bezier(.4, 0, .2, 1);
            -webkit-transition-property: opacity, -webkit-transform;
            transition-property: opacity, -webkit-transform;
            -o-transition-property: transform, opacity;
            transition-property: transform, opacity;
            transition-property: transform, opacity, -webkit-transform;
        }

        .main-nav.opened {
            -webkit-transform: none;
            -ms-transform: none;
            transform: none;
            visibility: visible;
            opacity: 1;

        }

        .main-nav.opened {
            width: 100%;
        }

        li.menu-item-has-children .sub-menu {
            position: relative;
            width: 100%;
            left: 0;
            top: auto !important;
            margin-top: 0px !important;
            overflow: hidden;
            display: none;
            transform: none !important;
            -webkit-transform: none !important;
            transition: none;
        }

        .main-menu {
            position: relative;
            z-index: 99;
            width: 100%;
            max-width: 100%;
            height: 100%;
            overflow-x: hidden;
            overflow-y: auto;
            background-color: #8acac6;
            /* float: right; */
            display: flex;
            padding-top: 30%;
            justify-content: center;
        }

        .menu-box {
            position: fixed !important;
            top: 0px;
            right: 0;
            width: 0;
            height: 100vh;
            z-index: 999;
            transition: all 0.3s;
            overflow: hidden;
        }

        .menu-item-has-children a i {
            position: absolute;
            right: 15px;
            top: 25px;
            opacity: 0.5;
        }

        .menu-site .main-menu li a {
            /* background-color: #8acac6; */
            color: #fff;
        }

        .menu-item-has-children ul li {
            border-bottom: 1px solid #e1e1e1;
        }

        .menu-site .main-menu>li {
            /* display: block; */
            /* float: none; */
            /* text-align: left; */
            /* position: relative; */
        }

        .menu-site .main-menu li a {
            /* padding: 10px 40px; */
            display: inline;
            font-weight: 600;
            font-size: 16px;
            font-weight: 700;
            line-height: 50px;
            color: #fff;
        }

        .main-menu li.active a,
        .main-menu li a:hover {
            /* text-decoration: underline; */
            color: #8acac6;
        }

        li.menu-item-has-children .sub-menu li a {
            background-color: #fff;
            color: #333;
        }

        .menu-site .main-menu>li {
            /* padding: 0; */
        }

        .menu-box.opened {
            width: 100%;
            opacity: 1;
            visibility: inherit;
        }

        .menu-site {
            float: right;
            width: 86%;
            position: absolute;
            z-index: 9999;
            top: 0;
            background: #fff;
        }

        .menu-box {
            margin-left: 0;
        }

        .menu-box .bg-menu {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: hsla(0, 0%, 50%, 0.41);
            background-color: rgba(36, 38, 43, .5);
            z-index: 98;
        }

        .form_search input {
            min-width: 218px;
        }

        .hamburger-menu {
            position: relative;
            cursor: pointer;
        }

        .hamburger-menu .hamburger .hamburger_line_1,
        .hamburger-menu .hamburger .hamburger_line_2,
        .hamburger-menu .hamburger .hamburger_line_3 {
            position: absolute;
            right: 0;
            top: 0px;
            width: 20px;
            border-bottom: 2px solid currentColor;
        }

        .hamburger {
            color: #fff;
        }

        .hamburger-menu .hamburger .hamburger_line_1 {
            width: 10px;
        }

        .hamburger-menu .hamburger .hamburger_line_2 {
            top: 6px;
        }

        .hamburger-menu .hamburger .hamburger_line_3 {
            top: 12px;

        }

        #search_results ul:before {
            left: 70px !important;
        }

        #search_results.opened {
            display: block !important;
            position: absolute;
            top: 100% !important;
            background: #FFFFFF;
            box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
            border-radius: 8px;
            width: 80% !important;
            left: 0 !important;
            right: 0 !important;
            margin: auto !important;
            z-index: 9999;
        }

        #brand_sel {
            display: none;
            position: absolute;
            top: 100% !important;
            background: #FFFFFF;
            box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
            border-radius: 8px;
            width: 80% !important;
            left: 0 !important;
            right: 0 !important;
            margin: auto !important;
            z-index: 9999;
        }

        #model_sel {
            display: none;
            position: absolute;
            top: 100% !important;
            background: #FFFFFF;
            box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
            border-radius: 8px;
            width: 80% !important;
            left: 0 !important;
            right: 0 !important;
            margin: auto !important;
            z-index: 9999;
        }

        .owl-carousel img {
            width: 100%;
        }

        .owl_app_screen .item {
            padding-right: 0px !important;
        }

        .modal-content {
            width: 100% !important;
        }

        .modal {

            background-color: black;
            padding-top: 50% !important;

        }

        .prev,
        .next {
            width: 28px !important;
            height: 28px !important;
            padding: 0 !important;
            justify-content: center;
        }

        .prev svg,
        .next svg {
            width: 10px;
            height: 10px;
        }

        .modal-content .close {
            color: white;
            position: absolute;
            top: -70%;
            right: 20px;
            width: 32px;
            height: 32px;
            font-size: 35px;
            font-weight: bold;
            background: #fffdfd9e;
        }

    }

    @media only screen and (min-device-width : 280px) and (max-device-width : 320px) {
        #header {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0 10px;
        }

        #search_results ul:before {
            left: 70px !important;
        }

        #search_results.opened {
            display: block !important;
            position: absolute;
            top: calc(100% + 12px) !important;
            background: #FFFFFF;
            box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
            border-radius: 8px;
            width: 250px !important;
            left: -20% !important;
            right: 0 !important;
            margin: auto !important;
            z-index: 9999;
        }

        .form_search {
            position: relative;
            text-align: center;
        }

        .form_search input {
            min-width: 150px !important;
            width: 70%;
            font-size: 12px;
        }

        .title-head .title {
            font-size: 12px;
        }

        .icon svg {
            width: 10px;
            margin-right: -3px;
        }

        .item_box_app .images_app {
            width: 60px;
            height: 60px;
            margin-right: 12px;
            display: inline-table;
        }

        .box_download {
            width: 100%;
        }

        .btn_download,
        .btn_outline {
            min-width: auto;
            font-size: 12px;
            padding: 12px 20px;
        }

        .txt_download .title {
            font-size: 20px;
            line-height: 25px;
        }

        .box_app_speecs li strong {
            width: 45%;
        }

        .form_layout table th,
        .form_layout table td {
            padding: 5px;
            line-height: 16px;
        }

        .title_head .txt {
            font-size: 14px;
        }

        .form_layout {
            width: 100%;
            overflow-x: auto;
        }

        .list_app_mb .images_app {
            width: 40px;
            height: 40px;
        }

        .form_search .btn_submit {
            border: none;
            background: none;
            cursor: pointer;
            position: absolute;
            left: 45px;
            top: 10px;
            height: 20px;
        }

        html,
        body {
            overflow-x: hidden;
        }

        .list_social li:not(:last-child) {
            margin-right: 10px;
        }

        .btn_view_more,
        .btn_defaulf {
            font-size: 10px;
        }

    }

    @media only screen and (min-device-width : 475px) {
        .btn_top_view {
            justify-content: center;
            font-size: 10px;
            flex-wrap: wrap;
        }
    }

    /*Ipad
@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) {
    .form_search input {
        min-width: 320px;
    }
    .btn_download{
    min-width: 100%!important;
}
}


/*Ipad-pro*/
    @media only screen and (min-width: 1024px) and (max-height: 1366px) and (-webkit-min-device-pixel-ratio: 1.5) {}

    @media only screen and (min-device-width : 1920px) {}

    @media only screen and (max-device-width : 1200px) {
        .form_search input {
            min-width: 100%;
        }

    }

    .list_more_dev {
        display: flex;

    }

    .list_more_dev .item_box_app {
        width: calc(100%/7);
        flex-direction: column;
        align-items: flex-start;
    }

    .list_more_dev .content_right .title {
        margin-top: 8px !important;
        -webkit-line-clamp: 2;
    }

    /* select Device */
    .input_search {
        display: none;
    }

    #brand_sel {
        /* display: block !important; */
        position: absolute;
        top: 100%;
        background: #FFFFFF;
        box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
        border-radius: 8px;
        width: 100%;
        left: 0;
        right: 0;
        margin: auto;
        padding: 10px;
        z-index: 9999;
    }

    #model_sel {
        /* display: block !important; */
        position: absolute;
        top: 100%;
        background: #FFFFFF;
        box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
        border-radius: 8px;
        width: 100%;
        left: 0;
        right: 0;
        margin: auto;
        padding: 10px;
        z-index: 9999;
    }

    .input_search ul {
        position: relative;
        padding: 16px;
        max-height: 225px;
        overflow: auto;

    }

    .input_search ul:before {
        content: '';
        position: absolute;
        bottom: 100%;
        left: 25px;
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-bottom: 10px solid #fff;
        /*box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);*/
        margin-left: -10px;
        transition: inherit;
        z-index: 2;
    }

    /* end select Device */

    /*//css new*/
    #search_results {
        display: none;
    }

    #search_results.opened {
        display: block !important;
        position: absolute;
        top: 100%;
        background: #FFFFFF;
        box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
        border-radius: 8px;
        width: 370px;
        left: 0;
        right: 0;
        margin: auto;
        z-index: 9999;
    }

    #search_results ul {
        position: relative;
        padding: 16px;

    }

    #search_results ul:before {
        content: '';
        position: absolute;
        bottom: 100%;
        left: 25px;
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-bottom: 10px solid #fff;
        /*box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);*/
        margin-left: -10px;
        transition: inherit;
        z-index: 2;
    }

    #search_results ul li .item .images {
        width: 40px;
        height: 40px;
        margin-right: 12px;
    }

    #search_results ul li .item .title {
        font-size: 15px;
        font-weight: 500;
        display: -webkit-box;
        overflow: hidden;
        -webkit-line-clamp: 1;
        text-overflow: ellipsis;
        -webkit-box-orient: vertical;
    }

    #search_results ul li .item .title a {
        color: inherit;
    }

    #search_results ul li .item {
        padding: 16px 0;
        color: #333;
    }

    #search_results ul li:not(:last-child) .item {
        border-bottom: 1px solid #E9EDF0;
    }

    #search_results ul li .item p {
        color: #6F6F6F;
        font-size: 11px;
        display: -webkit-box;
        overflow: hidden;
        -webkit-line-clamp: 1;
        text-overflow: ellipsis;
        -webkit-box-orient: vertical;
        line-height: 14px;
    }

    #search_results ul li .item {
        display: flex;
        align-items: flex-start;
    }

    .select-dropdown {
        position: relative;
        display: inline-block;
        max-width: 100%;
        margin: 0 15px 0 0;
        width: 100%;
    }

    .select-dropdown::last-child {
        margin-right: 0px;
    }

    .select-dropdown__button {
        display: block;
        width: 100%;
        height: 44px;
        padding: 4px 16px;
        font-size: 14px;
        line-height: 1.42857143;
        text-align: left;
        color: #6F6F6F;
        background-color: #fff;
        background-image: none;
        border: 1px solid #EFEEFF;
        box-sizing: border-box;
        border-radius: 10px;
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }

    .select-dropdown__button::focus {
        outline: none;
    }

    .select-dropdown__button .zmdi-chevron-down {
        position: absolute;
        right: 10px;
        top: 12px;
    }

    .select-dropdown__list {
        position: absolute;
        display: block;
        left: 0;
        right: 0;
        top: calc(100% + 5px);
        max-height: 300px;
        /*overflow: auto;*/
        margin: 0;
        padding: 0;
        /*border: 1px solid #E9EDF0;*/
        border-top: 0px solid transparent;
        list-style-type: none;
        opacity: 0;
        pointer-events: none;
        transform-origin: top left;
        transform: scale(1, 0);
        transition: all ease-in-out 0.3s;
        z-index: 2;
        background: #FFFFFF;
        box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16);
        border-radius: 8px;
    }

    .select-dropdown__list:before {
        content: '';
        position: absolute;
        bottom: 100%;
        left: 25px;
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-bottom: 10px solid #fff;
        /* box-shadow: 0px 1px 6px rgba(0, 0, 0, 0.16); */
        margin-left: -10px;
        transition: inherit;
        z-index: 2;
    }

    .select-dropdown__list.active {
        opacity: 1;
        pointer-events: auto;
        transform: scale(1, 1);
    }

    .select-dropdown__list-item {
        display: block;
        list-style-type: none;
        padding: 10px 15px;
        background: #fff;
        border-top: 1px solid #E9EDF0;
        font-size: 14px;
        line-height: 1.4;
        cursor: pointer;
        color: #616161;
        text-align: left;
        transition: all ease-in-out 0.3s;
    }

    .select-dropdown__list-item:hover {
        background-color: #8acac6;
        color: #fff;
        transition: all ease-in-out 0.3s;
    }

    /*//modal*/
    .column {
        float: left;
        width: 25%;
    }

    /* The Modal (background) */
    .modal {
        display: none;
        position: fixed;
        z-index: 1;
        /*padding-top: 100px;*/
        align-items: center;
        justify-content: center;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgba(0, 0, 0, 0.63);
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        /*background-color: #fefefe;*/
        margin: auto;
        padding: 0;
        width: 970px;
        max-width: 1200px;
    }

    /* The Close Button */
    .close {
        color: white;
        position: absolute;
        top: -20px;
        right: -20px;
        font-size: 35px;
        font-weight: bold;
        background: #00000038;
        width: 44px;
        height: 44px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 50%;
        box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.16);
        z-index: 999;
    }

    .menu-box .close {
        top: 48px;
        right: 20px;
        width: 32px;
        height: 32px;
        background: #ffffff63;
    }

    .close:hover,
    .close:focus {
        color: #999;
        text-decoration: none;
        cursor: pointer;
    }

    .mySlides {
        display: none;
    }

    .owl-carousel img,
    .cursor {
        cursor: pointer;
    }

    /* Next & previous buttons */
    .prev,
    .next {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: 44px;
        height: 44px;
        border-radius: 50%;
        padding: 16px;
        /*margin-top: -50px;*/
        color: white;
        font-weight: bold;
        font-size: 20px;
        transition: 0.6s ease;
        user-select: none;
        -webkit-user-select: none;
        border: 1px solid #FFFFFF;
        display: flex;
        align-items: center;
    }

    /* Position the "next button" to the right */
    .next {
        right: 16px;
    }

    .prev {
        left: 16px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover,
    .next:hover {
        background: #0A84FF;
        opacity: 0.87;
        box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.16);
        border: 1px solid #0A84FF;
    }

    /* Number text (1/3 etc) */
    .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    .caption-container {
        text-align: center;
        background-color: black;
        padding: 2px 16px;
        color: white;
    }

    .demo {
        opacity: 0.6;
    }

    .active,
    .demo:hover {
        opacity: 1;
    }

    img.hover-shadow {
        transition: 0.3s;
    }

    .hover-shadow:hover {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    .select_category button {
        background-color: #F1F3F8 !important;
        border-radius: 4px;
    }

    li.page-item a {
        min-width: 40px;
        min-height: 40px;
        padding: 10px;
        display: block;
        border: 1px solid #CCCCCC;
        box-sizing: border-box;
        border-radius: 4px;
        margin-right: 10px;
        color: black;
    }

    li.page-item a:hover {
        color: #FFFFFF;
        background: #8acac6;
    }

    ul.pagination li {
        display: inline-block;
    }

    ul.pagination li a.disabled {
        background: #F0F4F7;
        border: 1px solid #F0F4F7;
    }

    ul.pagination li a.active {
        background: #8acac6;
        border: 1px solid #8acac6;
        color: #fff;
    }

    .select__main {
        position: relative;
        display: inline-block;
        user-select: none;
        color: #666;
        width: 100%;
        display: block;
        width: 100%;

    }

    .select__main .select__single--selected {
        display: flex;
        cursor: pointer;
        width: 100%;
        height: 44px;
        padding: 4px 16px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #6f6f6f;
        background-color: #fff;
        background-image: none;
        border: 1px solid #efeeff;
        box-sizing: border-box;
        border-radius: 10px;
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }

    .select__main .select__single--selected.select__disabled {
        background-color: #dcdee2;
        cursor: not-allowed;
    }

    .select__main .select__single--selected.select__open {
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .select__main .select__single--selected .placeholder {
        display: flex;
        align-items: center;
        flex: 1 1 100%;
        text-align: left;
        width: calc(100% - 30px);
        line-height: 1em;
    }

    .select__main .select__single--selected .placeholder * {
        display: flex;
        align-items: center;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 100%;
    }

    .select__main .select__single--selected .placeholder .select__disabled {
        cursor: pointer;
        color: #dcdee2;
    }

    .select__main .select__single--selected .select__arrow {
        display: flex;
        align-items: center;
        justify-content: flex-end;
        flex: 0 1 auto;
        margin: -1px 4px 0 5px;
    }

    .select__main .select__single--selected .select__arrow span {
        border: solid #666;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        transition: transform 0.2s, margin 0.2s;
    }

    .select__main .select__single--selected .select__arrow span.arrow-up {
        transform: rotate(-135deg);
        margin: 3px 0 0 0;
    }

    .select__main .select__single--selected .select__arrow span.arrow-down {
        transform: rotate(45deg);
        margin: -3px 0 0 0;
    }

    .select__main .select__multi--selected {
        display: flex;
        flex-direction: row;
        cursor: pointer;
        min-height: 52px;
        width: 100%;
        padding: 0 0 0 3px;
        border: 1px solid #dcdee2;
        border-radius: 0px;
        background-color: #fff;
        outline: 0;
        box-sizing: border-box;
        transition: background-color 0.3s;
    }

    .select__main .select__multi--selected.select__disabled {
        background-color: #dcdee2;
        cursor: not-allowed;
    }

    .select__main .select__multi--selected.select__disabled .select__values .select__disabled {
        color: #666;
    }

    .select__main .select__multi--selected.select__disabled .select__value--delete {
        cursor: not-allowed;
    }

    .select__main .select__multi--selected.select__open {
        border-bottom-left-radius: 0px;
        border-bottom-right-radius: 0px;
    }

    .select__main .select__multi--selected.select__values {
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-start;
        flex: 1 1 100%;
        width: calc(100% - 30px);
    }

    .select__main .select__multi--selected.select__values .select__disabled {
        display: flex;
        padding: 4px 5px;
        margin: 2px 0px;
        line-height: 1em;
        align-items: center;
        width: 100%;
        color: #dcdee2;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    @keyframes scaleIn {
        0% {
            transform: scale(0);
            opacity: 0;
        }

        100% {
            transform: scale(1);
            opacity: 1;
        }
    }

    @keyframes scaleOut {
        0% {
            transform: scale(1);
            opacity: 1;
        }

        100% {
            transform: scale(0);
            opacity: 0;
        }
    }

    .select__main .select__multi--selected.select__values .select__value {
        display: flex;
        user-select: none;
        align-items: center;
        font-size: 12px;
        padding: 3px 5px;
        margin: 3px 5px 3px 0px;
        color: #fff;
        background-color: #5897fb;
        border-radius: 0px;
        animation-name: scaleIn;
        animation-duration: 0.2s;
        animation-timing-function: ease-out;
        animation-fill-mode: both;
    }

    .select__main .select__multi--selected.select__values .select__value.select__out {
        animation-name: scaleOut;
        animation-duration: 0.2s;
        animation-timing-function: ease-out;
    }

    .select__main .select__multi--selected.select__values .select__value--delete {
        margin: 0 0 0 5px;
        cursor: pointer;
    }

    .select__main .select__multi--selected.select .select__add {
        display: flex;
        flex: 0 1 3px;
        margin: 9px 12px 0 5px;
    }

    .select__main .select__multi--selected.select .select__add .select__plus {
        display: flex;
        justify-content: center;
        align-items: center;
        background: #666;
        position: relative;
        height: 10px;
        width: 2px;
        transition: transform 0.2s;
    }

    .select__main .select__multi--selected.select .select__add .select__plus:after {
        background: #666;
        content: "";
        position: absolute;
        height: 2px;
        width: 10px;
        left: -4px;
        top: 4px;
    }

    .select__main .select__multi--selected.select .select__add .select__plus.select__cross {
        transform: rotate(45deg);
    }

    .select__main .select__content {
        position: absolute;
        width: 100%;
        box-sizing: border-box;
        border: solid 1px #dcdee2;
        border-top: 0;
        z-index: 1010;
        background-color: #fff;
        box-shadow: 0 4px 5px rgba(0, 0, 0, 0.15);
        transform-origin: center top;
        transition: transform 0.2s, opacity 0.2s;
        opacity: 0;
        transform: scaleY(0);
    }

    .select__main .select__content.select__open {
        display: block;
        opacity: 1;
        transform: scaleY(1);
    }

    .select__main .select__content .select__search {
        padding: 6px 10px 6px 10px;
    }

    .select__main .select__content .select__search input {
        display: block;
        font-size: inherit;
        width: 100%;
        height: 44px;
        padding: 4px 16px;
        border: 1px solid #dcdee2;
        border-radius: 0px;
        background-color: #fff;
        outline: 0;
        box-sizing: border-box;
    }

    .select__main .select__content .select__search input::placeholder {
        color: #b3b3b3;
    }

    .select__main .select__content .select__search input:focus {
        box-shadow: 0 0 5px #5897fb;
    }

    .select__main .select__content .select__list {
        max-height: 200px;
        overflow-x: hidden;
        overflow-y: auto;
        text-align: left;
    }

    .select__main .select__content .select__list .select__optgroup--label {
        padding: 6px 10px 6px 10px;
        font-weight: bold;
    }

    .select__main .select__content .select__list .select__optgroup .select__option {
        padding: 6px 6px 6px 25px;
    }

    .select__main .select__content .select__list .select__option {
        padding: 5px 8px 5px 8px;
        cursor: pointer;
        user-select: none;
    }

    .select__main .select__content .select__list .select__option * {
        display: inline-block;
        vertical-align: middle;
    }

    .select__main .select__content .select__list .select__option:hover,
    .select__main .select__content .select__list .select__option.select__highlighted {
        color: #fff;
        background-color: #5897fb;
    }

    .select__main .select__content .select__list .select__option.select__disabled {
        cursor: default;
        color: #dcdee2;
        background-color: #fff;
    }

    .select__main .select__content .select__list .select__option.select__hide {
        display: none;
    }

    /*//Css update*/
    #carousel-multiple-items-ow .tns-nav {
        text-align: center;
        margin: 10px 0;
        position: absolute;
        bottom: 0;
        right: 0;
        z-index: 999;
    }

    #carousel-multiple-items-ow .tns-controls [aria-controls] {
        font-size: 15px;
        margin: 0 5px;
        width: auto;
        height: auto;
        border-radius: 50%;
        color: #000;
        background: transparent;
        /* border-radius: 3px; */
        display: flex;
        border: none;
        z-index: 999;
        padding: 0;
    }

    #carousel-multiple-items-ow .tns-controls [data-controls="prev"] {
        left: 5px;
    }

    #carousel-multiple-items-ow .tns-controls [data-controls="next"] {
        right: 5px;
    }

    #carousel-multiple-items-ow .tns-controls [aria-controls]:hover,
    #owl_app_screen-ow .tns-controls [aria-controls]:hover {
        background: transparent;
    }

    #owl_app_screen-ow .tns-controls [aria-controls] {
        top: -40px;
    }

    #owl_app_screen-ow .tns-controls [data-controls="next"] {
        right: 0%;
    }

    #owl_app_screen-ow .tns-controls [data-controls="prev"] {
        left: auto;
        right: 5%;
    }

    #owl_app_screen-ow .tns-controls [aria-controls] {
        width: auto;
        height: auto;
        border: none;
        padding: 0;
    }

    #owl_app_screen-ow .tns-controls [aria-controls]:hover svg path {
        fill: #5a92e9;
    }

    .tns-outer img {
        width: 100%;
    }

    .menu_results li.selected,
    .menu_results li:hover {
        color: #FFFFFF;
        background: #0A84FF;
        padding-left: 5px;

    }

    /******************star-point***************/
    .item_rate {
        margin-right: 0;
    }

    .item_rate::before {
        font-size: 15px;
        content: "★★★★★";
    }

    .item_rate {
        position: relative;
    }

    .item_rate>span::before {
        font-size: 15px;
        color: #fcd700;
        content: "★★★★★";
    }

    .item_rate>span {
        display: inline-block;
        position: absolute;
        /* top: -0.8px; */
        left: 0;
        width: 0;
        overflow: hidden;
    }

    div.apk_item_infor>div.item_rate_mb {
        display: block;
        position: absolute;
        bottom: 39px;
    }

    .item_rate_mb {
        display: none;
    }

    .item_rate {
        margin-right: 20px;
    }

    .obb_bundle {
        padding: 5px !important
    }

    /************************************modal*************************************/
    #baguetteBox-overlay .img_screen img {
        display: inline-block;
        width: auto;
        height: auto;
        max-height: 100%;
        max-width: 100%;
        vertical-align: middle;
        -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .6);
        -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .6);
        box-shadow: 0 0 8px rgba(0, 0, 0, .6);
    }

    #baguetteBox-overlay .img_screen {
        display: inline-block;
        position: relative;
        width: 100%;
        height: 100%;
        text-align: center;
    }

    .img_screen {
        display: inline-block;
        width: auto;
        height: auto;
        max-height: 100%;
        max-width: 100%;
        vertical-align: middle;
        -webkit-box-shadow: 0 0 0px rgba(0, 0, 0, .6);
        -moz-box-shadow: 0 0 0px rgba(0, 0, 0, .6);
        box-shadow: 0 0 0px rgba(0, 0, 0, .6);
        margin: auto;

    }

    /************************************end Modal*****************************************************/
    /************************************end Main****************************************************/
</style>
<style type="text/css">
    /***owl.css***/
    .ec {
        position: relative
    }

    .ec .ec__holder {
        overflow: hidden;
        width: 99%;
    }

    .ec .ec__track {
        will-change: transform
    }

    .ec .ec__track:after {
        content: '';
        display: table;
        clear: both
    }

    .ec .ec__item {
        float: left;
        overflow: hidden
    }

    .ec .ec__pagination {
        text-align: center;
        margin-top: 10px
    }

    .ec .ec__pagination .pagination__dot {
        width: 8px;
        height: 8px;
        background-color: transparent;
        display: inline-block;
        border: 1px solid #000;
        margin: 0 5px;
        border-radius: 50%;
        cursor: pointer
    }

    .ec .ec__pagination .pagination__dot--active {
        background-color: #000
    }

    .ec .ec__nav {
        position: absolute;
        bottom: 10px;
        border-top: 2px solid #000;
        border-right: 2px solid #000;
        width: 8px;
        height: 8px;
        cursor: pointer
    }

    .ec .ec__nav:before {
        content: ''
    }

    .ec .ec__nav--prev {
        transform: rotate(-135deg);
        left: 1em
    }

    .ec .ec__nav--next {
        transform: rotate(45deg);
        right: 1em
    }

    /***end-owl.css***/
</style>

<style type="text/css">
    /*********************tiny-slider.css********************/
    .tns-outer {
        position: relative;
        padding: 0 !important
    }

    .tns-outer [hidden] {
        display: none !important
    }

    .tns-outer [data-action] {
        display: none;
    }

    .tns-outer [aria-controls],
    .tns-outer [data-action] {
        cursor: pointer
    }

    .tns-slider {
        -webkit-transition: all 0s;
        -moz-transition: all 0s;
        transition: all 0s
    }

    .tns-slider>.tns-item {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box
    }

    .tns-horizontal.tns-subpixel {
        white-space: nowrap
    }

    .tns-horizontal.tns-subpixel>.tns-item {
        cursor: pointer;
        display: inline-block;
        vertical-align: top;
        white-space: normal
    }

    .tns-horizontal.tns-no-subpixel:after {
        content: '';
        display: table;
        clear: both
    }

    .tns-horizontal.tns-no-subpixel>.tns-item {
        float: left
    }

    .tns-horizontal.tns-carousel.tns-no-subpixel>.tns-item {
        margin-right: -100%
    }

    .tns-no-calc {
        position: relative;
        left: 0
    }

    .tns-gallery {
        position: relative;
        left: 0;
        min-height: 1px
    }

    .tns-gallery>.tns-item {
        position: absolute;
        left: -100%;
        -webkit-transition: transform 0s, opacity 0s;
        -moz-transition: transform 0s, opacity 0s;
        transition: transform 0s, opacity 0s
    }

    .tns-gallery>.tns-slide-active {
        position: relative;
        left: auto !important
    }

    .tns-gallery>.tns-moving {
        -webkit-transition: all 0.25s;
        -moz-transition: all 0.25s;
        transition: all 0.25s
    }

    .tns-autowidth {
        display: inline-block
    }

    .tns-lazy-img {
        -webkit-transition: opacity 0.6s;
        -moz-transition: opacity 0.6s;
        transition: opacity 0.6s;
        opacity: 0.6
    }

    .tns-lazy-img.tns-complete {
        opacity: 1
    }

    .tns-ah {
        -webkit-transition: height 0s;
        -moz-transition: height 0s;
        transition: height 0s
    }

    .tns-ovh {
        overflow: hidden
    }

    .tns-visually-hidden {
        position: absolute;
        left: -10000em
    }

    .tns-transparent {
        opacity: 0;
        visibility: hidden
    }

    .tns-fadeIn {
        opacity: 1;
        filter: alpha(opacity=100);
        z-index: 0
    }

    .tns-normal,
    .tns-fadeOut {
        opacity: 0;
        filter: alpha(opacity=0);
        z-index: -1
    }

    .tns-vpfix {
        white-space: nowrap
    }

    .tns-vpfix>div,
    .tns-vpfix>li {
        display: inline-block
    }

    .tns-t-subp2 {
        margin: 0 auto;
        width: 310px;
        position: relative;
        height: 10px;
        overflow: hidden
    }

    .tns-t-ct {
        width: 2333.3333333%;
        width: -webkit-calc(100% * 70 / 3);
        width: -moz-calc(100% * 70 / 3);
        width: calc(100% * 70 / 3);
        position: absolute;
        right: 0
    }

    .tns-t-ct:after {
        content: '';
        display: table;
        clear: both
    }

    .tns-t-ct>div {
        width: 1.4285714%;
        width: -webkit-calc(100% / 70);
        width: -moz-calc(100% / 70);
        width: calc(100% / 70);
        height: 10px;
        float: left
    }

    /*# sourceMappingURL=sourcemaps/tiny-slider.css.map */

    .autoWidth img {
        max-width: none;
    }

    .autoHeight img {
        width: 100%;
    }

    .customize-tools {
        position: relative;
    }

    .controls {
        text-align: center;
    }

    .controls li {
        display: block;
        position: absolute;
        top: 50%;
        height: 60px;
        line-height: 60px;
        margin-top: -30px;
        padding: 0 15px;
        cursor: pointer;
        transition: background 0.3s;
    }

    .controls li img {
        display: inline-block;
        vertical-align: middle;
    }

    .controls .prev {
        left: 0;
    }

    .controls .next {
        right: 0;
    }

    .controls li:hover {
        background: #f2f2f2;
    }

    .tns-controls {
        text-align: center;
        margin-bottom: 10px;

    }

    .tns-controls [aria-controls] {
        font-size: 15px;
        margin: 0 5px;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        padding: 0 1em;
        /* height: 2.5em; */
        color: #000;
        background: #fff;
        /* border-radius: 3px; */
        border: 0;
        top: 50%;
        display: flex;
        align-items: center;
        transform: translateY(-50%);
        border: 1px solid #5a92e9;
        position: absolute;
    }

    .tns-controls [aria-controls] svg path {
        fill: #5a92e9;
    }

    .tns-controls [aria-controls]:hover svg path {
        fill: #fff;
    }

    .tns-controls [aria-controls]:hover {
        background: #5a92e9;
    }

    .tns-controls [data-controls="prev"] {
        left: -5%;
    }

    .tns-controls [data-controls="next"] {
        right: -5%;
    }

    .lt-ie9 .tns-controls>[aria-controls] {
        line-height: 2.5em;
    }

    [data-action] {
        display: block;
        margin: 10px auto;
        font-size: 17px;
        min-width: 3em;
        text-align: center;
        background: transparent;
        border: 0;
    }

    .tns-controls [disabled] {
        color: #999999;
        background: #B3B3B3;
        cursor: not-allowed !important;
    }

    .tns-nav {
        text-align: center;
        margin: 10px 0;
    }

    .tns-nav>[aria-controls] {
        width: 10px;
        height: 10px;
        padding: 0;
        margin: 0 5px;
        border-radius: 50%;
        background: #c9c9c9;
        border: 2px solid #c9c9c9;
        /*border: 0;*/
    }

    .tns-nav>.tns-nav-active {
        background: #fff;
        border: 2px solid #4f87de;

    }

    .playbutton-wrapper {
        text-align: center;
    }

    .playbutton-wrapper button {
        height: 34px;
        padding: 0 10px;
        font-size: 14px;
        background: #fff;
        border-radius: 3px;
        border: 1px solid #ccc;
        cursor: pointer;
    }

    .wrapper {
        padding: 0 5em;
    }

    .suite-container .title,
    .suite-container .subtitle,
    .suite-container li {
        padding: 5px 10px;
    }

    .suite-container li {
        font-size: 12px;
        line-height: 1.4;
        background: #f5f5f5;
    }

    .suite-container .title {
        font-size: 14px;
        font-weight: bold;
        background: #8acac6;
        text-transform: capitalize;
    }

    .suite-container .subtitle {
        font-size: 13px;
        font-weight: bold;
        background: #dedede;
        text-transform: capitalize;
    }

    .suite-container .fail {
        background: #FFADE2;
    }

    [class*="vertical"] .img {
        padding: 30px 0;
    }

    [class*="vertical"] .img:before {
        content: '';
        display: inline-block;
        width: 1px;
        margin-right: -1px;
        height: 100%;
        vertical-align: middle;
    }

    [class*="vertical"] .img a {
        display: inline-block;
        padding: 0;
    }

    [class*="vertical"] .img-1 {
        height: 137px;
    }

    [class*="vertical"] .img-2 {
        height: 122px;
    }

    [class*="vertical"] .img-3 {
        height: 143px;
    }

    [class*="vertical"] .img-4 {
        height: 325px;
    }

    [class*="vertical"] .img-5 {
        height: 140px;
    }

    [class*="vertical"] .img-6 {
        height: 119px;
    }

    [class*="vertical"] .img-7 {
        height: 223px;
    }

    [class*="vertical"] .img-8 {
        height: 164px;
    }

    /*[class*="vertical"] p { display: none; }*/

    /*.animation .img-1 { height: 137px; }
.animation .img-2 { height: 122px; }
.animation .img-3 { height: 143px; }
.animation .img-4 { height: 325px; }
.animation .img-5 { height: 140px; }
.animation .img-6 { height: 119px; }
.animation .img-7 { height: 223px; }
.animation .img-8 { height: 164px; }*/

    /* reset prism */
    pre[class*="language-"] {
        margin: 0;
    }

    /* test results */
    .test-results {
        line-height: 1.4;
        padding: 20px 10%;
        background-color: #f2f2f2;
    }

    /* go to */
    .goto-controls {
        margin-bottom: 10px;
        text-align: center;
    }

    .goto-controls input,
    .goto-controls .button {
        display: inline-block;
        font-size: 14px;
        height: 2.6em;
        line-height: 2.6;
        padding: 0 1em;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        vertical-align: middle;
    }

    .goto-controls input {
        border: 1px solid #ccc;
    }

    .goto-controls .button {
        min-width: 4em;
        border: none;
        color: #fff;
        background-color: #333;
        cursor: pointer;
    }

    /* test results */
    .test-results {
        line-height: 1.6;
    }

    .test-results .title {
        font-weight: bold;
        text-transform: capitalize;
    }

    .test-results .title:nth-child(n+2) {
        margin-top: 1em;
    }

    .test-results [class*="item"] {
        margin-left: 1em;
        font-size: 0.9em;
    }

    .test-results [class*="item"] {
        font-weight: 300;
    }

    .test-results [class*="item"]:after {
        margin-left: 0.5em;
    }

    .test-results .item-success {
        color: #0EB80E;
    }

    .test-results .item-success:after {
        content: '✓';
    }

    .test-results .item-fail {
        color: #ED1E1E;
    }

    .test-results .item-fail:after {
        content: '✗';
    }

    .test-results .item-comment {
        font-style: italic;
        color: #666;
    }

    .test-results .item-notsure:after {
        content: '？';
    }

    .test-results .item-running:after {
        display: inline-block;
        height: 16px;
        content: url('../images/pinwheel.svg');
        vertical-align: middle;
    }

    .customize-tools [hidden] {
        display: none;
    }


    /* https://daneden.github.io/animate.css/ */
    @-webkit-keyframes jello {

        from,
        11.1%,
        to {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

        22.2% {
            -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);
            transform: skewX(-12.5deg) skewY(-12.5deg);
        }

        33.3% {
            -webkit-transform: skewX(6.25deg) skewY(6.25deg);
            transform: skewX(6.25deg) skewY(6.25deg);
        }

        44.4% {
            -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);
            transform: skewX(-3.125deg) skewY(-3.125deg);
        }

        55.5% {
            -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);
            transform: skewX(1.5625deg) skewY(1.5625deg);
        }

        66.6% {
            -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);
            transform: skewX(-0.78125deg) skewY(-0.78125deg);
        }

        77.7% {
            -webkit-transform: skewX(0.390625deg) skewY(0.390625deg);
            transform: skewX(0.390625deg) skewY(0.390625deg);
        }

        88.8% {
            -webkit-transform: skewX(-0.1953125deg) skewY(-0.1953125deg);
            transform: skewX(-0.1953125deg) skewY(-0.1953125deg);
        }
    }

    @keyframes jello {

        from,
        11.1%,
        to {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

        22.2% {
            -webkit-transform: skewX(-12.5deg) skewY(-12.5deg);
            transform: skewX(-12.5deg) skewY(-12.5deg);
        }

        33.3% {
            -webkit-transform: skewX(6.25deg) skewY(6.25deg);
            transform: skewX(6.25deg) skewY(6.25deg);
        }

        44.4% {
            -webkit-transform: skewX(-3.125deg) skewY(-3.125deg);
            transform: skewX(-3.125deg) skewY(-3.125deg);
        }

        55.5% {
            -webkit-transform: skewX(1.5625deg) skewY(1.5625deg);
            transform: skewX(1.5625deg) skewY(1.5625deg);
        }

        66.6% {
            -webkit-transform: skewX(-0.78125deg) skewY(-0.78125deg);
            transform: skewX(-0.78125deg) skewY(-0.78125deg);
        }

        77.7% {
            -webkit-transform: skewX(0.390625deg) skewY(0.390625deg);
            transform: skewX(0.390625deg) skewY(0.390625deg);
        }

        88.8% {
            -webkit-transform: skewX(-0.1953125deg) skewY(-0.1953125deg);
            transform: skewX(-0.1953125deg) skewY(-0.1953125deg);
        }
    }

    .jello {
        -webkit-animation-name: jello;
        animation-name: jello;
        -webkit-transform-origin: center;
        transform-origin: center;
    }

    @-webkit-keyframes rollOut {
        from {
            opacity: 1;
        }

        to {
            opacity: 0;
            -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);
            transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);
        }
    }

    @keyframes rollOut {
        from {
            opacity: 1;
        }

        to {
            opacity: 0;
            -webkit-transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);
            transform: translate3d(100%, 0, 0) rotate3d(0, 0, 1, 120deg);
        }
    }

    .rollOut {
        -webkit-animation-name: rollOut;
        animation-name: rollOut;
    }

    /**************************************/
    /* http://prismjs.com/download.html?themes=prism&languages=clike+javascript */
    /**
 * prism.js default theme for JavaScript, CSS and HTML
 * Based on dabblet (http://dabblet.com)
 * @author Lea Verou
 */

    code[class*="language-"],
    pre[class*="language-"] {
        color: black;
        background: none;
        text-shadow: 0 1px white;
        font-family: Consolas, Monaco, 'Andale Mono', 'Ubuntu Mono', monospace;
        text-align: left;
        white-space: pre;
        word-spacing: normal;
        word-break: normal;
        word-wrap: normal;
        line-height: 1.5;

        -moz-tab-size: 4;
        -o-tab-size: 4;
        tab-size: 4;

        -webkit-hyphens: none;
        -moz-hyphens: none;
        -ms-hyphens: none;
        hyphens: none;
    }

    pre[class*="language-"]::-moz-selection,
    pre[class*="language-"] ::-moz-selection,
    code[class*="language-"]::-moz-selection,
    code[class*="language-"] ::-moz-selection {
        text-shadow: none;
        background: #b3d4fc;
    }

    pre[class*="language-"]::selection,
    pre[class*="language-"] ::selection,
    code[class*="language-"]::selection,
    code[class*="language-"] ::selection {
        text-shadow: none;
        background: #b3d4fc;
    }

    @media print {

        code[class*="language-"],
        pre[class*="language-"] {
            text-shadow: none;
        }
    }

    /* Code blocks */
    pre[class*="language-"] {
        padding: 1em;
        margin: .5em 0;
        overflow: auto;
    }

    :not(pre)>code[class*="language-"],
    pre[class*="language-"] {
        background: #f5f2f0;
    }

    /* Inline code */
    :not(pre)>code[class*="language-"] {
        padding: .1em;
        border-radius: .3em;
        white-space: normal;
    }

    .token.comment,
    .token.prolog,
    .token.doctype,
    .token.cdata {
        color: slategray;
    }

    .token.punctuation {
        color: #999;
    }

    .namespace {
        opacity: .7;
    }

    .token.property,
    .token.tag,
    .token.boolean,
    .token.number,
    .token.constant,
    .token.symbol,
    .token.deleted {
        color: #905;
    }

    .token.selector,
    .token.attr-name,
    .token.string,
    .token.char,
    .token.builtin,
    .token.inserted {
        color: #690;
    }

    .token.operator,
    .token.entity,
    .token.url,
    .language-css .token.string,
    .style .token.string {
        color: #a67f59;
        background: hsla(0, 0%, 100%, .5);
    }

    .token.atrule,
    .token.attr-value,
    .token.keyword {
        color: #07a;
    }

    .token.function {
        color: #DD4A68;
    }

    .token.regex,
    .token.important,
    .token.variable {
        color: #e90;
    }

    .token.important,
    .token.bold {
        font-weight: bold;
    }

    .token.italic {
        font-style: italic;
    }

    .token.entity {
        cursor: help;
    }
</style>