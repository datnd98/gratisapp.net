<main class="page_detail">
    <section class="section_banner text-center mt-4 mb-4">
        <div class="container">
            <a href="#" title="" style="    display: revert;">
                <!--                img_desktop-->
                <img src="<?php echo base_url(); ?>assets/images/banner.png" alt="" class="hidden-sm hidden-xs">
                <!--                img_mobile-->
                <img src="<?php echo base_url(); ?>assets/images/Rectangle22.png" alt="" class="hidden-lg hidden-md">
            </a>
        </div>
    </section>
    <section class="section_content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url("category/" . $apk['category']); ?>"><?php echo $apk['category']; ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $apk['title']; ?></li>
                    </ul>
                    <div class="download_app">
                        <div class="images">
                            <img src="<?php echo $apk['cover']; ?>" alt="" title="">
                        </div>
                        <div class="txt_download">
                            <h1 class="title mt-0"><a href="<?php echo base_url($apk['urlTitle'] . '/' . $apk['appid']) ?>"><?php echo $apk['title']; ?></a></h1>
                            <div class="des_txt">
                                <p>By: <a href="<?php echo base_url("dev/" . urlencode($apk['dev_id'])); ?>"><?php echo $apk['offerby']; ?></a></p>
                                <p>Version: <span><?php echo $apk['version']; ?></span></p>
                                <p>Update: <span><?php echo date("d-m-Y", $apk['uploaddate']); ?></span></p>
                            </div>
                            <!--                             hiden mobile-->
                            <div class="rank_download hidden-sm hidden-xs">
                                <div class="item">
                                    <p><?php
                                        if ($apk['rate_total'] >= 1000000000) {
                                            $mol = $apk['rate_total'] % 1000000000;
                                            $rate = ($apk['rate_total'] - $mol) / 1000000000;
                                            echo $rate . "B+";
                                        } elseif ($apk['rate_total'] >= 1000000) {
                                            $mol = $apk['rate_total'] % 1000000;
                                            $rate = ($apk['rate_total'] - $mol) / 1000000;
                                            echo $rate . "M+";
                                        } elseif ($apk['rate_total'] >= 1000) {
                                            $mol = $apk['rate_total'] % 1000;
                                            $rate = ($apk['rate_total'] - $mol) / 1000;
                                            echo $rate . "K+";
                                        } else {
                                            echo $apk['rate_total'];
                                        }
                                        ?> Reviews</p>
                                    <div class="view_rank">
                                        <div class="count_rank"><?php echo round($apk['score'] * 2, 1); ?></div>
                                        <div class="rankting">
                                            <div class="item_rate flex">
                                                <span class="" style="width: <?php echo round($apk['score'] / 5 * 100, 2) ?>%;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <p>Downloads</p>
                                    <div class="view_rank">
                                        <div class="count_rank"><?php
                                                                if ($apk['installs'] >= 1000000000) {
                                                                    $mol = $apk['installs'] % 1000000000;
                                                                    $rate = ($apk['installs'] - $mol) / 1000000000;
                                                                    echo $rate . "B+";
                                                                } elseif ($apk['installs'] >= 1000000) {
                                                                    $mol = $apk['installs'] % 1000000;
                                                                    $rate = ($apk['installs'] - $mol) / 1000000;
                                                                    echo $rate . "M+";
                                                                } elseif ($apk['installs'] >= 1000) {
                                                                    $mol = $apk['installs'] % 1000;
                                                                    $rate = ($apk['installs'] - $mol) / 1000;
                                                                    echo $rate . "K+";
                                                                } else {
                                                                    echo $apk['installs'];
                                                                }
                                                                ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box_download">
                            <?php
                            if ($files[0]['testing'] != 2) {
                            ?>
                                <a href="<?php echo base_url("/download/" . $link_download['apk']); ?>" class="btn_download mb-1">
                                    <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 8.74228e-08L6 12.17L2.41 8.59L1 10L7 16L13 10L11.59 8.59L8 12.17L8 0L6 8.74228e-08Z" fill="white" />
                                        <path d="M0 18H14V20H0V18Z" fill="white" />
                                    </svg>
                                    Download APK (<?php if ($files[0]['size'] >= 1073741824) {
                                                        $bytes = number_format(($files[0]['size'] / 1073741824), 1) . ' GB';
                                                    } elseif ($files[0]['size'] >= 1048576) {
                                                        $bytes = number_format(($files[0]['size'] / 1048576), 1) . ' MB';
                                                    } elseif ($files[0]['size'] >= 1024) {
                                                        $bytes = number_format(($files[0]['size'] / 1024), 1) . ' KB';
                                                    } elseif ($files[0]['size'] > 1) {
                                                        $bytes = $files[0]['size'] . ' bytes';
                                                    } elseif ($files[0]['size'] == 1) {
                                                        $bytes = $files[0]['size'] . ' byte';
                                                    } else {
                                                        $bytes = '';
                                                    }

                                                    echo $bytes; ?>)</a>
                            <?php } else { ?>
                                <a href="<?php echo base_url("/download/" . $link_download['bundle']); ?>" class="btn_download mb-1 obb_bundle">
                                    <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 8.74228e-08L6 12.17L2.41 8.59L1 10L7 16L13 10L11.59 8.59L8 12.17L8 0L6 8.74228e-08Z" fill="white" />
                                        <path d="M0 18H14V20H0V18Z" fill="white" />
                                    </svg>
                                    Download APK + BUNDLE (<?php if ($files[0]['size_total'] >= 1073741824) {
                                                                $bytes = number_format(($files[0]['size_total'] / 1073741824), 1) . ' GB';
                                                            } elseif ($files[0]['size_total'] >= 1048576) {
                                                                $bytes = number_format(($files[0]['size_total'] / 1048576), 1) . ' MB';
                                                            } elseif ($files[0]['size_total'] >= 1024) {
                                                                $bytes = number_format(($files[0]['size_total'] / 1024), 1) . ' KB';
                                                            } elseif ($files[0]['size_total'] > 1) {
                                                                $bytes = $files[0]['size_total'] . ' bytes';
                                                            } elseif ($files[0]['size_total'] == 1) {
                                                                $bytes = $files[0]['size_total'] . ' byte';
                                                            } else {
                                                                $bytes = '';
                                                            }

                                                            echo $bytes; ?>)</a>
                            <?php }
                            if ($file[0]['size_obb'] > 0) { ?>
                                <a href="<?php echo base_url("download/" . $files[0]['appid'] . "/" . $files[0]['version'] . "/" . $files[0]['versioncode']); ?>" class="btn_download mb-1 obb_bundle">
                                    <svg width="14" height="20" viewBox="0 0 14 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 8.74228e-08L6 12.17L2.41 8.59L1 10L7 16L13 10L11.59 8.59L8 12.17L8 0L6 8.74228e-08Z" fill="white" />
                                        <path d="M0 18H14V20H0V18Z" fill="white" />
                                    </svg>
                                    Download OBB (<?php if ($files[0]['obb_size'] >= 1073741824) {
                                                            $bytes = number_format(($files[0]['obb_size'] / 1073741824), 1) . ' GB';
                                                        } elseif ($files[0]['obb_size'] >= 1048576) {
                                                            $bytes = number_format(($files[0]['obb_size'] / 1048576), 1) . ' MB';
                                                        } elseif ($files[0]['obb_size'] >= 1024) {
                                                            $bytes = number_format(($files[0]['obb_size'] / 1024), 1) . ' KB';
                                                        } elseif ($files[0]['obb_size'] > 1) {
                                                            $bytes = $files[0]['obb_size'] . ' bytes';
                                                        } elseif ($files[0]['obb_size'] == 1) {
                                                            $bytes = $files[0]['obb_size'] . ' byte';
                                                        } else {
                                                            $bytes = '';
                                                        }

                                                        echo $bytes; ?>)</a>
                            <?php } ?>
                            <a href="" class="btn_outline">
                                <svg width="10" height="18" viewBox="0 0 10 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M3.33333 14.8333H5.83333V17.3333H3.33333V14.8333ZM5 0.666626C9.45833 0.849959 11.4 5.34996 8.75 8.72496C8.05833 9.55829 6.94167 10.1083 6.39167 10.8083C5.83333 11.5 5.83333 12.3333 5.83333 13.1666H3.33333C3.33333 11.775 3.33333 10.6 3.89167 9.76663C4.44167 8.93329 5.55833 8.44163 6.25 7.89163C8.26667 6.02496 7.76667 3.38329 5 3.16663C4.33696 3.16663 3.70107 3.43002 3.23223 3.89886C2.76339 4.3677 2.5 5.00358 2.5 5.66663H0C0 4.34054 0.526784 3.06877 1.46447 2.13109C2.40215 1.19341 3.67392 0.666626 5 0.666626Z" fill="#4C40F7" />
                                </svg>
                                How to Install</a>
                            <!--                             show_mobile-->
                            <div class="rank_download hidden-lg hidden-md">
                                <div class="item">
                                    <p><?php
                                        if ($apk['rate_total'] >= 1000000000) {
                                            $mol = $apk['rate_total'] % 1000000000;
                                            $rate = ($apk['rate_total'] - $mol) / 1000000000;
                                            echo $rate . "B+";
                                        } elseif ($apk['rate_total'] >= 1000000) {
                                            $mol = $apk['rate_total'] % 1000000;
                                            $rate = ($apk['rate_total'] - $mol) / 1000000;
                                            echo $rate . "M+";
                                        } elseif ($apk['rate_total'] >= 1000) {
                                            $mol = $apk['rate_total'] % 1000;
                                            $rate = ($apk['rate_total'] - $mol) / 1000;
                                            echo $rate . "K+";
                                        } else {
                                            echo $apk['rate_total'];
                                        }
                                        ?> Reviews</p>
                                    <div class="view_rank">
                                        <div class="count_rank"><?php echo round($apk['score'] * 2, 1); ?></div>
                                        <div class="rankting">
                                            <div class="item_rate flex">
                                                <span class="" style="width: <?php echo round($apk['score'] / 5 * 100, 2) ?>%;"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <p>Downloads</p>
                                    <div class="view_rank">
                                        <div class="count_rank"><?php
                                                                if ($apk['installs'] >= 1000000000) {
                                                                    $mol = $apk['installs'] % 1000000000;
                                                                    $rate = ($apk['installs'] - $mol) / 1000000000;
                                                                    echo $rate . "B+";
                                                                } elseif ($apk['installs'] >= 1000000) {
                                                                    $mol = $apk['installs'] % 1000000;
                                                                    $rate = ($apk['installs'] - $mol) / 1000000;
                                                                    echo $rate . "M+";
                                                                } elseif ($apk['installs'] >= 1000) {
                                                                    $mol = $apk['installs'] % 1000;
                                                                    $rate = ($apk['installs'] - $mol) / 1000;
                                                                    echo $rate . "K+";
                                                                } else {
                                                                    echo $apk['installs'];
                                                                }
                                                                ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="box_share_social">
                                <span>Share on:</span>
                                <ul class="list_social text-center ">
                                    <li><a href="" title="Pinterest"><img src="<?php echo base_url(); ?>assets/images/Pinterest_smail.svg" alt=""></a></li>
                                    <li><a href="" title="Facebook"><img src="<?php echo base_url(); ?>assets/images/Facebook_smail.png" alt=""></a>
                                    </li>
                                    <li><a href="" title="Twitter"><img src="<?php echo base_url(); ?>assets/images/Twitter_smail.svg" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <?php if (count($list_version[$files[0]['version']]) > 1) { ?>
                        <div class="info_download mt-5 full_mobile">
                            <div class="title_head">
                                <div class="count">
                                    option <br><strong>1</strong>
                                </div>
                                <div class="txt">Download the APK according to your Device</div>
                            </div>
                            <div class="form_layout ">
                                <div class="form-group">
                                    <div class="row " style="display: flex;align-items: flex-end;flex-wrap: wrap;">
                                        <div class="col-md-6 mb-2">
                                            <label for="">Select OS</label>
                                            <div class="aselect">
                                                <select name="android" id="android_sel" class="form-control">
                                                    <option value="0">Select android version</option>
                                                    <option value="29">Android 10</option>
                                                    <option value="28">Android 9.0</option>
                                                    <option value="27">Android 8.1</option>
                                                    <option value="26">Android 8.0</option>
                                                    <option value="25">Android 7.1</option>
                                                    <option value="24">Android 7.0</option>
                                                    <option value="23">Android 6.0</option>
                                                    <option value="22">Android 5.1</option>
                                                    <option value="21">Android 5.0</option>
                                                    <option value="19">Android 4.4.x</option>
                                                    <option value="18">Android 4.3</option>
                                                    <option value="17">Android 4.2</option>
                                                    <option value="16">Android 4.1</option>
                                                    <option value="15">Android 4.0.3 - 4.0.4</option>
                                                    <option value="14">Android 4.0.1 - 4.0.2</option>
                                                    <option value="13">Lower</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <label for="">Select Brand</label>
                                            <div id="selectBrand" class="aselect">
                                                <input type="button" id="brandSelect" class="form-control" onclick="showbrand()" value="Default" style="text-align: left;">
                                                <div id="brand_sel" class="input_search" status="0">
                                                    <input type="text" id="brandInput" class="form-control" placeholder="Type your phone's brand" onkeyup="selectbrand()">
                                                    <ul id="brand_list" class="menu_results"></ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <label for="">Select Device Model</label>
                                            <div id="selectModel" class="aselect">
                                                <input type="button" id="modelSelect" class="form-control" onclick="showmodel()" value="Default" style="text-align: left;">
                                                <div id="model_sel" class="input_search" status="0">
                                                    <input type="text" id="modelInput" class="form-control" placeholder="Type your phone's model" onkeyup="selectmodel()">
                                                    <ul id="model_list" class="menu_results"></ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2">
                                            <button class="btn_download" onclick="general_link('<?php echo $apk['appid'] . "','" . $apk['urlTitle']; ?>')">GENERATE LINK</button>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="">Get your GSF ID (Android ID)</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="getLink" style="display: none;">
                                <div class="title_head">
                                    <div class="count">
                                        <strong></strong>
                                    </div>
                                    <div class="txt">Your download link</div>
                                </div>
                                <div class="form_layout pd-0">
                                    <table>
                                        <thead>
                                            <th>Variant</th>
                                            <th>Architecture</th>
                                            <th>Minimum Version</th>
                                            <th>Screen DPI</th>
                                            <th class="hidden-xs hidden-sm"></th>
                                        </thead>
                                        <tbody id="variant_list">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="info_download mt-4 full_mobile">
                            <div class="title_head">
                                <div class="count">
                                    option <br><strong>2</strong>
                                </div>
                                <div class="txt">Download the APK according to your Device</div>
                            </div>
                            <div class="form_layout pd-0">
                                <table>
                                    <thead>
                                        <th>Variant</th>
                                        <th>Architecture</th>
                                        <th>Minimum Version</th>
                                        <th>Screen DPI</th>
                                        <th class="hidden-xs hidden-sm"></th>
                                    </thead>
                                    <tbody id="">
                                        <!-- group-->
                                        <?php
                                        foreach ($list_version[$version[0]] as $key => $variant) {
                                        ?>
                                            <tr>
                                                <td>
                                                    <p class="var_num"><?php echo $variant['versioncode']; ?></p>
                                                    <p><span><?php echo date("Y-m-d", strtotime($variant['uploaddate'])); ?></span></p>
                                                </td>
                                                <td><?php
                                                    if (empty($variant['architecture'])) {
                                                        echo 'NO ARCHITECTURE';
                                                    } elseif ($variant['architecture'] != '-1') {
                                                        echo $variant['architecture'];
                                                    } else {
                                                        echo 'NO ARCHITECTURE';
                                                    }
                                                    ?></td>
                                                <td><?php
                                                    switch ($variant['requiresandroid']) {
                                                        case 6:
                                                            $rq = '2.0.1';
                                                            break;
                                                        case 14:
                                                            $rq = '4.0.1 - 4.0.2';
                                                            break;
                                                        case 15:
                                                            $rq = '4.0.3 - 4.0.4';
                                                            break;
                                                        case 16:
                                                            $rq = '4.1';
                                                            break;
                                                        case 17:
                                                            $rq = '4.2';
                                                            break;
                                                        case 18:
                                                            $rq = '4.3';
                                                            break;
                                                        case 19:
                                                            $rq = '4.4';
                                                            break;
                                                        case 21:
                                                            $rq = '5.0';
                                                            break;
                                                        case 22:
                                                            $rq = '5.1';
                                                            break;
                                                        case 23:
                                                            $rq = '6.0';
                                                            break;
                                                        case 24:
                                                            $rq = '7.0';
                                                            break;
                                                        case 25:
                                                            $rq = '7.1';
                                                            break;
                                                        case 26:
                                                            $rq = '8.0.0';
                                                            break;
                                                        case 27:
                                                            $rq = '8.1.0';
                                                            break;
                                                        case 28:
                                                            $rq = '9';
                                                            break;
                                                        case 29:
                                                            $rq = '10';
                                                            break;
                                                        default:
                                                            // code...
                                                            break;
                                                    }
                                                    echo 'Android ' . $rq . '+';
                                                    ?></td>
                                                <td><?php
                                                    $variant_dpi = json_decode($variant['dpi'], true);
                                                    if ($variant_dpi['dpi_min'] == -1 && $variant_dpi['dpi_max'] == -1) {
                                                        $dpi = 'NO DPI';
                                                    } elseif ($variant_dpi['dpi_min'] == $variant_dpi['dpi_max']) {
                                                        $dpi = $variant_dpi['dpi_min'] . ' DPI *';
                                                    } else {
                                                        $dpi = $variant_dpi['dpi_min'] . '-' . $variant_dpi['dpi_max'] . ' DPI *';
                                                    }
                                                    echo $dpi;
                                                    ?></td>
                                                <td class="hidden-sm hidden-xs">
                                                    <a href="<?php echo base_url() . "download/" . $link_download['variant'][$key]; ?>" class="btn_down">
                                                        <svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M5.25 0.5L5.25 9.6275L2.5575 6.9425L1.5 8L6 12.5L10.5 8L9.4425 6.9425L6.75 9.6275L6.75 0.5L5.25 0.5Z" fill="white" />
                                                            <path d="M0.75 14H11.25V15.5H0.75V14Z" fill="white" />
                                                        </svg>
                                                        Download
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr class="hidden-md hidden-lg">
                                                <td colspan="4">
                                                    <a href="<?php echo base_url() . "download/" . $link_download['variant'][$key]; ?>" class="btn_down">
                                                        <svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M5.25 0.5L5.25 9.6275L2.5575 6.9425L1.5 8L6 12.5L10.5 8L9.4425 6.9425L6.75 9.6275L6.75 0.5L5.25 0.5Z" fill="white" />
                                                            <path d="M0.75 14H11.25V15.5H0.75V14Z" fill="white" />
                                                        </svg>
                                                        Download
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <!--end group-->
                                        <!-- group-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="box_app_owl mt-4 ">
                        <div class="title-head mb-3">
                            <h2 class="mg-0 title">Screen Shots</h2>
                        </div>
                        <div class="row list_app ">
                            <div class="col-md-12">
                                <div class="owl_app_screen" id="owl_app_screen">
                                    <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                                        <div class="item">
                                            <img src="<?php echo $images[$i][2]; ?>" alt="" onclick="openModal();currentSlide(<?php echo ($i + 1); ?>)">
                                        </div>
                                    <?php } ?>
                                </div>
                                <div id="myModal" class="modal">
                                    <div class="modal-content" id="modal-content">
                                        <span class="close cursor" onclick="closeModal()">
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0.984405 13.6665C0.855625 13.6665 0.729732 13.6284 0.622648 13.5568C0.515564 13.4853 0.432101 13.3836 0.382816 13.2646C0.333531 13.1457 0.320638 13.0148 0.345768 12.8884C0.370899 12.7621 0.432923 12.6461 0.523996 12.5551L12.5551 0.524021C12.6772 0.401913 12.8428 0.333313 13.0155 0.333313C13.1881 0.333313 13.3538 0.401913 13.4759 0.524021C13.598 0.646129 13.6666 0.811743 13.6666 0.98443C13.6666 1.15712 13.598 1.32273 13.4759 1.44484L1.44481 13.4759C1.38441 13.5364 1.31264 13.5844 1.23363 13.6171C1.15462 13.6499 1.06992 13.6666 0.984405 13.6665Z" fill="white" />
                                                <path d="M13.0154 13.6665C12.9299 13.6666 12.8452 13.6499 12.7662 13.6171C12.6872 13.5844 12.6154 13.5364 12.555 13.4759L0.52396 1.44484C0.401852 1.32273 0.333252 1.15712 0.333252 0.98443C0.333252 0.811743 0.401852 0.646129 0.52396 0.524021C0.646068 0.401913 0.811682 0.333313 0.984369 0.333313C1.15706 0.333313 1.32267 0.401913 1.44478 0.524021L13.4758 12.5551C13.5669 12.6461 13.6289 12.7621 13.6541 12.8884C13.6792 13.0148 13.6663 13.1457 13.617 13.2646C13.5677 13.3836 13.4843 13.4853 13.3772 13.5568C13.2701 13.6284 13.1442 13.6665 13.0154 13.6665Z" fill="white" />
                                            </svg>
                                        </span>
                                        <?php for ($i = 0; $i < sizeof($images); $i++) { ?>
                                            <div class="mySlides">
                                                <!-- <div class="numbertext">1 / 4</div> -->
                                                <div class="img_screen">
                                                    <img src="<?php echo $images[$i][2]; ?>" style="width:100%">
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <a class="prev" onclick="plusSlides(-1)" id="prev">
                                            <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0.25 7.99997C0.25 8.17434 0.311093 8.34889 0.433124 8.48201L6.68309 15.3002C6.92731 15.5666 7.32278 15.5666 7.56684 15.3002C7.8109 15.0338 7.81105 14.6023 7.56684 14.3361L1.75874 7.99997L7.56684 1.66384C7.81105 1.39742 7.81105 0.966001 7.56684 0.699751C7.32262 0.433501 6.92715 0.433331 6.68309 0.699751L0.433124 7.51792C0.311093 7.65105 0.25 7.82559 0.25 7.99997Z" fill="white" />
                                            </svg>


                                        </a>
                                        <a class="next" onclick="plusSlides(1)" id="next"> <svg width="8" height="16" viewBox="0 0 8 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M7.75 7.99997C7.75 8.17434 7.68891 8.34889 7.56688 8.48201L1.31691 15.3002C1.07269 15.5666 0.677224 15.5666 0.433163 15.3002C0.189102 15.0338 0.188946 14.6023 0.433163 14.3361L6.24126 7.99997L0.433163 1.66384C0.188945 1.39742 0.188945 0.966001 0.433162 0.699751C0.67738 0.433501 1.07285 0.433331 1.31691 0.699751L7.56688 7.51792C7.68891 7.65105 7.75 7.82559 7.75 7.99997Z" fill="white" />
                                            </svg></a>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box_app_speecs mt-4">
                        <div class="title-head mb-2">
                            <h2 class="mg-0 title">App Specs</h2>
                        </div>
                        <ul>
                            <li>
                                <strong>Package:</strong>
                                <span><a href=""><?php echo $apk['appid']; ?></a></span>
                            </li>
                            <li>
                                <strong>Version:</strong>
                                <span><?php echo $apk['version'] . " (" . $apk['versioncode'] . ")"; ?></span>
                            </li>
                            <li>
                                <strong>Size:</strong>
                                <span><?php if ($apk['size'] >= 1073741824) {
                                            $bytes = number_format(($apk['size'] / 1073741824), 1) . ' GB';
                                        } elseif ($apk['size'] >= 1048576) {
                                            $bytes = number_format(($apk['size'] / 1048576), 1) . ' MB';
                                        } elseif ($apk['size'] >= 1024) {
                                            $bytes = number_format(($apk['size'] / 1024), 1) . ' KB';
                                        } elseif ($apk['size'] > 1) {
                                            $bytes = $apk['size'] . ' bytes';
                                        } elseif ($apk['size'] == 1) {
                                            $bytes = $apk['size'] . ' byte';
                                        } else {
                                            $bytes = '';
                                        }

                                        echo $bytes; ?></span>
                            </li>
                            <li>
                                <strong>Update on:</strong>
                                <span><?php echo date("F d, Y", $apk['uploaddate']); ?></span>
                            </li>
                            <li>
                                <strong>Installs:</strong>
                                <span><?php
                                        if ($apk['installs'] >= 1000000000) {
                                            $mol = $apk['installs'] % 1000000000;
                                            $rate = ($apk['installs'] - $mol) / 1000000000;
                                            echo $rate . "B+";
                                        } elseif ($apk['installs'] >= 1000000) {
                                            $mol = $apk['installs'] % 1000000;
                                            $rate = ($apk['installs'] - $mol) / 1000000;
                                            echo $rate . "M+";
                                        } elseif ($apk['installs'] >= 1000) {
                                            $mol = $apk['installs'] % 1000;
                                            $rate = ($apk['installs'] - $mol) / 1000;
                                            echo $rate . "K+";
                                        } else {
                                            echo $apk['installs'];
                                        }
                                        ?></span>
                            </li>
                            <li>
                                <strong>Signature:</strong>
                                <span><?php echo $files[0]['signature'] ?></span>
                            </li>
                            <li>
                                <strong>APK File SHA1:</strong>
                                <span><?php echo $files[0]['sha1'] ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="box_description mt-4">
                        <div class="title-head mb-3">
                            <h2 class="mg-0 title">Description</h2>
                        </div>
                        <p id="more">
                            <?php echo $apk['description']; ?>
                        </p>
                        <div class="see-more text-center" id="see" style="display: block;color: #4C40F7;cursor: pointer">
                            <span class="btn_readmore " id="seemore" onclick="moreOrLess()">Read more</span>
                        </div>
                        <script>
                            //show more
                            var see = document.getElementById("see");
                            var content = document.getElementById("more").innerHTML;
                            var more = document.getElementById("more");
                            var seeMore = document.getElementById("seemore");
                            var ellipsestext = "...";
                            var showChar = 500;

                            if (content.length > showChar) {
                                var hide = true;
                                see.style.display = "block";

                                var c = content.substr(0, showChar);
                                var h = content.substr(showChar, content.length - showChar);
                                more.innerHTML = c + ellipsestext;

                                function moreOrLess() {
                                    if (hide) {
                                        more.innerHTML = c + h;
                                        hide = false;
                                        seeMore.innerHTML = "Read less";
                                    } else {
                                        more.innerHTML = c + ellipsestext;
                                        hide = true;
                                        seeMore.innerHTML = "Read more";
                                    }
                                }

                            }
                        </script>
                    </div>
                    <div class="box_more_app mt-5 hidden-sm hidden-xs">
                        <div class="title-head mb-3">
                            <h2 class="mg-0 title">More from Developer</h2>
                        </div>
                        <div class="row list_app ">
                            <div class="col-md-12">
                                <div class="owl-carousel owl-theme list_more_dev ">
                                    <?php for ($j = 0; $j < sizeof($devApps); $j++) : ?>
                                        <div class="item_box_app">
                                            <div class="images_app">
                                                <a href="<?php echo base_url($devApps[$j]['urlTitle'] . '/' . $devApps[$j]['appid']) ?>">
                                                    <img src="<?php echo $devApps[$j]['cover'] ?>" alt="">
                                                </a>
                                            </div>
                                            <div class="content_right">
                                                <h3 class="title mg-0"><a href="<?php echo base_url($devApps[$j]['urlTitle'] . '/' . $devApps[$j]['appid']) ?>"><?php echo $devApps[$j]['title'] ?></a></h3>
                                                <div class="rankting">
                                                    <div class="item_rate flex">
                                                        <span class="" style="width: <?php echo round($devApps[$j]['score'] / 5 * 100, 2) ?>%;"></span>
                                                    </div>
                                                </div>
                                                <span class="date"><?php
                                                                    $today = strtotime(date('Y-m-d'));
                                                                    $time = floor(($today - $devApps[$j]['uploaddate']) / 86400);
                                                                    if ($time > 365) {
                                                                        if (floor($time / 365) > 1) {
                                                                            echo floor($time / 365) . " years ago";
                                                                        } else {
                                                                            echo floor($time / 365) . " year ago";
                                                                        }
                                                                    } elseif ($time > 31) {
                                                                        if (floor($time / 31) > 1) {
                                                                            echo floor($time / 31) . " months ago";
                                                                        } else {
                                                                            echo floor($time / 31) . " month ago";
                                                                        }
                                                                    } elseif ($time > 1) {
                                                                        echo $time . " days ago";
                                                                    } elseif ($time == 1) {
                                                                        echo $time . " day ago";
                                                                    } else {
                                                                        echo floor(($today - $devApps[$j]['uploaddate']) / 3600) . " hours ago";
                                                                    }
                                                                    ?></span>
                                            </div>
                                        </div>
                                    <?php endfor; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="img_sile_bar text-center hidden-xs hidden-sm">
                        <a href="#" title="">
                            <picture><img src="<?php echo base_url(); ?>assets/images/img_ads.png" alt=""></picture>
                        </a>
                    </div>
                    <div class="box_download_app mt-4 mb-4 hidden-xs hidden-sm">
                        <div class="imgage"><a href=""><img src="<?php echo base_url(); ?>assets/images/Logo_res.png" alt=""></a></div>
                        <div class="box_content ">
                            <h3 class="title mt-0"><a href="">Install Choilieng App</a></h3>
                            <p>For your faster download experience</p>
                            <a href="" class="btn_defaulf mt-1">Download</a>
                        </div>
                    </div>
                    <div class="box_related_app mt-4 mb-4">
                        <div class="title-head mb-3">
                            <h2 class="mg-0 title">Related Apps</h2>
                        </div>
                        <div class="row list_app owl_related " id="owl_related">
                            <?php for ($j = 0; $j < sizeof($relate); $j++) : ?>
                                <div class="col-md-12 ">
                                    <div class="item_box_app">
                                        <div class="images_app">
                                            <a href="<?php echo base_url($relate[$j]['urlTitle'] . '/' . $relate[$j]['appid']); ?>">
                                                <img src="<?php echo $relate[$j]['cover']; ?>" alt="">
                                            </a>
                                        </div>
                                        <div class="content_right">
                                            <h3 class="title mg-0"><a href="<?php echo base_url($relate[$j]['urlTitle'] . '/' . $relate[$j]['appid']) ?>"><?php echo $relate[$j]['title']; ?></a></h3>
                                            <div class="rankting">
                                                <div class="item_rate flex">
                                                    <span class="" style="width: <?php echo round($relate[$j]['score'] / 5 * 100, 2) ?>%;"></span>
                                                </div>
                                            </div>
                                            <span class="date"><?php
                                                                $today = strtotime(date('Y-m-d'));
                                                                $time = floor(($today - $relate[$j]['uploaddate']) / 86400);
                                                                if ($time > 365) {
                                                                    if (floor($time / 365) > 1) {
                                                                        echo floor($time / 365) . " years ago";
                                                                    } else {
                                                                        echo floor($time / 365) . " year ago";
                                                                    }
                                                                } elseif ($time > 31) {
                                                                    if (floor($time / 31) > 1) {
                                                                        echo floor($time / 31) . " months ago";
                                                                    } else {
                                                                        echo floor($time / 31) . " month ago";
                                                                    }
                                                                } elseif ($time > 1) {
                                                                    echo $time . " days ago";
                                                                } elseif ($time == 1) {
                                                                    echo $time . " day ago";
                                                                } else {
                                                                    echo floor(($today - $relate[$j]['uploaddate']) / 3600) . " hours ago";
                                                                }
                                                                ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <div id="see1">
                            <span class="btn_show_more hidden-xs hidden-sm" id="show_more" onclick="moreOrLess()">Show more</span>
                        </div>
                        <script>
                            if (screen.width > 991) {
                                //show more
                                var see1 = document.getElementById("see1");
                                var content1 = document.getElementById("owl_related").innerHTML;
                                var more1 = document.getElementById("owl_related");
                                var seeMore1 = document.getElementById("show_more");
                                var ellipsestext1 = "";
                                var showChar1 = 4100;
                                // console.log(content1.length)
                                if (content1.length > showChar1) {
                                    var hide1 = true;
                                    see1.style.display = "block";

                                    var a = content1.substr(0, showChar1);
                                    var b = content1.substr(showChar1, content1.length - showChar1);
                                    more1.innerHTML = a + ellipsestext1;

                                    function moreOrLess() {
                                        if (hide1) {
                                            more1.innerHTML = a + b;
                                            hide1 = false;
                                            seeMore1.innerHTML = "Show less";
                                        } else {
                                            more1.innerHTML = a + ellipsestext1;
                                            hide1 = true;
                                            seeMore1.innerHTML = "Show more";
                                        }
                                    }

                                }
                            }
                        </script>
                    </div>
                    <div class="box_tag mb-4">
                        <h3 class="title_default mt-0">Hot Tags</h3>
                        <div class="list_tag">
                            <a href="" title="">Among Us</a>
                            <a href="" title="">Tiktok</a>
                            <a href="" title="">Facebook</a>
                            <a href="" title="">GAM</a>
                            <a href="" title="">LOL</a>
                            <a href="" title="">PUBG</a>
                            <a href="" title="">Pinterest</a>
                            <a href="" title="">PUBG</a>
                            <a href="" title="">Pinterest</a>
                            <a href="" title="">Insta</a>
                            <a href="" title="">LOL</a>

                        </div>
                    </div>
                    <div class="box_social">
                        <p>Follow :</p>
                        <ul class="list_social text-center mt-2">
                            <li><a href="" title="Twitter"><img src="<?php echo base_url(); ?>assets/images/Twitter.svg" alt=""></a></li>
                            <li><a href="" title="Facebook"><img src="<?php echo base_url(); ?>assets/images/Path.svg" alt=""></a></li>
                            <li><a href="" title="Youtube"><img src="<?php echo base_url(); ?>assets/images/Youtube.svg" alt=""></a></li>
                        </ul>
                    </div>
                    <!--banner_mobile-->
                    <div class="mt-4">
                        <a href="" class="hidden-lg hidden-md "><img src="<?php echo base_url(); ?>assets/images/banner_mobile_bt.png" alt="" style="width: 100%"></a>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>
<script src="<?php echo base_url(); ?>assets/js/select.js"></script>
<script>
    //modal
    function openModal() {
        document.getElementById("myModal").style.display = "flex";
    }

    function closeModal() {
        document.getElementById("myModal").style.display = "none";
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        // dots[slideIndex-1].className += " active";
        // captionText.innerHTML = dots[slideIndex-1].alt;
    }
    //close modal
    window.addEventListener('click', function(e) {
        var div = document.getElementById('myModal')
        if (document.getElementById('modal-content').contains(e.target) || document.getElementById('owl_app_screen').contains(e.target)) {

        } else {
            document.getElementById("myModal").style.display = "none";
        }
    });


    document.addEventListener('keydown', function(event) {
        if (event.which === 37) {
            chose5 = document.getElementById('prev').click();
        } else if (event.which === 39) {
            chose15 = document.getElementById('prev').click();
        }
    }, false);
</script>