<?php
/*
Class thực hiện việc thao tác với elasticsearch
Sử dụng thư viện elasticsearch php 
https://www.elastic.co/guide/en/elasticsearch/client/php-api/current/index.html
*/
defined('BASEPATH') or exit('No direct script access allowed');
require './vendor/autoload.php';
class Api
{
	protected $client;
	protected $name_index;
	protected $api_img;
	public function __construct()
	{

		$this->client = \Elasticsearch\ClientBuilder::create()->setSSLVerification(false)->setHosts(["46.4.88.151:9200"])->build();
		$this->name_index = 'apktrending_new';
	}
	// chi tiết app theo appid
	public function getDetailApp($id)
	{
		$params['index'] = 'apktrending_new';
		$params['type'] = 'apktrending_new';
		$params['id'] = $id;
		$params['client'] = ['ignore' => 404];
		$app = $this->client->get($params);
		// print_r($app);die;
		if ($app['found'] == true) {
			// $tmp = strtolower($app['_source']['category']);
			// $tmp = str_replace(' & ', '_and_', $tmp);
			// $tmp = str_replace(' ', '_', $tmp);
			// $app['_source']['category_url'] = $tmp;
			$app['_source']['urlTitle'] = $app['_source']['urltitle'];
			$result = array('data' => $app['_source'], 'found' => $app['found']);
		} else {
			$result = array('data' => array(), 'found' => $app['found']);
		}
		// print_r($result);die;
		return $result;
	}
	// check app bị kiện
	public function abuseAppid($appid)
	{
		// $params['index'] = 'apktot_abuse';
		// $params['type'] = 'apktot_file';
		// $params['id'] = $appid;
		// $params['client'] = ['ignore' => 404];
		// $app = $this->client->get($params);
		$list_app_abuse = file_get_contents('http://apk.tovicorp.com/list_abuse.txt');
		$list_app_abuse = json_decode($list_app_abuse, true);
		if (in_array($appid, $list_app_abuse)) {
			return true;
		} else {
			return false;
		}
	}
	// lấy relate của 1 app
	public function relateApp($total_rate)
	{
		$where = [
			'index' => $this->name_index,
			'type' => $this->name_index,
			'body' => [
				"from" => 0, "size" => 5,
				'sort' => [
					['rate_total' => ["order" => "desc"]],
				],
				'query' => [
					'bool' => [
						'filter' => [
							'match' => [
								'is_file' => 1
							]
						],
						'must' => [
							['range' => [
								'rate_total' => [
									'lt' =>  $total_rate,
								]

							]],
						]
					]
				]
			],

		];
		$res = $this->client->search($where);

		$a = array();
		foreach ($res['hits']['hits'] as $key => $app) {
			//print_r($app);die;
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']['urlTitle'] = $app['_source']['urltitle'];
			$a[] = $app['_source'];
		}

		$result = $a;
		return $result;
	}
	// update developer
	public function updateDeveloper($data)
	{
		$data = json_decode($data, true);
		foreach ($data as $key => $value) {
			// print_r($value['dpi']);die;
			$params['index'] = 'device_mobile';
			$params['type'] = 'device_mobile';
			$params['id'] = $value['id'];
			$params['body']['doc']['id'] = $value['id'];
			$params['body']['doc']['dpi'] = $value['dpi'];
			$params['body']['doc']['os'] = $value['os'];
			$params['body']['doc']['androidVersion'] = $value['androidVersion'];
			$params['body']['doc']['sdk'] = $value['sdk'];
			$params['body']['doc']['chipset'] = $value['chipset'];
			$params['body']['doc']['brandName'] = $value['brandName'];
			$params['body']['doc']['deviceName'] = $value['deviceName'];
			$params['body']['doc']['height'] = $value['height'];
			$params['body']['doc']['width'] = $value['width'];
			$params['body']['doc']['gpu'] = $value['gpu'];
			$params['body']['doc']['cpu'] = $value['cpu'];
			$params['body']['doc']['models'] = $value['models'];
			print_r($params);
			die;
			$params['body']['doc_as_upsert'] = true;
			$response = $this->client->update($params);
			// $this->response(200,$response);
		}
	}
	// update device
	public function updateArchitecture($data)
	{
		$data = json_decode($data, true);
		foreach ($data as $key => $value) {
			// print_r($value['dpi']);die;
			$params['index'] = 'device_mobile';
			$params['type'] = 'device_mobile';
			$params['id'] = $value['id'];
			$params['body']['doc']['id'] = $value['id'];
			$params['body']['doc']['dpi'] = $value['dpi'];
			$params['body']['doc']['os'] = $value['os'];
			$params['body']['doc']['androidVersion'] = $value['androidVersion'];
			$params['body']['doc']['sdk'] = $value['sdk'];
			$params['body']['doc']['chipset'] = $value['chipset'];
			$params['body']['doc']['brandName'] = $value['brandName'];
			$params['body']['doc']['deviceName'] = $value['deviceName'];
			$params['body']['doc']['height'] = $value['height'];
			$params['body']['doc']['width'] = $value['width'];
			$params['body']['doc']['gpu'] = $value['gpu'];
			$params['body']['doc']['cpu'] = $value['cpu'];
			$params['body']['doc']['models'] = $value['models'];
			// print_r($params);die;
			$params['body']['doc_as_upsert'] = true;
			$response = $this->client->update($params);
			// $this->response(200,$response);
		}
	}

	public function getBrand($brand)
	{
		$page = 1;
		$size = 500;
		$from = ($page - 1) * $size;
		$params['index'] = 'list_brand';
		$params['type'] = 'list_brand';
		if ($brand != '') {
			$params['body'] = [
				'size' => $size,
				'from' => $from,
				'query' => [
					"prefix" => ['brandName' => $brand]
				],
			];
		} else {
			$params['body'] = [
				'size' => $size,
				'from' => $from,
			];
		}
		$res = $this->client->search($params);
		// print_r($res);die;
		foreach ($res['hits']['hits'] as $key => $app) {
			//print_r($app);die;
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']["brandName"] = $app['_source']['brandName'];
			$a[] = $app['_source'];
		}
		$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		return $result;
	}

	function generalmodel($brand)
	{
		$page = 1;
		$size = 500;
		$from = ($page - 1) * $size;
		$client = $this->client;
		$params['index'] = 'device_mobile';
		$params['type'] = 'device_mobile';
		$params['body'] = [
			'size' => $size,
			'from' => $from,
			'query' => [
				'bool' => [
					'must' => [
						[
							"match" => ['brandName' => $brand]
						],
						[
							"range" => [
								'sdk' => [
									'gte' => 1,
									'lte' => 33
								]
							]
						]
					]
				]
			],
		];
		$res = $client->search($params);
		// print_r($res);die;
		foreach ($res['hits']['hits'] as $key => $app) {
			//print_r($app);die;
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']["brandName"] = $app['_source']['brandName'];
			$app['_source']["deviceName"] = $app['_source']['deviceName'];
			$app['_source']["chipset"] = $app['_source']['chipset'];
			$app['_source']["dpi"] = $app['_source']['dpi'];
			$app['_source']["sdk"] = $app['_source']['sdk'];
			$app['_source']["models"] = $app['_source']['models'];
			$a[] = $app['_source'];
		}
		$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		return $result;
	}

	public function getModel($model)
	{
		$page = 1;
		$size = 3000;
		$from = ($page - 1) * $size;
		$client = $this->client;
		$params['index'] = 'device_mobile';
		$params['type'] = 'device_mobile';
		$params['body'] = [
			'size' => $size,
			'from' => $from,
			'query' => [
				"prefix" => ['deviceName' => $model]
			],
		];
		$res = $client->search($params);
		// print_r($res);die;
		foreach ($res['hits']['hits'] as $key => $app) {
			//print_r($app);die;
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']["brandName"] = $app['_source']['brandName'];
			$app['_source']["deviceName"] = $app['_source']['deviceName'];
			$app['_source']["chipset"] = $app['_source']['chipset'];
			$app['_source']["dpi"] = $app['_source']['dpi'];
			$app['_source']["sdk"] = $app['_source']['sdk'];
			$app['_source']["models"] = $app['_source']['models'];
			$a[] = $app['_source'];
		}
		$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		return $result;
	}
	public function getArchitecture($id)
	{
		$page = 1;
		$size = 500;
		$from = ($page - 1) * $size;
		$params['index'] = 'device_mobile';
		$params['type'] = 'device_mobile';
		$params['body'] = [
			'size' => $size,
			'from' => $from,
			'query' => [
				'bool' => [
					'must' => [
						[
							"match" => ['id' => $id]
						]
					]
				]

			],
		];
		$res = $this->client->search($params);
		// print_r($res);die;
		foreach ($res['hits']['hits'] as $key => $app) {
			//print_r($app);die;
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']["brandName"] = $app['_source']['brandName'];
			$app['_source']["deviceName"] = $app['_source']['deviceName'];
			$app['_source']["chipset"] = $app['_source']['chipset'];
			$app['_source']["dpi"] = $app['_source']['dpi'];
			$app['_source']["sdk"] = $app['_source']['sdk'];
			$app['_source']["models"] = $app['_source']['models'];
			$a[] = $app['_source'];
		}
		$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		return $result;
	}
	public function getFileVariant($appid, $chipset, $sdk)
	{
		$page = 1;
		$size = 500;
		$from = ($page - 1) * $size;
		$params['index'] = 'apk_file';
		$params['type'] = 'apk_file';
		$params['body'] = [
			'size' => $size,
			'from' => $from,
			'sort' => ['uploaddate' => ['order' => 'desc']],
			'query' => [
				'bool' => [
					'must' => [
						[
							"match" => ['appid' => $appid]
						],
						[
							"match" => ['architecture' => $chipset]
						],
						[
							"range" => [
								'requiresandroid' => [
									'gte' => 0,
									'lte' => $sdk
								]
							]
						]
					]
				]


			],
		];
		$res = $this->client->search($params);
		// print_r($res);die;
		foreach ($res['hits']['hits'] as $key => $app) {
			// print_r($app);die;
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			// $app['_source']["brandName"] = $app['_source']['brandName'];
			// $app['_source']["deviceName"] = $app['_source']['deviceName'];
			// $app['_source']["chipset"] = $app['_source']['chipset'];
			$app['_source']["dpi"] = $app['_source']['dpi'];
			// $app['_source']["sdk"] = $app['_source']['sdk'];
			// $app['_source']["models"] = $app['_source']['models'];
			$a[] = $app['_source'];
		}
		$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		return $result;
	}
	// lấy file của 1 app
	public function getallabuse($page)
	{
		// $page = 1;
		$size = 5000;
		$from = ($page - 1) * $size;
		$params['index'] = 'apktot_abuse';
		$params['type'] = 'apktot_file';
		$params['body'] = [
			'size' => $size,
			'from' => $from,
			// 'query' => [
			// 'bool' => [
			// 'must' => [
			// [
			// "match" => [ 'brandName' => $brand ]
			// ],
			// [
			// "range" => [
			// 'sdk' => [
			// 'gte' => 1,
			// 'lte' => 33
			// ]
			// ]
			// ]
			// ]
			// ]

			// ],
		];
		$res = $this->client->search($params);
		// print_r($res);die;
		foreach ($res['hits']['hits'] as $key => $app) {
			//print_r($app);die;
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']["added"] = $app['_source']['added'];
			$app['_source']["status"] = $app['_source']['status'];
			// $app['_source']["timestamp"] = $app['_source']['@timestamp'];
			$app['_source']["appid"] = $app['_source']['appid'];
			// $app['_source']["version"] = $app['_source']['@version'];
			$app['_source']["type"] = $app['_source']['type'];
			$a[] = $app['_source'];
		}
		$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		return $result;
	}
	public function getFile($page, $size, $id)
	{
		$from = ($page - 1) * $size;
		$params['index'] = 'apk_file';
		$params['type'] = 'apk_file';
		$params['body'] = [
			'size' => $size,
			'from' => $from,
			'sort' => ['uploaddate' => ['order' => 'desc']],
			'query' => [
				'match' => [
					'appid' => $id
				]

			],
		];
		$res = $this->client->search($params);
		// print_r($res);die;
		if (empty($res['hits']['hits'])) {
			return array('file' => 'not_found');
		}
		$a = array();
		foreach ($res['hits']['hits'] as $key => $app) {
			//print_r($app);die;
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']['size_apk'] = $this->formatSizeUnits($app['_source']['size']);
			$app['_source']['size_full'] = $this->formatSizeUnits($app['_source']['size'] + $app['_source']['obb_size'] + $app['_source']['patch_size']);
			$app['_source']['size_total'] = $app['_source']['size'] + $app['_source']['obb_size'] + $app['_source']['patch_size'];
			$app['_source']["uploadDate"] = $app['_source']['uploaddate'];
			$app['_source']["versionCode"] = $app['_source']['versioncode'];
			$a[] = $app['_source'];
		}

		$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		// print_r($result);die;
		return $result;
	}
	// list app theo category
	public function listAppOfCategory($category, $page, $size, $sort = null)
	{
		$total = 0;
		$from = ((int)$page - 1) * (int)$size;
		if ($from < 0) {
			$from = 0;
		}
		if ($from >= 1000000) {
			$from = 0;
		}
		switch ($sort) {
			case 'installs':
				$tmp = 'installs';
				break;
			case 'ratings':
				$tmp = 'score';
				break;
			case 'trending':
				$tmp = 'weekly_rating';
				break;
			case 'new':
				$tmp = 'inserted_date';
				break;
			case 'uploads':
				$tmp = 'uploaddate';
				break;
			default:
				$tmp = 'uploaddate';
				break;
		}
		$where = [
			'index' => $this->name_index,
			'type' => $this->name_index,
			'body' => [
				"from" => $from, "size" => $size,
				'sort' => [
					[$tmp => ["order" => "desc"]],
				],
				'query' => [
					'bool' => [
						'filter' => [
							'match' => [
								'is_file' => 1
							]
						],
						'must' => [
							['match' => [
								'category' =>  $category,
							]],
						]
					]
				]
			],

		];
		//print_r($where);
		$res = $this->client->search($where);
		$a = array();
		foreach ($res['hits']['hits'] as $key => $app) {
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']['uploadDate'] = $app['_source']['uploaddate'];
			$app['_source']['urlTitle'] = $app['_source']['urltitle'];
			if ($app['_source']['uploaddate'] == 0) {
				$app['_source']['uploadDate'] = time();
			}
			$a[] = $app['_source'];
		}
		//print_r(count($a));
		return $result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
	}
	// convert dung lượng ra text
	public function formatSizeUnits($bytes)
	{
		if ($bytes >= 1073741824) {
			$bytes = number_format($bytes / 1073741824, 1) . ' GB';
		} elseif ($bytes >= 1048576) {
			$bytes = number_format($bytes / 1048576, 1) . ' MB';
		} elseif ($bytes >= 1024) {
			$bytes = number_format($bytes / 1024, 1) . ' KB';
		} elseif ($bytes > 1) {
			$bytes = $bytes . ' bytes';
		} elseif ($bytes == 1) {
			$bytes = $bytes . ' byte';
		} else {
			$bytes = '0 bytes';
		}

		return $bytes;
	}
	protected function formatSizeUnitsMB($bytes)
	{
		if ($bytes >= 1073741824) {
			$bytes = number_format(($bytes / 1073741824), 1) . ' GB';
		} elseif ($bytes >= 1048576) {
			$bytes = number_format(($bytes / 1048576), 1) . ' MB';
		} elseif ($bytes >= 1024) {
			$bytes = number_format(($bytes / 1024), 1) . ' KB';
		} elseif ($bytes > 1) {
			$bytes = $bytes . ' bytes';
		} elseif ($bytes == 1) {
			$bytes = $bytes . ' byte';
		} else {
			$bytes = '0 bytes';
		}

		return $bytes;
	}
	// get list app by type ( type = 1  (game), type = 0  là app)
	public function getApkApp($page, $size, $type)
	{
		$from = ($page - 1) * $size;
		$params['index'] = $this->name_index;
		$params['type'] = $this->name_index;

		$params['body'] = [
			'size' => $size,
			'from' => $from,
			'sort' => ['uploaddate' => ['order' => 'desc']],
			'query' => [
				'bool' => [
					'filter' => [
						'match' => [
							'is_file' => 1
						]
					],
					'must' => [
						['match' => [
							'game' => $type
						]],
					]
				]

			],
		];
		$res = $this->client->search($params);
		$a = array();
		foreach ($res['hits']['hits'] as $key => $app) {
			unset($app['_source']['@version']);
			unset($app['_source']['@timestamp']);
			$app['_source']['urlTitle'] = $app['_source']['urltitle'];
			$app['_source']['uploadDate'] = $app['_source']['uploaddate'];
			if ($app['_source']['uploaddate'] == 0) {
				$app['_source']['uploadDate'] = time();
			}
			$a[] = $app['_source'];
		}
		$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		return $result;
	}
	// list app by dev
	public function listAppByDev($devID, $page, $size)
	{
		$from = ($page - 1) * $size;
		$where = [
			'index' => $this->name_index,
			'type' => $this->name_index,
			'body' => [
				"from" => $from, "size" => $size,
				'sort' => [
					["uploaddate" => ["order" => "desc"]],
					"_score"
				],
				'query' => [
					'bool' => [
						'filter' => [
							'match' => [
								'is_file' => 1
							]
						],
						'must' => [
							['match' => [
								'dev_id' =>  $devID,
							]],
						]
					]
				],
			]
		];
		//print_r($where);
		try {
			$res = $this->client->search($where);
			foreach ($res['hits']['hits'] as $app) {
				unset($app['_source']['@version']);
				unset($app['_source']['@timestamp']);
				$app['_source']['uploadDate'] = $app['_source']['uploaddate'];
				$app['_source']['urlTitle'] = $app['_source']['urltitle'];
				if ($app['_source']['uploaddate'] == 0) {
					$app['_source']['uploadDate'] = time();
				}
				$a[] = $app['_source'];
			}
			$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		} catch (\Exception $e) {
			$result = array('data' => array());
		}

		return $result;
	}
	public function similar()
	{
	}

	public function getAndroid($sdk)
	{
		switch ($sdk) {
			case 5:
				$rq = '2.0';
				break;
			case 6:
				$rq = '2.0.1';
				break;
			case 7:
				$rq = '2.1';
				break;
			case 11:
				$rq = '3.0';
				break;
			case 12:
				$rq = '3.1';
				break;
			case 13:
				$rq = '3.2';
				break;
			case 14:
				$rq = '4.0.1 - 4.0.2';
				break;
			case 15:
				$rq = '4.0.3 - 4.0.4';
				break;
			case 16:
				$rq = '4.1';
				break;
			case 17:
				$rq = '4.2';
				break;
			case 18:
				$rq = '4.3';
				break;
			case 19:
				$rq = '4.4';
				break;
			case 21:
				$rq = '5.0';
				break;
			case 22:
				$rq = '5.1';
				break;
			case 23:
				$rq = '6.0';
				break;
			case 24:
				$rq = '7.0';
				break;
			case 25:
				$rq = '7.1';
				break;
			case 26:
				$rq = '8.0.0';
				break;
			case 27:
				$rq = '8.1.0';
				break;
			case 28:
				$rq = '9';
				break;
			case 29:
				$rq = '10';
				break;
			default:
				$rq = '...';
				break;
		}
		return $rq;
	}
	// top app home
	public function getTopApps($page, $size, $obb)
	{
		$from = ($page - 1) * $size;
		$where = [
			'index' => $this->name_index,
			'type' => $this->name_index,
			'body' => [
				"from" => $from, "size" => $size,
				'sort' => [
					["uploaddate" => ["order" => "desc"]],
					"_score"
				],
				'query' => [
					'bool' => [
						'filter' => [
							'match' => [
								'is_file' => 1
							]
						],
						'must' => [
							['match' => [
								'obb' =>  $obb,
							]],
						]
					]
				],
			]
		];
		//print_r($where);
		try {
			$res = $this->client->search($where);
			foreach ($res['hits']['hits'] as $app) {
				unset($app['_source']['@version']);
				unset($app['_source']['@timestamp']);
				$app['_source']['uploadDate'] = $app['_source']['uploaddate'];
				$app['_source']['urlTitle'] = $app['_source']['urltitle'];
				if ($app['_source']['uploaddate'] == 0) {
					$app['_source']['uploadDate'] = time();
				}
				$a[] = $app['_source'];
			}
			$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		} catch (\Exception $e) {
			$result = array('data' => array());
		}

		return $result;
	}
	// top app home
	public function getTopAppsHot($page, $size, $obb)
	{
		$from = ($page - 1) * $size;
		$where = [
			'index' => $this->name_index,
			'type' => $this->name_index,
			'body' => [
				"from" => $from, "size" => $size,
				'sort' => [
					["installs" => ["order" => "desc"]],
					"_score"
				],
				'query' => [
					'bool' => [
						'filter' => [
							'match' => [
								'is_file' => 1
							]
						],
						'must' => [
							['match' => [
								'obb' =>  $obb,
							]],
						]
					]
				],
			]
		];
		//print_r($where);
		try {
			$res = $this->client->search($where);
			foreach ($res['hits']['hits'] as $app) {
				unset($app['_source']['@version']);
				unset($app['_source']['@timestamp']);
				$app['_source']['uploadDate'] = $app['_source']['uploaddate'];
				$app['_source']['urlTitle'] = $app['_source']['urltitle'];
				if ($app['_source']['uploaddate'] == 0) {
					$app['_source']['uploadDate'] = time();
				}
				$a[] = $app['_source'];
			}
			$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		} catch (\Exception $e) {
			$result = array('data' => array());
		}

		return $result;
	}
	// top game obb home
	public function getGameOBB()
	{
		$where = [
			'index' => 'apktrending_new',
			'type' => 'apktrending_new',
			'size' => '50',
			'body' => [
				'sort' => [
					["installs" => ["order" => "desc"]]
				],
				'query' => [
					'bool' => [
						'must' => [
							'terms' => [
								'appid' =>  ['com.miHoYo.GenshinImpact', 'com.dts.freefireth', 'com.tencent.ig', 'com.pubg.krmobile', 'com.vng.pubgmobile', 'com.activision.callofduty.shooter', 'com.garena.game.kgvn', 'com.igg.castleclash', 'com.rovio.baba', 'com.idleif.abyssrium', 'com.rovio.angrybirdsgo', 'com.pixonic.wwr', 'com.tap4fun.reignofwar', 'com.gamedevltd.wwh', 'com.crazylabs.lady.bug', 'com.alexanderwinn.TerraGenesis', 'com.xmgame.savethegirl', 'com.gamevil.valkyrie.android.google.global.normal', 'com.diandian.gog', 'com.libiitech.princesssalon', 'com.studiolb.tamingmaster', 'com.igg.android.mobileroyale'],
							]
						]
					]
				],
			]
		];
		// print_r($where);die;
		try {
			$res = $this->client->search($where);
			foreach ($res['hits']['hits'] as $app) {
				unset($app['_source']['@version']);
				unset($app['_source']['@timestamp']);
				$app['_source']['uploadDate'] = $app['_source']['uploaddate'];
				$app['_source']['urlTitle'] = $app['_source']['urltitle'];
				if ($app['_source']['uploaddate'] == 0) {
					$app['_source']['uploadDate'] = time();
				}
				$a[] = $app['_source'];
			}
			$result = array('total' => $res['hits']['total'], 'page' => $page, 'data' => $a);
		} catch (\Exception $e) {
			$result = array('data' => array());
		}

		return $result;
	}
}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */