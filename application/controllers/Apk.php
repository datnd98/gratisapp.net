<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'Api.php';
class Apk extends CI_Controller
{
	protected $client;
	protected $name_index;
	public function __construct()
	{
		//echo 1;die;
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('fnc');
		// $this->output->cache(86400);
	}

	public function index()
	{
		//$this->output->clear_all_cache();
		//$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		//$this->output->cache(86400);
		//$this->load->model('apkmodel');
		//$sql = "SELECT title,appid,dev_id,offerby,score,uploadDate,cover,urlTitle from apkshow where game = 0 order by uploadDate DESC limit 0,12";
		//print_r($sql);
		$api = new Api();
		// for ($i = 1; $i < 12; $i++) {
		// $res = $api->getallabuse($i);
		// file_put_contents("list_app_abuse.txt", json_encode($res['data']).PHP_EOL,  FILE_APPEND | LOCK_EX);
		// // print_r($res);die;
		// }
		// die("done");
		$page = random_int(1, 5);
		// $topgames = $api->get_detail('com.facebook.katana');
		$topgames = $api->getTopApps($page, 12, 1);
		$topapps = $api->getTopApps($page, 12, 0);
		$topapps_hot = $api->getTopAppsHot($page, 12, 0);
		$topgames_hot = $api->getTopAppsHot($page, 12, 1);
		$game_obb_hot = $api->getGameOBB();
		// print_r($game_obb_hot);die;
		//$sql = "SELECT title,appid,dev_id,offerby,score,uploadDate,cover,thumbnails,size,urlTitle from apkshow where OBB = 1 order by installs DESC limit 0,13";
		//print_r($sql);
		//$topapps = $this->apkmodel->getRawSql($sql);
		// print_r($topapps['data']);die;
		// print_r($topgames['data']);die;
		// print_r($topapps_hot['data']);die;
		// print_r($topgames_hot['data']);die;
		// print_r($game_obb_hot['data']);die;
		$data['main'] = 'home';
		$data['meta'] = 'meta_home';
		$data['style'] = 'style/style';
		$data['topapps'] = $topapps['data'];
		$data['topgames'] = $topgames['data'];
		$data['topapps_hot'] = $topapps_hot['data'];
		$data['topgames_hot'] = $topgames_hot['data'];
		$data['game_obb_hot'] = $game_obb_hot['data'];
		$this->load->view('master', $data, FALSE);
	}
	public function detail()
	{
		//$this->output->delete_cache();
		// if($api->abuseAppid($appid)){
		//           header("HTTP/1.1 301 Moved Permanently"); 
		// 	header('Location: https://apktrending.com');
		// 	exit();
		//       }
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		// $this->output->cache(86400);
		//$this->load->model('apkmodel');
		$slug = $this->uri->segment(2);

		//$appid = explode('-', $slug);
		//$appid = $appid[count($appid) - 1];
		if (substr($slug, 0, 4) == 'app-') {
			$appid = substr($slug, 4);
			$appid = 'com.' . $appid;
		} elseif (substr($slug, 0, 3) == 'ap-') {
			$appid = substr($slug, 3);
			$appid = 'app.' . $appid;
		} else {
			$appid = $slug;
		}
		//$appid = str_replace('app-', 'com.', $slug);
		$appid = str_replace('.html', '', $appid);
		$appid = str_replace('-', '.', $appid);
		// $sql = "SELECT appid from abuse_app where appid = '$appid' limit 1";


		// $abuse = $this->apkmodel->getRawSql($sql);
		// if(count($abuse) >= 1){
		// 	header("HTTP/1.1 301 Moved Permanently"); 

		// 	header('Location: https://apktrending.com');
		// 	exit();
		// }
		// $sql = "SELECT * from apkshow where appid = '$appid'";
		// //print_r($sql);die;
		// $result = $this->apkmodel->getRawSql($sql);
		// if(empty($result)){
		// 	header("HTTP/1.1 301 Moved Permanently"); 
		// 	header('Location: https://apktrending.com');
		// 	exit();
		// }
		$api = new Api();
		$result = $api->getDetailApp($appid);
		//print_r($result);die;
		$result = $result['data'];
		header("HTTP/1.1 301 Moved Permanently");
		header('Location: https://apktrending.com/' . $result['urlTitle'] . '/' . $appid);
		exit();
		//print_r($appid);
		$sql_file = "SELECT * FROM apk_file where appid='$appid' order by versionCode DESC limit 0,10 ";
		$files = $this->apkmodel->getRawSql($sql_file);
		//print_r($files);
		$data['apk'] = $result[0];
		$data['files'] = $files;
		$data['main'] = 'detail';
		$data['meta'] = 'meta_detail';
		$data['style'] = 'style/style';
		$this->load->view('master', $data, FALSE);
	}
	public function deleteCache()
	{
		$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
		//echo $uri;
		$uri = $_GET['uri'];
		$this->output->delete_cache($uri);
	}
	public function page()
	{
		$id = $this->uri->segment(1);
		//echo $id;
		if ($id == 'dmca') {
			$page = 'page/dmca';
			$data['title'] = 'DMCA';
		} elseif ($id == 'privacy-policy') {
			$page = 'page/privacy';
			$data['title'] = 'Privacy Policy';
		} elseif ($id == 'about') {
			$page = 'page/about';
			$data['title'] = 'About';
		}

		$data['page'] = $page;
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$data['main'] = 'page';
		$data['meta'] = 'meta_page';
		$data['style'] = 'style/style';
		$this->load->view('master', $data, FALSE);
	}

	public function detailNew()
	{
		//$this->output->delete_cache();

		// $this->output->cache(86400);
		$api = new Api();
		$appid = $this->uri->segment(2);
		$slug = $this->uri->segment(1);
		if ($appid == 'com.facebook.katana' || $appid == 'com.google.android.gms' || $appid == 'com.viber.voip' || $appid == 'org.telegram.messenger') {
			$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
			//$this->output->cache(86400);
		}
		$list_app_abuse = file_get_contents('http://apk.tovicorp.com/list_abuse.txt');
		$list_app_abuse = json_decode($list_app_abuse, true);
		if ($appid != 'com.facebook.katana') {
			if (in_array(strtolower($appid), $list_app_abuse)) {
				header("HTTP/1.1 301 Moved Permanently");
				header('Location: https://apktrending.com');
				exit();
			}
		}
		if ($appid != 'com.facebook.katana') {
			if ($api->abuseAppid($appid)) {
				header("HTTP/1.1 301 Moved Permanently");
				header('Location: https://apktrending.com');
				exit();
			}
		}
		$result = $api->getDetailApp($appid);
		$result = $result['data'];
		if (empty($result)) {
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: https://apktrending.com');
			exit();
		}

		$urlTitle = $result['urlTitle'];
		if (empty($urlTitle)) {
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: https://apktrending.com');
			exit();
		}
		// echo $urlTitle;
		// echo PHP_EOL;
		// echo urlencode($urlTitle);die;
		if (urlencode($urlTitle) != $slug) {
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: https://apktrending.com/' . urlencode($urlTitle) . '/' . $appid);
			exit();
		}
		//print_r($appid);
		//$sql_file = "SELECT * FROM apk_file where appid='$appid' order by versionCode DESC limit 0,10 ";
		//$files = $this->apkmodel->getRawSql($sql_file);
		// if($appid == 'com.facebook.katana' || $appid == 'com.google.android.gms' || $appid == 'com.viber.voip' || $appid =='org.telegram.messenger'){
		$files  = $api->getFile(1, 100, $appid);
		$version = array();
		foreach ($files['data'] as $file) {
			$version[] = $file['version'];
		}
		$version = array_unique($version);
		foreach ($files['data'] as $tmp) {
			$list_version[$tmp['version']][] = $tmp;
		}
		//print_r($list_version);
		$data['list_version'] = $list_version;
		$data['version'] = $version;
		// } else {
		// $files = $api->getFile(1, 1, $appid);
		// }
		// $list_brand = $api->getAllBrand();
		// print_r($list_brand['data']);die;
		$relate = $api->relateApp($result['rate_total']);
		// print_r($relate);die;
		$devApps = $api->listAppByDev($result['dev_id'], 1, 5);
		$files  = $files['data'];
		// print_r($files);die;

		$images = json_decode($result['thumbnails'], true);
		// print_r($images['image']);die;

		$testing = $files[0]['testing'];
		$obb = 0;
		// $link_download['apk'] = $title."-novariant-apk-".$appid."/".$version[0]."/".$list_version[$ver][0]['versioncode'];
		$link_download['apk'] = $this->convertUrl($appid, $result['title'], $version[0], $list_version[$version[0]][0]['versioncode'], 0, 0);
		if ($testing == 2) {
			$obb = 2;
		} elseif ($files[0]['obb_size'] > 0) {
			$obb = 1;
		}
		$variant = 0;
		if (count($list_version[$version[0]]) > 2) {
			$variant = 1;
		}
		if ($variant == 1) {
			foreach ($list_version[$version[0]] as $item) {
				// $link_download['variant'][] = $result['title']."-variant-apk-".$appid."/".$ver."/".$item['versioncode'];
				$link_download['variant'][] = $this->convertUrl($appid, $result['title'], $version[0], $item['versioncode'], 1, 0);
			}
		} elseif ($obb == 2) {
			// $link_download['bundle'] = $result['title']."-novariant-bundle-".$appid."/".$ver."/".$list_version[$ver][0]['versioncode'];
			$link_download['bundle'] = $this->convertUrl($appid, $result['title'], $version[0], $list_version[$version[0]][0]['versioncode'], 0, 1);
		} elseif ($obb == 1) {
			// $link_download['obb'] = $result['title']."-novariant-obb-".$appid."/".$ver."/".$list_version[$ver][0]['versioncode'];
			$link_download['obb'] = $this->convertUrl($appid, $result['title'], $version[0], $list_version[$version[0]][0]['versioncode'], 0, 2);
		}
		// print_r($link_download);die;
		// print_r($result);die;
		// print_r($devApps['data']);
		// $data['brands'] = $list_brand['data'];
		$data['apk'] = $result;
		$data['files'] = $files;
		$data['devApps'] = $devApps['data'];
		$data['relate'] = $relate;
		$data['list_version'] = $list_version;
		$data['link_download'] = $link_download;
		$data['main'] = 'detail';
		$data['meta'] = 'meta_detail';
		$data['style'] = 'style/style';
		$data['images'] = $images['image'];
		$this->load->view('master', $data, FALSE);
	}

	public function devApps()
	{
		$api = new Api();
		$dev_id = $this->uri->segment(2);
		$dev_id = urldecode($dev_id);
		//echo $dev_id;
		//$this->load->model('apkmodel');
		//$sql = "SELECT title,appid,dev_id,offerby,score,uploadDate,cover,urlTitle from apkshow where dev_id = '$dev_id' and is_file = 1";
		// //print_r($sql);
		//$result = $this->apkmodel->getRawSql($sql);
		$result = $api->listAppByDev($dev_id, 1, 100);
		$result = $result['data'];
		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['list'] = $result;
		$data['totalPage'] = 1;
		$data['page'] = 1;
		$data['title_page'] = $result[0]['offerby'];
		$data['sub_page'] = 'Top Apps';
		$data['title'] = $result[0]['offerby'] . ' top apps - ' . date('F Y', time());
		$data['description'] = 'Best Android apps developed by ' . $result[0]['offerby'] . ' - Selective collection from apkTrending in ' . date('F Y', time());
		$this->load->view('master', $data, FALSE);
	}
	public function apkApp()
	{
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}
		$api = new Api();
		$result = $api->getApkApp($page, 12, 0);

		// print_r($result['data']);die;
		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['list'] = $result['data'];
		$data['totalPage'] = ceil($result['total'] / 12);
		$data['page'] = $page;
		$data['title_page'] = 'App';
		$data['sub_page'] = 'Top Apps';
		$data['title'] = 'APK Download - Free Android Apps | gratisapp.net';
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$this->load->view('master', $data, FALSE);
	}

	public function apkGame()
	{
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}
		$api = new Api();
		$result = $api->getApkApp($page, 12, 1);
		//$result = $result['data'];

		// print_r($result['data']);die;
		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['list'] = $result['data'];
		$data['totalPage'] = ceil($result['total'] / 12);
		$data['page'] = $page;
		$data['title_page'] = 'Game';
		$data['sub_page'] = 'Top Games';
		$data['title'] = 'APK Download - Free Android Games | gratisapp.net';
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$this->load->view('master', $data, FALSE);
	}

	public function viewallnewapp()
	{
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}
		$api = new Api();

		$topapps = $api->getTopApps($page, 15, 0);

		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['allstyle'] = 'style/style';
		$data['category'] = "APP";
		$data['list'] = $topapps['data'];
		$data['totalPage'] = 10;
		$data['page'] = $page;
		$data['title_page'] = '';
		$data['sub_page'] = 'Top New Apps';
		$data['title'] = 'APK Download - Free Android Apps | apkTrending.com';
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$this->load->view('master', $data, FALSE);
	}
	public function viewallpopularapp()
	{
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}
		$api = new Api();

		$topapps = $api->getTopAppsHot($page, 15, 0);

		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['allstyle'] = 'style/style';
		$data['category'] = "APP";
		$data['list'] = $topapps['data'];
		$data['totalPage'] = 10;
		$data['page'] = $page;
		$data['title_page'] = '';
		$data['sub_page'] = 'Top New Apps';
		$data['title'] = 'APK Download - Free Android Apps | apkTrending.com';
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$this->load->view('master', $data, FALSE);
	}

	public function viewallgame24h()
	{
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}
		$api = new Api();

		$topapps = $api->getTopAppsHot($page, 15, 1);

		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['allstyle'] = 'style/style';
		$data['category'] = "GAME";
		$data['list'] = $topapps['data'];
		$data['totalPage'] = 10;
		$data['page'] = $page;
		$data['title_page'] = '';
		$data['sub_page'] = 'Top New Apps';
		$data['title'] = 'APK Download - Free Android Apps | apkTrending.com';
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$this->load->view('master', $data, FALSE);
	}

	public function viewallnewgame()
	{
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}
		$api = new Api();

		$topapps = $api->getTopApps($page, 15, 1);

		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['allstyle'] = 'style/style';
		$data['category'] = "GAME";
		$data['list'] = $topapps['data'];
		$data['totalPage'] = 10;
		$data['page'] = $page;
		$data['title_page'] = '';
		$data['sub_page'] = 'Top New Apps';
		$data['title'] = 'APK Download - Free Android Apps | apkTrending.com';
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$this->load->view('master', $data, FALSE);
	}

	public function categoryApps()
	{
		$api = new Api();
		//$this->load->model('apkmodel');
		$category = $this->uri->segment(2);
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		} else {
			$page = 1;
		}
		$from = ($page - 1) * 12;
		$size = 12;
		$category = urldecode($category);
		$result = $api->listAppOfCategory($category, $page, $size);
		//$sql = "SELECT title,appid,dev_id,offerby,score,uploadDate,cover,urlTitle from apkshow where category = '$category' and is_file = 1 order by uploadDate DESC limit $from,12";
		//print_r($sql);
		$totalPage = ceil($result['total'] / 12);
		//$result = $this->apkmodel->getRawSql($sql);
		$result = $result['data'];
		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['list'] = $result;
		$data['totalPage'] = $totalPage;
		$data['page'] = $page;
		$data['title_page'] = $category;
		$data['sub_page'] = 'Top Apps';
		$data['title'] = $category . ' top apps - ' . date('F Y', time());
		$data['description'] = 'Best ' . $category . ' Android apps of ' . date('F Y', time()) . ' selected by apkTrending';
		$this->load->view('master', $data, FALSE);
	}

	public function downloadApp()
	{
		$api = new Api();
		$slug = $this->uri->segment(2);
		$info = base64_decode($slug);
		$info = explode("&", $info);
		$check = $info[1];
		$check = str_replace("check=", "", $check);
		// print_r($check);die;exit();
		$info = base64_decode($info[0]);
		parse_str($info, $output);
		if (md5($output['title'] . "Tovi123@") == $check) {
			$appid = $output['id'];
			$list_app_abuse = file_get_contents('/var/www/html/apktrending/assets/list_abuse.txt');
			$list_app_abuse = json_decode($list_app_abuse, true);
			if ($appid != 'com.facebook.katana') {
				if (in_array(strtolower($appid), $list_app_abuse)) {
					header("HTTP/1.1 301 Moved Permanently");
					header('Location: https://apktrending.com');
					exit();
				}
			}
			$version = $output['v'];
			$versionCode = $output['sn'];
			$title = $output['title'];
			$variant = $output['variant'];
			$apk = $output['apk'];
			$title = str_replace(",", " ", $title);
			$title = str_replace("/", " ", $title);
			$title = str_replace(".", " ", $title);
			$title = str_replace("*", " ", $title);
			$title = str_replace(":", " ", $title);
			$title = str_replace("&", "", $title);
			$title = str_replace("-", "", $title);
			$title = urlencode($title);
			// print_r($title);die;
			$t = time();
			if ($apk == 'apk') {
				if ($variant == 1) {
					$secret = $appid . "__Tovi@1234";
					$secret_code = md5($secret);
					$uri = "?id=" . $appid . "&title=" . $title . "&version=" . $version . "&vcc=" . $versionCode . "&variant=1&apk=1";
					$uri = $uri . "&k=$secret_code&t=$t";
					$uri = base64_encode($uri);
					$download_link = "https://download.apktrending.com/" . $uri;
				} else {
					$secret = $appid . "__Tovi@1234";
					$secret_code = md5($secret);
					$uri = "?id=" . $appid . "&title=" . $title . "&version=" . $version . "&vcc=" . $versionCode . "&apk=1";

					$uri = $uri . "&k=$secret_code&t=$t";
					$uri = base64_encode($uri);
					$download_link = "https://download.apktrending.com/" . $uri;
				}
				// die();
			} else {
				if ($variant == 1) {
					$secret = $appid . "__Tovi@1234";
					$secret_code = md5($secret);
					$uri = "?id=" . $appid . "&title=" . $title . "&version=" . $version . "&vcc=" . $versionCode . "&variant=1";
					$uri = $uri . "&k=$secret_code&t=$t";
					$uri = base64_encode($uri);
					$download_link = "https://download.apktrending.com/" . $uri;
				} else {
					$secret = $appid . "__Tovi@1234";
					$secret_code = md5($secret);
					$uri = "?id=" . $appid . "&title=" . $title . "&version=" . $version . "&vcc=" . $versionCode;
					$uri = $uri . "&k=$secret_code&t=$t";
					$uri = base64_encode($uri);
					$download_link = "https://download.apktrending.com/" . $uri;
				}
			}
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: $download_link");
			exit();
		} else {
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: https://apktrending.com');
			exit();
		}
	}

	public function livesearch()
	{
		/*		$this->load->model('apkmodel');
		$q = $_POST['q'];
		//echo $q;die;
		$sql = "SELECT appid,offerby,dev_id,cover from apkshow where MATCH(title) against ('$q' IN NATURAL LANGUAGE MODE) limit 0,10";
		echo $sql;
		$result = $this->apkmodel->getRawSql($sql);
		echo json_encode($result);*/
		require './vendor/autoload.php';
		$this->client = \Elasticsearch\ClientBuilder::create()->setSSLVerification(false)->setHosts(["46.4.88.151:9200"])->build();
		$this->name_index = 'apktot';
		//$this->api_img = 'http://apk.lenam.com.vn/toan/apk/images/storage/logos/';
		$keyword = $_POST['q'];
		$tmp_key = str_replace(' ', '', $keyword);
		$where = [
			'index' => 'apktrending_new',
			'type' => 'apktrending_new',
			'body' => [
				"from" => 0, "size" => 5,
				'sort' => [
					["installs" => ["order" => "desc"]],
					"_score"
				],
				'query' =>  [
					'multi_match' => [
						"query"  =>   '*' . $keyword . '*',
						"type" => "phrase_prefix",
						"fields" => ["title"],
						"tie_breaker" => 0.2,
						"minimum_should_match" => "10%",
						"analyzer" => "standard",
					],
				]
			]
		];
		$res = $this->client->search($where);
		if ($test == 1) {
			print_r($where);
			print_r($res['hits']['hits']);
		}
		$result = array();
		foreach ($res['hits']['hits'] as $item) {

			$item['_source']['urlTitle'] = $item['_source']['urltitle'];
			//$tmp = array('appid' => $item['_source']['appid'],'title' => $item['_source']['title'],'offerby' => $item['_source']['offerby'],'cover' => $item['_source']['cover'],'dev_id' => $item['_source']['dev_id'],'urltitle' => $item['_source']['urltitle'],'score' => $item['_source']['score']);
			$result[] = $item['_source'];
		}
		echo json_encode($result);
	}
	public function search()
	{
		require './vendor/autoload.php';
		$this->client = \Elasticsearch\ClientBuilder::create()->setSSLVerification(false)->setHosts(["46.4.88.151:9200"])->build();
		$this->name_index = 'apktrending';
		// $this->output->delete_cache();
		//$this->api_img = 'http://apk.lenam.com.vn/toan/apk/images/storage/logos/';
		//   	ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);
		$keyword = $_GET['q'];
		if ($keyword == '') {
			$keyword = 'pubg';
		}
		$tmp_key = str_replace(' ', '', $keyword);
		$where = [
			'index' => 'apktrending_new',
			'type' => 'apktrending_new',
			'body' => [
				"from" => 0, "size" => 5,
				'sort' => [
					["installs" => ["order" => "desc"]],
					"_score"
				],
				'query' =>  [
					'bool' => [
						'should' => [
							['bool' => [
								'filter' => [

									'multi_match' => [
										"query"  =>   '*' . $keyword . '*',
										"type" => "phrase_prefix",
										"fields" => ["title"],
										"tie_breaker" => 0.2,
										"minimum_should_match" => "10%",
										"analyzer" => "standard",
									],

								],
							]],
							['bool' => [
								'filter' => [

									'multi_match' => [
										"query"  =>   '*' . $tmp_key . '*',
										"type" => "phrase_prefix",
										"fields" => ["title"],
										"tie_breaker" => 0.2,
										"minimum_should_match" => "10%",
										"analyzer" => "standard",
									],

								],
							]],
						]
					]
				],
			]
		];
		$res = $this->client->search($where);
		if ($res['hits']['total'] == 0) {
			$check_status = $this->CheckUrl($tmp_key);
			if ($check_status == 200) {
				header("HTTP/1.1 301 Moved Permanently");
				header('Location: https://apktrending.com/aa/' . $tmp_key);
				exit();
			}
		}
		// print_r($res);die;
		$result = array();
		foreach ($res['hits']['hits'] as $item) {

			$item['_source']['urlTitle'] = $item['_source']['urltitle'];
			$item['_source']['uploadDate'] = $item['_source']['uploaddate'];
			//$tmp = array('appid' => $item['_source']['appid'],'title' => $item['_source']['title'],'offerby' => $item['_source']['offerby'],'cover' => $item['_source']['cover'],'dev_id' => $item['_source']['dev_id'],'urltitle' => $item['_source']['urltitle'],'score' => $item['_source']['score']);
			$result[] = $item['_source'];
		}
		//echo json_encode($result);
		$totalPage = 0;
		$page = 1;
		$data['meta'] = 'meta_list';
		$data['main'] = 'list';
		$data['style'] = 'style/style';
		$data['list'] = $result;
		$data['totalPage'] = $totalPage;
		$data['page'] = $page;
		//$data['title'] = 'Result of '.$keyword;
		$data['title_page'] = $keyword;
		$data['key_search'] = $keyword;
		$data['sub_page'] = 'List Apps';
		$data['title'] = $keyword . ' List apps - ' . date('F Y', time());
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$this->load->view('master', $data, FALSE);
	}
	public function CheckUrl($appid)
	{
		$url = 'https://play.google.com/store/apps/details?id=' . $appid;
		// print_r($url);die;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_USERAGENT => "Mozilla/5.0 (iPad; CPU OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53",
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));
		$resp = curl_exec($curl);
		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		return $http_code;
	}

	// public function showAllBrand()
	// {
	// 	$api = new Api();
	// 	$this->load->model('apkmodel');
	// 	$list_brand = $api->getAllBrand();
	// 	// print_r($list_brand['data']);die;
	// 	return json_encode($list_brand['data']);
	// }

	public function showBrand()
	{
		$api = new Api();
		$this->load->model('apkmodel');
		$brand = $_GET['brand'];
		$brand = strtolower($brand);
		// print_r($brand);die;
		if ($brand != '') {
			$list_brand = $api->getBrand($brand);
			$html = '';
			if (count($list_brand['data']) > 0) {
				foreach ($list_brand['data'] as $key => $value) {
					if ($key < 5) {
						$html .= '<li onclick="Select_Item(this.innerHTML)">' . $value['brandName'] . '</li>';
					}
				}
			} else {
				$html .= '<li>Can\'t find this brand</li>';
			}
			echo $html;
		} else {
			echo '';
		}
		// print_r($list_brand);die;
	}

	function generalmodel()
	{
		$api = new Api();
		$brand = $_GET['brand'];
		if ($brand != '') {
			$list_brand = $api->generalmodel($brand);
			$html = '';
			$i = 0;
			if (count($list_brand['data']) > 0) {
				foreach ($list_brand['data'] as $value) {
					if ($i < 5) {
						$html .= '<li onclick="select_model(this.innerHTML)">' . $value['deviceName'] . '</li>';
						$i++;
					}
				}
			} else {
				$html .= '<li>Can\'t find any model</li>';
			}
			$html .= '';
			echo $html;
		} else {
			echo '';
		}
		// print_r($list_brand);die;
	}

	public function showmodel()
	{
		$api = new Api();
		$this->load->model('apkmodel');
		$brand = $_GET['brand'];
		$model = $_GET['model'];
		if ($brand != '') {
			if ($model != '') {
				$i = 0;
				$html = '';
				$list_brand = $api->getModel($model);
				if (count($list_brand['data']) > 0) {
					foreach ($list_brand['data'] as $value) {
						if ($value['brandName'] == $brand) {
							if ($i < 5) {
								$html .= '<li onclick="select_model(this.innerHTML)">' . $value['deviceName'] . '</li>';
								$i++;
							}
						}
					}
				} else {
					$html .= '<li>Can\'t find this device</li>';
				}
				echo $html;
			}
		} else {
			echo '';
		}
		// print_r($list_brand);die;
	}
	public function generallink()
	{
		$api = new Api();
		$this->load->model('apkmodel');
		$appid = $_GET['appid'];
		$urlTitle = $_GET['urlTitle'];
		$id_device = $_GET['id_device'];
		$sdk = $_GET['sdk'];
		if ($sdk == 0) {
			$sdk = 29;
		}
		$result = $api->getDetailApp($appid);
		$result = $result['data'];
		$files  = $api->getFile(1, 100, $appid);
		foreach ($files['data'] as $file) {
			$version[] = $file['version'];
		}
		$version = array_unique($version);
		foreach ($files['data'] as $tmp) {
			$list_version[$tmp['version']][] = $tmp;
		}
		foreach ($list_version[$version[0]] as $item) {
			// $link_download['variant'][] = $result['title']."-variant-apk-".$appid."/".$ver."/".$item['versioncode'];
			$link_download['variant'][] = $this->convertUrl($appid, $result['title'], $version[0], $item['versioncode'], 1, 0);
		}
		// print_r($id_device);
		// print_r($appid);die;
		$architecture = $api->getArchitecture($id_device);
		// print_r($architecture);die;
		if (!empty($architecture['data'])) {
			// print_r($architecture);die;
			$chipset = $architecture['data'][0]['chipset'];
			$dpi = $architecture['data'][0]['dpi'];
			$list_file = $api->getFileVariant($appid, $chipset, $sdk);
			$html = '';
			foreach ($list_file['data'] as $key => $value) {
				$dpi_arr = json_decode($value['dpi'], true);
				if ($value['version'] == $list_file['data'][0]['version']) {
					if ($value['requiresandroid'] <= $sdk) {
						if ($dpi_arr['dpi_min'] <= $dpi && $dpi <= $dpi_arr['dpi_max']) {
							$html .= '<tr>';
							$html .= '<td><p class="var_num">' . $value['versioncode'] . '</p><p class="date">' . date('Y-m-d', strtotime($value['uploaddate'])) . '</p></td>';
							$html .= '<td>' . $value['architecture'] . '</td>';
							$html .= '<td>Android ' . $this->getAndroid($value['requiresandroid']) . '+</td>';
							// print_r($dpi_arr);die;
							if ($dpi_arr['dpi_min'] == -1 && $dpi_arr['dpi_max'] == -1) {
								$dpii = 'NO DPI';
							} elseif ($dpi_arr['dpi_min'] == $dpi_arr['dpi_max']) {
								$dpii = $dpi_arr['dpi_min'] . ' DPI *';
							} else {
								$dpii = $dpi_arr['dpi_min'] . '-' . $dpi_arr['dpi_max'] . ' DPI *';
							}
							$html .= '<td>' . $dpii . '</td>';
							$html .= '<td class="hidden-sm hidden-xs"><a href="' . base_url() . "download/" . $link_download['variant'][$key] . '" class="btn_down"><svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M5.25 0.5L5.25 9.6275L2.5575 6.9425L1.5 8L6 12.5L10.5 8L9.4425 6.9425L6.75 9.6275L6.75 0.5L5.25 0.5Z" fill="white" />
							<path d="M0.75 14H11.25V15.5H0.75V14Z" fill="white" />
						</svg>
						Download</a></td></tr>';
							$html .= '<tr class="hidden-md hidden-lg"<td colspan="4"><a href="' . base_url() . "download/" . $link_download['variant'][$key] . '" class="btn_down"><svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M5.25 0.5L5.25 9.6275L2.5575 6.9425L1.5 8L6 12.5L10.5 8L9.4425 6.9425L6.75 9.6275L6.75 0.5L5.25 0.5Z" fill="white" />
							<path d="M0.75 14H11.25V15.5H0.75V14Z" fill="white" />
						</svg>
						Download</a></td></tr>';
						} elseif ($dpi_arr['dpi_min'] == '-1' && $dpi_arr['dpi_max'] == '-1') {
							$html .= '<tr>';
							$html .= '<td><p class="var_num">' . $value['versioncode'] . '</p><p class="date">' . date('Y-m-d', strtotime($value['uploaddate'])) . '</p></td>';
							$html .= '<td>' . $value['architecture'] . '</td>';
							$html .= '<td>Android ' . $this->getAndroid($value['requiresandroid']) . '+</td>';
							// print_r($dpi_arr);die;
							if ($dpi_arr['dpi_min'] == -1 && $dpi_arr['dpi_max'] == -1) {
								$dpii = 'NO DPI';
							} elseif ($dpi_arr['dpi_min'] == $dpi_arr['dpi_max']) {
								$dpii = $dpi_arr['dpi_min'] . ' DPI *';
							} else {
								$dpii = $dpi_arr['dpi_min'] . '-' . $dpi_arr['dpi_max'] . ' DPI *';
							}
							$html .= '<td>' . $dpii . '</td>';
							$html .= '<td class="hidden-sm hidden-xs"><a href="' . base_url() . "download/" . $link_download['variant'][$key] . '" class="btn_down"><svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M5.25 0.5L5.25 9.6275L2.5575 6.9425L1.5 8L6 12.5L10.5 8L9.4425 6.9425L6.75 9.6275L6.75 0.5L5.25 0.5Z" fill="white" />
							<path d="M0.75 14H11.25V15.5H0.75V14Z" fill="white" />
						</svg>
						Download</a></td></tr>';
							$html .= '<tr class="hidden-md hidden-lg"<td colspan="4"><a href="' . base_url() . "download/" . $link_download['variant'][$key] . '" class="btn_down"><svg width="12" height="16" viewBox="0 0 12 16" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M5.25 0.5L5.25 9.6275L2.5575 6.9425L1.5 8L6 12.5L10.5 8L9.4425 6.9425L6.75 9.6275L6.75 0.5L5.25 0.5Z" fill="white" />
							<path d="M0.75 14H11.25V15.5H0.75V14Z" fill="white" />
						</svg>
						Download</a></td></tr>';
						}
					}
				}
			}
			// print_r($html);die;
			echo $html;
		} else {
			echo '';
		}
		// print_r($list_brand);die;
	}
	public function verificationApp()
	{
		$data['meta'] = 'meta_list';
		$data['main'] = 'verification';
		$data['style'] = 'style/style';
		// $data['list'] = $result['data'];
		$data['title_page'] = '';
		$data['title'] = 'APK Download - Free Android Games | gratisapp.net';
		$data['description'] = 'Download Android apps & games APK files for Mobile & PC. Get latest versions of APK & obb/data for free and 100% safe!';
		$this->load->view('master', $data, FALSE);
	}
	public function getAndroid($sdk)
	{
		switch ($sdk) {
			case 5:
				$rq = '2.0';
				break;
			case 6:
				$rq = '2.0.1';
				break;
			case 7:
				$rq = '2.1';
				break;
			case 11:
				$rq = '3.0';
				break;
			case 12:
				$rq = '3.1';
				break;
			case 13:
				$rq = '3.2';
				break;
			case 14:
				$rq = '4.0.1 - 4.0.2';
				break;
			case 15:
				$rq = '4.0.3 - 4.0.4';
				break;
			case 16:
				$rq = '4.1';
				break;
			case 17:
				$rq = '4.2';
				break;
			case 18:
				$rq = '4.3';
				break;
			case 19:
				$rq = '4.4';
				break;
			case 21:
				$rq = '5.0';
				break;
			case 22:
				$rq = '5.1';
				break;
			case 23:
				$rq = '6.0';
				break;
			case 24:
				$rq = '7.0';
				break;
			case 25:
				$rq = '7.1';
				break;
			case 26:
				$rq = '8.0.0';
				break;
			case 27:
				$rq = '8.1.0';
				break;
			case 28:
				$rq = '9';
				break;
			case 29:
				$rq = '10';
				break;
			default:
				$rq = '...';
				break;
		}
		return $rq;
	}

	public function convertUrl($appid, $title, $version, $version_code, $variant, $obb)
	{
		$download_url = "";
		if ($obb == 0) {
			$download_url .= "apk=apk";
		} elseif ($obb == 1) {
			$download_url .= "apk=bundle";
		} elseif ($obb == 2) {
			$download_url .= "apk=obb";
		}
		$download_url .= "&id=" . $appid;
		if ($variant == 1) {
			$download_url .= "&variant=1";
		} else {
			$download_url .= "&variant=0";
		}
		$download_url .= "&title=" . $title . "&v=" . $version . "&sn=" . $version_code . '&t=' . time();
		$download_url = base64_encode($download_url);
		$code_check = md5($title . "Tovi123@");
		$download_url = $download_url . "&check=" . $code_check;
		$download_url = base64_encode($download_url);
		return $download_url;
	}
}

/* End of file Apk.php */
/* Location: ./application/controllers/Apk.php */
