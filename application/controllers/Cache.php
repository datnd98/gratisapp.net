<?php 
/*
Thực hiện việc request cache và update data vào elasticsearch
*/
defined('BASEPATH') OR exit('No direct script access allowed');
require './vendor/autoload.php'; 
class Cache extends CI_Controller {

	protected $client;
	protected $name_index;
	protected $api_img;
	public function __construct()
	{
		parent::__construct();
		$this->client = \Elasticsearch\ClientBuilder::create()->setSSLVerification(false)
        ->setHosts(["136.243.138.237:9200"])->build();
		$this->name_index = 'apktrending_new';
        $this->load->helper('fnc');
	}
	public function index()
	{
		
	}
	public function checkHeader($request){
        $header = $request->header('x-api-key');
        if($header == 'nyztcnpvhwuancvw'){
            return true;
        } else {
            return false;
        }

    }
    // index file new ( từ cổng sendpost của java)
	public function indexFile(){
        // $check = $this->checkHeader($_POST);
        // if($check == false){
        //     $result = array('status' => 'failure', 'message' => 'error key');
        //     return $result;
        // }
        $data = $_POST['data'];
        $data = urldecode($data);
        //print_r($data);die;
        $data = json_decode($data,true);

        
        $appid = $_POST['appid'];

        //print_r($data);die;
        $params['index'] = 'apk_file';
        $params['type'] = 'apk_file';
        $params['id'] = $appid.$data['versionCode'];
        foreach ($data as $key => $value) {
            if($value != '' || $value != ' ' || empty(!$value)){
                if(strtolower($key) == 'uploaddate'){
                    $tmp = strtotime($value);
                    $params['body']['doc'][strtolower($key)] = date('Y-m-d H:i:s',$tmp);

                } else {
                    $params['body']['doc'][strtolower($key)] = $value;
                }
            }

        };
        //print_r($params);
        $params['body']['doc']['appid'] = $appid;
        $params['body']['doc_as_upsert'] = true;
        $client1 = \Elasticsearch\ClientBuilder::create(['host'=> 'localhost:9200'])->build();
        $response = $client1->update($params);
        //print_r($response);
        //return response()->json($response);
        
        $detail = $this->getDetailApp($appid);
        $url = 'https://apktrending.com/'.$detail['data']['urlTitle'].'/'.$appid;
        echo $url;
        
        $this->purgeCache($url);
        $this->deleteCache('/'.$detail['data']['urlTitle'].'/'.$appid);
        print_r($response);
        exit();
    }
    //delete cache của 1 url
    public function deleteCache($uri){
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->output->delete_cache($uri); 
    }
    // index app ( từ cổng sendpost của java)
    public function indexApp(){
        // $check = $this->checkHeader($_POST);
        // if($check == false){
        //     $result = array('status' => 'failure', 'message' => 'error key');
        //     return $result;
        // }
      
        $data = $_POST['data'];
        $data = urldecode($data);
        //print_r($data);die;

        $data = json_decode($data,true);
        $client = \Elasticsearch\ClientBuilder::create(['host'=>'localhost:9200'])->build();
        $params['index'] = $this->name_index;
        $params['type'] = $this->name_index;
        $params['id'] = $data['appid'];
        $detail = $this->getDetailApp($data['appid']);
        if($detail['found'] == true){
            if(!isset($detail['data']['urlTitle']) || $detail['data']['urlTitle'] == ''){
                $params['body']['doc']['urltitle'] = str_slug($data['title']);
                if($params['body']['doc']['urltitle'] == ''){
                    $params['body']['doc']['urltitle'] = $data['title'];
                }
            }
        } else {
            $params['body']['doc']['urltitle'] = str_slug($data['title']);
            if($params['body']['doc']['urltitle'] == ''){
                $params['body']['doc']['urltitle'] = $data['title'];
            }
        } 
        //print_r($params['body']['doc']);
        foreach ($data as $key => $value) {
            if($key == 'updated'){
                $params['body']['doc'][strtolower($key)] = strtotime($value);
            } else {
                $params['body']['doc'][strtolower($key)] = $value;
            }
            if($key == 'uploadDate'){
                if(!is_numeric($value)){
                    $params['body']['doc'][strtolower($key)] = strtotime($value);
                }
            }
        };
        //print_r($params);
        $params['body']['doc_as_upsert'] = true;
        $response = $client->update($params);
        print_r($response);
        exit();
        // $tmp = $this->getApp1000($data['appid']);
        // if($tmp['found'] == true){
        //     $res_data = $this->update_doc_1000($data);
        //     //print_r($res_data);
        // }
        //return response()->json($response);
    }
    // thêm app bị kiện từ cổng cdn.apktovi.com
    public function addAbuse(){

        $appid = $_GET['appid'];
        $index = $_GET['index'];
        $type = $_GET['type'];
        //print_r($request->input());die;
        $client = \Elasticsearch\ClientBuilder::create(['host'=>'136.243.138.237:9200'])->build();
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $appid,
            'body' => [ 'appid' => $appid,'type' => 'abuse','status' => 1, "added" => 1]
        ];
        //print_r($params);die;
        $detail = $this->getDetailApp($appid);
        $url = 'https://apktrending.com/'.$detail['data']['urlTitle'].'/'.$appid;
        $this->purgeCache($url);
        $this->deleteCache('/'.$detail['data']['urlTitle'].'/'.$appid);
        $response = $client->index($params);
        print_r($response);
        exit();
        // Create the index
        
    }
	public function addAbuse2(){

        $appid = $_GET['appid'];
        $index = $_GET['index'];
        $type = $_GET['type'];
        //print_r($request->input());die;
        $client = \Elasticsearch\ClientBuilder::create()->setSSLVerification(false)->setHosts(["144.202.0.117:9200"])->build();
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $appid,
            'body' => [ 'appid' => $appid,'type' => 'abuse','status' => 1, "added" => 1]
        ];
		$params['body']['doc_as_upsert'] = true;
        //print_r($params);die;
        // $detail = $this->getDetailApp($appid);
        // $url = 'https://apktrending.com/'.$detail['data']['urlTitle'].'/'.$appid;
        // $this->purgeCache($url);
        // $this->deleteCache('/'.$detail['data']['urlTitle'].'/'.$appid);
        $response = $client->index($params);
        print_r($response);
        exit();
        // Create the index
        
    }
    // purge cache của cloudflare
    public function purgeCache($url){
        $body = array('files' => array());
        $body['files'][] = $url;
        $curl = curl_init();
        print_r($body);
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/45d163cecbb147f239fdee620f3d5432/purge_cache",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($body),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer B58sfiG43P_4aBqKxuZPJhX1KWyuIWzN4hKKXRTs",
            "Content-Type: application/json",
            "X-Auth-Email: thanhtoanlc9@gmail.com",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
    }
    public function purgeCache1(){
		$url = $_GET['url'];
        $body = array('files' => array());
        $body['files'][] = $url;
        $curl = curl_init();
        print_r($body);
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.cloudflare.com/client/v4/zones/45d163cecbb147f239fdee620f3d5432/purge_cache",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($body),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer B58sfiG43P_4aBqKxuZPJhX1KWyuIWzN4hKKXRTs",
            "Content-Type: application/json",
            "X-Auth-Email: thanhtoanlc9@gmail.com",
            "cache-control: no-cache"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
    }
    public function getDetailApp($id)
    {
        $params['index'] = 'apktrending_new';
        $params['type'] = 'apktrending_new';
        $params['id'] = $id;
        $params['client'] = ['ignore' => 404];
        $app = $this->client->get($params);
        //print_r($app);die;
        if($app['found'] == true){
          // $tmp = strtolower($app['_source']['category']);
          // $tmp = str_replace(' & ', '_and_', $tmp);
          // $tmp = str_replace(' ', '_', $tmp);
          // $app['_source']['category_url'] = $tmp;
            if(isset($app['_source']['urltitle'])){
              $app['_source']['urlTitle'] = $app['_source']['urltitle'];
            }
          $result = array('data' => $app['_source'],'found' => $app['found']);
        } else {
          $result = array('data' => array(),'found' => $app['found']);
        }
        return $result;
    }
    public function detailApp(){
        $id = $_GET['id'];
        $params['index'] = 'apktrending_new';
        $params['type'] = 'apktrending_new';
        $params['id'] = $id;
        $params['client'] = ['ignore' => 404];
        $app = $this->client->get($params);
        //print_r($app);die;
        if($app['found'] == true){
          // $tmp = strtolower($app['_source']['category']);
          // $tmp = str_replace(' & ', '_and_', $tmp);
          // $tmp = str_replace(' ', '_', $tmp);
          // $app['_source']['category_url'] = $tmp;
            if(isset($app['_source']['urltitle'])){
              $app['_source']['urlTitle'] = $app['_source']['urltitle'];
            }
            $file = $this->getFile(1, 1, $id);
            //print_r($file);die;
            $app['_source']['file_name'] = 'apktrending.'.$id.'.'.$file['data'][0]['versionCode'].'.';
            $app['_source']['file'] = $file['data'];
            if($app['_source']['obb'] == 0){
                $app['_source']['file_name'] .= 'apk';
            } else {
                $app['_source']['file_name'] .= 'zip';
            }
            $result = array('data' => $app['_source'],'found' => $app['found']);
        } else {
          $result = array('data' => array(),'found' => $app['found']);
        }
        echo json_encode($result);
        exit();
    }
    public function getFile($page,$size,$id)
    {
        $from = ($page - 1)*$size;
        $params['index'] = 'apk_file';
        $params['type'] = 'apk_file';
        $params['body'] = [
            'size' => $size,
            'from' => $from,
            'sort' => ['uploaddate' => ['order' => 'desc']],
            'query' => [
                'match' => [
                    'appid' => $id
                ]
                
            ],
        ];
        $res = $this->client->search($params);
        //print_r($res);die;
        if(empty($res['hits']['hits'])){
          return array('file' => 'not_found');
        }
        $a = array();
        foreach ($res['hits']['hits'] as $key => $app) {
          //print_r($app);die;
          unset($app['_source']['@version']);
          unset($app['_source']['@timestamp']);
          $app['_source']["uploadDate"] = $app['_source']['uploaddate'];
          $app['_source']["versionCode"] = $app['_source']['versioncode'];
          $a[] = $app['_source'];
        }

        $result = array('total' => $res['hits']['total'],'page' => $page, 'data' => $a);
        return $result;
    }
    public function getListApp(){
        
        $page = $_GET['page'];
        $size = $_GET['size'];

        $from = ($page -1)*$size;
        $params['index'] = $this->name_index;
        $params['type'] = $this->name_index;

        $params['body'] = [
            'size' => $size,
            'from' => $from,
            'sort' => ['installs' => ['order' => 'desc']],
            'query' => [
              'bool' => [
                'filter' => [
                  'match' => [
                    'is_file' => 1
                  ]
                ],
                // 'must' => [
                //   ['match' => [
                //     'game' => $type
                //   ]],
                // ]
              ]

            ],
        ];
        $res = $this->client->search($params);

        $a = array();
        foreach ($res['hits']['hits'] as $key => $app) {
            unset($app['_source']['@version']);
            unset($app['_source']['@timestamp']);
            $app['_source']['urlTitle'] = $app['_source']['urltitle'];
            $app['_source']['uploadDate'] = $app['_source']['uploaddate'];
            if($app['_source']['uploaddate'] == 0){
                $app['_source']['uploadDate'] = time();
            }
            $a[] = $app['_source'];
        }
        $result = array('total' => $res['hits']['total'],'page' => $page, 'data' => $a);
        echo json_encode($result);
        exit();
    }
}

/* End of file Cache.php */
/* Location: ./application/controllers/Cache.php */
 ?>