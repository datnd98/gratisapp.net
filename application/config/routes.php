<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Apk';
$route['insertdevice'] = 'Apk/InsertDeviceMobile';
$route['verification'] = 'Apk/verificationApp';
$route['generallink'] = 'Apk/generallink';
$route['showallbrand'] = 'Apk/showAllBrand';
$route['showbrand'] = 'Apk/showBrand';
$route['showmodel'] = 'Apk/showmodel';
$route['generalmodel'] = 'Apk/generalmodel';
$route['livesearch'] = 'Apk/livesearch';
$route['search'] = 'Apk/search';
$route['apps'] = 'Apk/apkApp';
$route['dmca'] = 'Apk/page';
$route['privacy-policy'] = 'Apk/page';
$route['about'] = 'Apk/page';
$route['games'] = 'Apk/apkGame';
$route['topnewapps'] = 'Apk/viewallnewapp';
$route['toppopularapps'] = 'Apk/viewallpopularapp';
$route['topgame24h'] = 'Apk/viewallgame24h';
$route['topnewgame'] = 'Apk/viewallnewgame';
$route['dev/(:any)'] = 'Apk/devApps';
$route['category/(:any)'] = 'Apk/categoryApps';
$route['deleteCache'] = 'Apk/deleteCache';
$route['indexFile'] = 'Cache/indexFile';
$route['indexApp'] = 'Cache/indexApp';
$route['purgeCache'] = 'Cache/purgeCache1';
$route['abuse'] = 'Cache/addAbuse';
$route['abuse2'] = 'Cache/addAbuse2';
$route['apiDetail'] = 'Cache/detailApp';
$route['listApp'] = 'Cache/getListApp';
$route['download/(:any)'] = 'Apk/downloadApp';
$route['apk-android/(:any)'] = 'Apk/detail';
$route['(:any)/(:any)'] = 'Apk/detailNew';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
