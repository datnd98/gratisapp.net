# Overview
Project web site apktrending.com sử dụng framework codeigniter
Các tính năng chính của web
- List app by category
- Detail app
- Download apk

# Config
- Thực hiện config router trong thư mục config/router.php
- Thực hiện chỉnh sửa về elasticsearch trong controllers/Api.php (sửa cái localhost:9200 thành ip vận hành trong local)
- Chỉnh sửa về clear cache cloudflare, clear cache trên server Cache.php
- Chỉnh sửa về code chính web ở controller Apk.php
# Run
Thực hiện lệnh composer update
# Edit Code
Khi chỉnh sửa trên server thì cần thực hiện clear cache thông qua url 
https://cdn.apktovi.com/purgeCache