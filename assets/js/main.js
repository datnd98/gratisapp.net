"use strict";

(function($) {
  var APKTOP = {
    init: function init() {
      var self = this;

      self.slickCategory();
      self.randomColor();
      self.countDown();
      self.slickScreenShot();
      self.autoComplete();
      //add them for mobile
      self.hambergurMenu();
      self.searchMenu();
      self.autoCompleteMobile();
    },

    slickCategory: function slickCategory() {
      $(".apk-category__slick").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        dots: true,
        mobileFirst: true,
        responsive: [{
          breakpoint: 1200,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 992,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 576,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        }]
      });
    },

    randomColor: function randomColor() {
      var key = ["#89cffa", "#a9a3db", "#c5e1a5", "#ffcc80", "#ffccbc", "#f8bbd0", "#4f87de", "#58be5b", "#fcd700", "#ee6150"];

      $('.apk-category__slick-item').each(function() {
        var randomColor = key[Math.floor(Math.random() * key.length)];
        $(this).children().css('backgroundColor', randomColor);
      });
    },

    countDown: function countDown() {
      $('[data-countdown="init"]').each(function() {
        var $el = $(this);
        var endDate = $el.data('countdown-final');
        $el.countdown(endDate, function(event) {
          $(this).html(event.strftime("%D days %H:%M:%S"));
        });
      });
    },

    slickScreenShot: function slickScreenShot() {
      $('#pills-screenshot-tab').on("click",function(){
        $('.apk-screen-shot__slick').slick({infinite: false});
        $('.slick-prev').click();
      })
    },

    autoComplete: function autoComplete() {
      $('#search-top').keypress(function (e) {
        $('.search-auto-complete').show();
      })
    },

    /////add them for mobile/////
    hambergurMenu: function hambergurMenu() {
        $('.hambergur').on('click', function () {
            $('.menu-mobile').toggleClass('active');
        })
    },
    //click search
    searchMenu: function searchMenu() {
        $('.search-btn').on('click', function () {
            $('.search-mobile').toggleClass('active');
        })
    },
    autoCompleteMobile: function autoCompleteMobile() {
        $('#search-top-mobile').keypress(function (e) {
            $('.search-auto-complete').show();
        })
    },

  };

  $(document).ready(function() {
    APKTOP.init();
    $('[data-toggle="tooltip"]').tooltip();
  });
})(jQuery);